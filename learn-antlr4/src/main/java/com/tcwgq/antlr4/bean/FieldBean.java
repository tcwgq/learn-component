package com.tcwgq.antlr4.bean;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tcwgq.antlr4.field.FieldType;
import lombok.Data;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 字段信息
 */
@Data
public class FieldBean {
    /**
     * 字段名称
     */
    private String name;

    /**
     * 描述
     */
    private String description;

    /**
     * 字段数据类型，{@link FieldType}
     */
    private String type;

    /**
     * 指向的bean class name
     */
    private String ref;

    /**
     * 是否必须
     */
    private Boolean required;

    /**
     * 逻辑
     */
    private String logic;

    /**
     * 逻辑组合
     */
    private String valueExpressions;

    /**
     * 默认值
     */
    @JsonProperty("default")
    private String defaultValue;

    /**
     * list类型的元素字段
     */
    private FieldBean element;

    /**
     * 当类型是对象时指代对象内部的字段
     */
    private List<FieldBean> fields;

    /**
     * 当类型是对象时指代对象内部的字段
     */
    private List<String> values;

    public Map<String, FieldBean> getFields() {
        if (fields == null) {
            return Collections.emptyMap();
        }
        return fields.stream()
                .collect(Collectors.toMap(FieldBean::getName, Function.identity()));
    }

    public List<String> getValues() {
        if (values == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList(values);
    }
}
