package com.tcwgq.antlr4.bean;

import lombok.Data;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 一个ULR路径对应的处理流程
 */
@Data
public class FlowBean {
    /**
     * 绑定的URL路径
     */
    private String path;

    /**
     * 输入入参列表
     */
    private List<FieldBean> inputs;

    /**
     * 执行计划节点列表
     */
    private List<NodeBean> plan;

    /**
     * 输出出参列表
     */
    private List<FieldBean> outputs;

    public Map<String, FieldBean> getInputs() {
        if (inputs == null) {
            return Collections.emptyMap();
        }

        Map<String, FieldBean> result = new HashMap<>(inputs.size());
        for (FieldBean fieldBean : inputs) {
            if (result.containsKey(fieldBean.getName())) {
                throw new IllegalArgumentException("duplicated input: " + fieldBean.getName());
            }
            result.put(fieldBean.getName(), fieldBean);
        }
        return Collections.unmodifiableMap(result);
    }

    public Map<String, NodeBean> getPlan() {
        if (plan == null) {
            return Collections.emptyMap();
        }

        Map<String, NodeBean> result = new HashMap<>(plan.size());
        for (NodeBean nodeBean : plan) {
            if (result.containsKey(nodeBean.getName())) {
                throw new IllegalArgumentException("duplicated node: " + nodeBean.getName());
            }
            result.put(nodeBean.getName(), nodeBean);
        }
        return Collections.unmodifiableMap(result);
    }

    public Map<String, FieldBean> getOutputs() {
        if (outputs == null) {
            return Collections.emptyMap();
        }

        Map<String, FieldBean> result = new HashMap<>(inputs.size());
        for (FieldBean fieldBean : outputs) {
            if (result.containsKey(fieldBean.getName())) {
                throw new IllegalArgumentException("duplicated output: " + fieldBean.getName());
            }
            result.put(fieldBean.getName(), fieldBean);
        }
        return Collections.unmodifiableMap(result);
    }
}
