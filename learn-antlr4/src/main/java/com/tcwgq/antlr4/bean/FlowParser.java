package com.tcwgq.antlr4.bean;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * flow解析器
 */
public class FlowParser {
    /**
     * 从配置中解析flow
     *
     * @param config 配置
     * @return 解析出的flow
     */
    public FlowBean parse(String config) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(config, FlowBean.class);
    }
}
