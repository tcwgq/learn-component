package com.tcwgq.antlr4.bean;

import lombok.Data;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 执行计划节点
 */
@Data
public class NodeBean {
    /**
     * 节点名称
     */
    private String name;

    /**
     * 上一个节点，如果没有则为null
     */
    private List<String> previous;

    /**
     * 执行计划节点类型，{@link NodeType}
     */
    private String type;

    /**
     * 是否快速失败
     */
    private Boolean fastFailure = Boolean.TRUE;

    /**
     * 当执行计划节点类型为feign的时候的 feign client bean 名称
     */
    private String client;

    /**
     * 当执行计划节点类型为feign的时候的 feign client 方法名称
     */
    private String method;

    /**
     * 入参列表
     */
    private List<FieldBean> params;

    /**
     * 出参列表
     */
    private FieldBean results;

    /**
     * 仅用于循环node，根据什么进行循环
     */
    private String by;

    /**
     * 仅用于循环node，循环逻辑节点
     */

    private List<NodeBean> eachNodes;

    public List<String> getPrevious() {
        return previous == null ? Collections.emptyList() : Collections.unmodifiableList(previous);
    }

    public Map<String, FieldBean> getParams() {
        if (params == null) {
            return Collections.emptyMap();
        }

        Map<String, FieldBean> paramMap = new HashMap<>(params.size());
        for (int i = 0; i < params.size(); i++) {
            FieldBean param = params.get(i);
            paramMap.put("$" + i, param);
        }
        return paramMap;
    }

    public FieldBean getResults() {
        if (results != null) {
            results.setName("$0");
        }
        return results;
    }
}
