package com.tcwgq.antlr4.exception;

/**
 * 需要全局处理的异常
 */
public class FusionException extends RuntimeException {
    private final FusionReturn fusionReturn;

    public FusionException(FusionReturn fusionReturn) {
        super(fusionReturn.getMessage());
        this.fusionReturn = fusionReturn;
    }

    public FusionException(FusionReturn fusionReturn, Throwable cause) {
        super(fusionReturn.getMessage(), cause);
        this.fusionReturn = fusionReturn;
    }

    public FusionException(FusionReturn fusionReturn, String detailMessage) {
        super(fusionReturn.getMessage() + ": " + detailMessage);
        this.fusionReturn = fusionReturn;
    }

    public FusionException(FusionReturn fusionReturn, String detailMessage, Throwable cause) {
        super(fusionReturn.getMessage() + ": " + detailMessage, cause);
        this.fusionReturn = fusionReturn;
    }

    public int getCode() {
        return fusionReturn.getCode();
    }
}
