package com.tcwgq.antlr4.exception;

import lombok.Getter;

/**
 * 系统相关返回信息
 */
@Getter
public enum FusionReturn {
    UNKNOWN_PATH(404, "不支持的请求路径，请联系风控研发"),
    WRONG_FIELD(405, "字段错误"),
    BEAN_CAST_FAILED(501, "无法获取bean属性"),
    WRONG_CONFIG(502, "错误的配置文件，请联系风控研发"),
    UNKNOWN_FIELD(503, "错误的属性配置"),
    BACKEND_FAILED(504, "风控依赖的其他后端服务异常"),
    WRONG_BACKEND_PARAM(505, "参数错误"),
    MESSAGE_CONSUMED_FAILED(506, "消息消费失败"),
    UNSUPPORTED_OPERATION(600, "不支持的操作"),
    WRONG_FUNCTION(700, "错误的函数");

    private final int code;
    private final String message;

    FusionReturn(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
