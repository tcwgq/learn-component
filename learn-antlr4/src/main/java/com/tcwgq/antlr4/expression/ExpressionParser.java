// Generated from D:/gitlab/learn-component/learn-antlr4/src/main/resources\Expression.g4 by ANTLR 4.8
package com.tcwgq.antlr4.expression;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ExpressionParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, NUMBER=15, ID=16, STRING=17, 
		WS=18;
	public static final int
		RULE_propertyExpr = 0, RULE_variableExpr = 1, RULE_functionExpr = 2, RULE_paramExprs = 3, 
		RULE_functionParamExpr = 4;
	private static String[] makeRuleNames() {
		return new String[] {
			"propertyExpr", "variableExpr", "functionExpr", "paramExprs", "functionParamExpr"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'->'", "'('", "'=='", "')'", "'.'", "'._'", "'null'", "'?'", "':'", 
			"'${'", "'}'", "'()'", "', '", "'_.'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, "NUMBER", "ID", "STRING", "WS"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Expression.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public ExpressionParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class PropertyExprContext extends ParserRuleContext {
		public PropertyExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_propertyExpr; }
	 
		public PropertyExprContext() { }
		public void copyFrom(PropertyExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class VariablePropertyContext extends PropertyExprContext {
		public VariableExprContext variableExpr() {
			return getRuleContext(VariableExprContext.class,0);
		}
		public VariablePropertyContext(PropertyExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).enterVariableProperty(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).exitVariableProperty(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExpressionVisitor ) return ((ExpressionVisitor<? extends T>)visitor).visitVariableProperty(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SubPropertyContext extends PropertyExprContext {
		public PropertyExprContext propertyExpr() {
			return getRuleContext(PropertyExprContext.class,0);
		}
		public TerminalNode ID() { return getToken(ExpressionParser.ID, 0); }
		public SubPropertyContext(PropertyExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).enterSubProperty(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).exitSubProperty(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExpressionVisitor ) return ((ExpressionVisitor<? extends T>)visitor).visitSubProperty(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EqualsExpressionContext extends PropertyExprContext {
		public List<PropertyExprContext> propertyExpr() {
			return getRuleContexts(PropertyExprContext.class);
		}
		public PropertyExprContext propertyExpr(int i) {
			return getRuleContext(PropertyExprContext.class,i);
		}
		public EqualsExpressionContext(PropertyExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).enterEqualsExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).exitEqualsExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExpressionVisitor ) return ((ExpressionVisitor<? extends T>)visitor).visitEqualsExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class StringExpressionContext extends PropertyExprContext {
		public TerminalNode STRING() { return getToken(ExpressionParser.STRING, 0); }
		public StringExpressionContext(PropertyExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).enterStringExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).exitStringExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExpressionVisitor ) return ((ExpressionVisitor<? extends T>)visitor).visitStringExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MapValueExpressionContext extends PropertyExprContext {
		public List<PropertyExprContext> propertyExpr() {
			return getRuleContexts(PropertyExprContext.class);
		}
		public PropertyExprContext propertyExpr(int i) {
			return getRuleContext(PropertyExprContext.class,i);
		}
		public MapValueExpressionContext(PropertyExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).enterMapValueExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).exitMapValueExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExpressionVisitor ) return ((ExpressionVisitor<? extends T>)visitor).visitMapValueExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NumberExpressionContext extends PropertyExprContext {
		public TerminalNode NUMBER() { return getToken(ExpressionParser.NUMBER, 0); }
		public NumberExpressionContext(PropertyExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).enterNumberExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).exitNumberExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExpressionVisitor ) return ((ExpressionVisitor<? extends T>)visitor).visitNumberExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NullExpressionContext extends PropertyExprContext {
		public NullExpressionContext(PropertyExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).enterNullExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).exitNullExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExpressionVisitor ) return ((ExpressionVisitor<? extends T>)visitor).visitNullExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TernaryExpressionContext extends PropertyExprContext {
		public List<PropertyExprContext> propertyExpr() {
			return getRuleContexts(PropertyExprContext.class);
		}
		public PropertyExprContext propertyExpr(int i) {
			return getRuleContext(PropertyExprContext.class,i);
		}
		public TernaryExpressionContext(PropertyExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).enterTernaryExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).exitTernaryExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExpressionVisitor ) return ((ExpressionVisitor<? extends T>)visitor).visitTernaryExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FunctionTransformContext extends PropertyExprContext {
		public PropertyExprContext propertyExpr() {
			return getRuleContext(PropertyExprContext.class,0);
		}
		public FunctionExprContext functionExpr() {
			return getRuleContext(FunctionExprContext.class,0);
		}
		public FunctionTransformContext(PropertyExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).enterFunctionTransform(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).exitFunctionTransform(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExpressionVisitor ) return ((ExpressionVisitor<? extends T>)visitor).visitFunctionTransform(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CollectionPropertyContext extends PropertyExprContext {
		public PropertyExprContext propertyExpr() {
			return getRuleContext(PropertyExprContext.class,0);
		}
		public TerminalNode NUMBER() { return getToken(ExpressionParser.NUMBER, 0); }
		public CollectionPropertyContext(PropertyExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).enterCollectionProperty(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).exitCollectionProperty(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExpressionVisitor ) return ((ExpressionVisitor<? extends T>)visitor).visitCollectionProperty(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PropertyExprContext propertyExpr() throws RecognitionException {
		return propertyExpr(0);
	}

	private PropertyExprContext propertyExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		PropertyExprContext _localctx = new PropertyExprContext(_ctx, _parentState);
		PropertyExprContext _prevctx = _localctx;
		int _startState = 0;
		enterRecursionRule(_localctx, 0, RULE_propertyExpr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(21);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__1:
				{
				_localctx = new EqualsExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(11);
				match(T__1);
				setState(12);
				propertyExpr(0);
				setState(13);
				match(T__2);
				setState(14);
				propertyExpr(0);
				setState(15);
				match(T__3);
				}
				break;
			case T__9:
				{
				_localctx = new VariablePropertyContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(17);
				variableExpr();
				}
				break;
			case NUMBER:
				{
				_localctx = new NumberExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(18);
				match(NUMBER);
				}
				break;
			case T__6:
				{
				_localctx = new NullExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(19);
				match(T__6);
				}
				break;
			case STRING:
				{
				_localctx = new StringExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(20);
				match(STRING);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(43);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(41);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
					case 1:
						{
						_localctx = new MapValueExpressionContext(new PropertyExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_propertyExpr);
						setState(23);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(24);
						match(T__0);
						setState(25);
						propertyExpr(11);
						}
						break;
					case 2:
						{
						_localctx = new TernaryExpressionContext(new PropertyExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_propertyExpr);
						setState(26);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(27);
						match(T__7);
						setState(28);
						propertyExpr(0);
						setState(29);
						match(T__8);
						setState(30);
						propertyExpr(2);
						}
						break;
					case 3:
						{
						_localctx = new SubPropertyContext(new PropertyExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_propertyExpr);
						setState(32);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(33);
						match(T__4);
						setState(34);
						match(ID);
						}
						break;
					case 4:
						{
						_localctx = new CollectionPropertyContext(new PropertyExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_propertyExpr);
						setState(35);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(36);
						match(T__5);
						setState(37);
						match(NUMBER);
						}
						break;
					case 5:
						{
						_localctx = new FunctionTransformContext(new PropertyExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_propertyExpr);
						setState(38);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(39);
						match(T__4);
						setState(40);
						functionExpr();
						}
						break;
					}
					} 
				}
				setState(45);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class VariableExprContext extends ParserRuleContext {
		public VariableExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableExpr; }
	 
		public VariableExprContext() { }
		public void copyFrom(VariableExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class VariableContext extends VariableExprContext {
		public TerminalNode ID() { return getToken(ExpressionParser.ID, 0); }
		public VariableContext(VariableExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).enterVariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).exitVariable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExpressionVisitor ) return ((ExpressionVisitor<? extends T>)visitor).visitVariable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VariableExprContext variableExpr() throws RecognitionException {
		VariableExprContext _localctx = new VariableExprContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_variableExpr);
		try {
			_localctx = new VariableContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(46);
			match(T__9);
			setState(47);
			match(ID);
			setState(48);
			match(T__10);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionExprContext extends ParserRuleContext {
		public FunctionExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionExpr; }
	 
		public FunctionExprContext() { }
		public void copyFrom(FunctionExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class FunctionWithoutParamContext extends FunctionExprContext {
		public TerminalNode ID() { return getToken(ExpressionParser.ID, 0); }
		public FunctionWithoutParamContext(FunctionExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).enterFunctionWithoutParam(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).exitFunctionWithoutParam(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExpressionVisitor ) return ((ExpressionVisitor<? extends T>)visitor).visitFunctionWithoutParam(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FunctionWithParamsContext extends FunctionExprContext {
		public TerminalNode ID() { return getToken(ExpressionParser.ID, 0); }
		public ParamExprsContext paramExprs() {
			return getRuleContext(ParamExprsContext.class,0);
		}
		public FunctionWithParamsContext(FunctionExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).enterFunctionWithParams(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).exitFunctionWithParams(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExpressionVisitor ) return ((ExpressionVisitor<? extends T>)visitor).visitFunctionWithParams(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionExprContext functionExpr() throws RecognitionException {
		FunctionExprContext _localctx = new FunctionExprContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_functionExpr);
		try {
			setState(57);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				_localctx = new FunctionWithParamsContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(50);
				match(ID);
				setState(51);
				match(T__1);
				setState(52);
				paramExprs(0);
				setState(53);
				match(T__3);
				}
				break;
			case 2:
				_localctx = new FunctionWithoutParamContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(55);
				match(ID);
				setState(56);
				match(T__11);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParamExprsContext extends ParserRuleContext {
		public ParamExprsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_paramExprs; }
	 
		public ParamExprsContext() { }
		public void copyFrom(ParamExprsContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class PropertyParamContext extends ParamExprsContext {
		public PropertyExprContext propertyExpr() {
			return getRuleContext(PropertyExprContext.class,0);
		}
		public PropertyParamContext(ParamExprsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).enterPropertyParam(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).exitPropertyParam(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExpressionVisitor ) return ((ExpressionVisitor<? extends T>)visitor).visitPropertyParam(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FunctionParamContext extends ParamExprsContext {
		public FunctionParamExprContext functionParamExpr() {
			return getRuleContext(FunctionParamExprContext.class,0);
		}
		public FunctionParamContext(ParamExprsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).enterFunctionParam(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).exitFunctionParam(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExpressionVisitor ) return ((ExpressionVisitor<? extends T>)visitor).visitFunctionParam(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MultiParamsContext extends ParamExprsContext {
		public List<ParamExprsContext> paramExprs() {
			return getRuleContexts(ParamExprsContext.class);
		}
		public ParamExprsContext paramExprs(int i) {
			return getRuleContext(ParamExprsContext.class,i);
		}
		public MultiParamsContext(ParamExprsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).enterMultiParams(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).exitMultiParams(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExpressionVisitor ) return ((ExpressionVisitor<? extends T>)visitor).visitMultiParams(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParamExprsContext paramExprs() throws RecognitionException {
		return paramExprs(0);
	}

	private ParamExprsContext paramExprs(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ParamExprsContext _localctx = new ParamExprsContext(_ctx, _parentState);
		ParamExprsContext _prevctx = _localctx;
		int _startState = 6;
		enterRecursionRule(_localctx, 6, RULE_paramExprs, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(62);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__1:
			case T__6:
			case T__9:
			case NUMBER:
			case STRING:
				{
				_localctx = new PropertyParamContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(60);
				propertyExpr(0);
				}
				break;
			case T__13:
				{
				_localctx = new FunctionParamContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(61);
				functionParamExpr(0);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(69);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new MultiParamsContext(new ParamExprsContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_paramExprs);
					setState(64);
					if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
					setState(65);
					match(T__12);
					setState(66);
					paramExprs(4);
					}
					} 
				}
				setState(71);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class FunctionParamExprContext extends ParserRuleContext {
		public FunctionParamExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionParamExpr; }
	 
		public FunctionParamExprContext() { }
		public void copyFrom(FunctionParamExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class FunctionPathContext extends FunctionParamExprContext {
		public TerminalNode ID() { return getToken(ExpressionParser.ID, 0); }
		public FunctionPathContext(FunctionParamExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).enterFunctionPath(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).exitFunctionPath(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExpressionVisitor ) return ((ExpressionVisitor<? extends T>)visitor).visitFunctionPath(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class InnerFunctionContext extends FunctionParamExprContext {
		public FunctionParamExprContext functionParamExpr() {
			return getRuleContext(FunctionParamExprContext.class,0);
		}
		public FunctionExprContext functionExpr() {
			return getRuleContext(FunctionExprContext.class,0);
		}
		public InnerFunctionContext(FunctionParamExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).enterInnerFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).exitInnerFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExpressionVisitor ) return ((ExpressionVisitor<? extends T>)visitor).visitInnerFunction(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FunctionMultiPathContext extends FunctionParamExprContext {
		public FunctionParamExprContext functionParamExpr() {
			return getRuleContext(FunctionParamExprContext.class,0);
		}
		public TerminalNode ID() { return getToken(ExpressionParser.ID, 0); }
		public FunctionMultiPathContext(FunctionParamExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).enterFunctionMultiPath(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionListener ) ((ExpressionListener)listener).exitFunctionMultiPath(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExpressionVisitor ) return ((ExpressionVisitor<? extends T>)visitor).visitFunctionMultiPath(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionParamExprContext functionParamExpr() throws RecognitionException {
		return functionParamExpr(0);
	}

	private FunctionParamExprContext functionParamExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		FunctionParamExprContext _localctx = new FunctionParamExprContext(_ctx, _parentState);
		FunctionParamExprContext _prevctx = _localctx;
		int _startState = 8;
		enterRecursionRule(_localctx, 8, RULE_functionParamExpr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new FunctionPathContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(73);
			match(T__13);
			setState(74);
			match(ID);
			}
			_ctx.stop = _input.LT(-1);
			setState(84);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,7,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(82);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
					case 1:
						{
						_localctx = new FunctionMultiPathContext(new FunctionParamExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_functionParamExpr);
						setState(76);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(77);
						match(T__4);
						setState(78);
						match(ID);
						}
						break;
					case 2:
						{
						_localctx = new InnerFunctionContext(new FunctionParamExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_functionParamExpr);
						setState(79);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(80);
						match(T__4);
						setState(81);
						functionExpr();
						}
						break;
					}
					} 
				}
				setState(86);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,7,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 0:
			return propertyExpr_sempred((PropertyExprContext)_localctx, predIndex);
		case 3:
			return paramExprs_sempred((ParamExprsContext)_localctx, predIndex);
		case 4:
			return functionParamExpr_sempred((FunctionParamExprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean propertyExpr_sempred(PropertyExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 10);
		case 1:
			return precpred(_ctx, 1);
		case 2:
			return precpred(_ctx, 8);
		case 3:
			return precpred(_ctx, 7);
		case 4:
			return precpred(_ctx, 6);
		}
		return true;
	}
	private boolean paramExprs_sempred(ParamExprsContext _localctx, int predIndex) {
		switch (predIndex) {
		case 5:
			return precpred(_ctx, 3);
		}
		return true;
	}
	private boolean functionParamExpr_sempred(FunctionParamExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 6:
			return precpred(_ctx, 3);
		case 7:
			return precpred(_ctx, 2);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\24Z\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2"+
		"\5\2\30\n\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3"+
		"\2\3\2\3\2\3\2\7\2,\n\2\f\2\16\2/\13\2\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4"+
		"\3\4\3\4\3\4\5\4<\n\4\3\5\3\5\3\5\5\5A\n\5\3\5\3\5\3\5\7\5F\n\5\f\5\16"+
		"\5I\13\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\7\6U\n\6\f\6\16\6X\13"+
		"\6\3\6\2\5\2\b\n\7\2\4\6\b\n\2\2\2b\2\27\3\2\2\2\4\60\3\2\2\2\6;\3\2\2"+
		"\2\b@\3\2\2\2\nJ\3\2\2\2\f\r\b\2\1\2\r\16\7\4\2\2\16\17\5\2\2\2\17\20"+
		"\7\5\2\2\20\21\5\2\2\2\21\22\7\6\2\2\22\30\3\2\2\2\23\30\5\4\3\2\24\30"+
		"\7\21\2\2\25\30\7\t\2\2\26\30\7\23\2\2\27\f\3\2\2\2\27\23\3\2\2\2\27\24"+
		"\3\2\2\2\27\25\3\2\2\2\27\26\3\2\2\2\30-\3\2\2\2\31\32\f\f\2\2\32\33\7"+
		"\3\2\2\33,\5\2\2\r\34\35\f\3\2\2\35\36\7\n\2\2\36\37\5\2\2\2\37 \7\13"+
		"\2\2 !\5\2\2\4!,\3\2\2\2\"#\f\n\2\2#$\7\7\2\2$,\7\22\2\2%&\f\t\2\2&\'"+
		"\7\b\2\2\',\7\21\2\2()\f\b\2\2)*\7\7\2\2*,\5\6\4\2+\31\3\2\2\2+\34\3\2"+
		"\2\2+\"\3\2\2\2+%\3\2\2\2+(\3\2\2\2,/\3\2\2\2-+\3\2\2\2-.\3\2\2\2.\3\3"+
		"\2\2\2/-\3\2\2\2\60\61\7\f\2\2\61\62\7\22\2\2\62\63\7\r\2\2\63\5\3\2\2"+
		"\2\64\65\7\22\2\2\65\66\7\4\2\2\66\67\5\b\5\2\678\7\6\2\28<\3\2\2\29:"+
		"\7\22\2\2:<\7\16\2\2;\64\3\2\2\2;9\3\2\2\2<\7\3\2\2\2=>\b\5\1\2>A\5\2"+
		"\2\2?A\5\n\6\2@=\3\2\2\2@?\3\2\2\2AG\3\2\2\2BC\f\5\2\2CD\7\17\2\2DF\5"+
		"\b\5\6EB\3\2\2\2FI\3\2\2\2GE\3\2\2\2GH\3\2\2\2H\t\3\2\2\2IG\3\2\2\2JK"+
		"\b\6\1\2KL\7\20\2\2LM\7\22\2\2MV\3\2\2\2NO\f\5\2\2OP\7\7\2\2PU\7\22\2"+
		"\2QR\f\4\2\2RS\7\7\2\2SU\5\6\4\2TN\3\2\2\2TQ\3\2\2\2UX\3\2\2\2VT\3\2\2"+
		"\2VW\3\2\2\2W\13\3\2\2\2XV\3\2\2\2\n\27+-;@GTV";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}