// Generated from D:/gitlab/learn-component/learn-antlr4/src/main/resources\Expression.g4 by ANTLR 4.8
package com.tcwgq.antlr4.expression;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link ExpressionParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface ExpressionVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by the {@code variableProperty}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableProperty(ExpressionParser.VariablePropertyContext ctx);
	/**
	 * Visit a parse tree produced by the {@code subProperty}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubProperty(ExpressionParser.SubPropertyContext ctx);
	/**
	 * Visit a parse tree produced by the {@code equalsExpression}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqualsExpression(ExpressionParser.EqualsExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stringExpression}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringExpression(ExpressionParser.StringExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code mapValueExpression}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMapValueExpression(ExpressionParser.MapValueExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code numberExpression}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumberExpression(ExpressionParser.NumberExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code nullExpression}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNullExpression(ExpressionParser.NullExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ternaryExpression}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTernaryExpression(ExpressionParser.TernaryExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code functionTransform}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionTransform(ExpressionParser.FunctionTransformContext ctx);
	/**
	 * Visit a parse tree produced by the {@code collectionProperty}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCollectionProperty(ExpressionParser.CollectionPropertyContext ctx);
	/**
	 * Visit a parse tree produced by the {@code variable}
	 * labeled alternative in {@link ExpressionParser#variableExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable(ExpressionParser.VariableContext ctx);
	/**
	 * Visit a parse tree produced by the {@code functionWithParams}
	 * labeled alternative in {@link ExpressionParser#functionExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionWithParams(ExpressionParser.FunctionWithParamsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code functionWithoutParam}
	 * labeled alternative in {@link ExpressionParser#functionExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionWithoutParam(ExpressionParser.FunctionWithoutParamContext ctx);
	/**
	 * Visit a parse tree produced by the {@code propertyParam}
	 * labeled alternative in {@link ExpressionParser#paramExprs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPropertyParam(ExpressionParser.PropertyParamContext ctx);
	/**
	 * Visit a parse tree produced by the {@code functionParam}
	 * labeled alternative in {@link ExpressionParser#paramExprs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionParam(ExpressionParser.FunctionParamContext ctx);
	/**
	 * Visit a parse tree produced by the {@code multiParams}
	 * labeled alternative in {@link ExpressionParser#paramExprs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiParams(ExpressionParser.MultiParamsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code functionPath}
	 * labeled alternative in {@link ExpressionParser#functionParamExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionPath(ExpressionParser.FunctionPathContext ctx);
	/**
	 * Visit a parse tree produced by the {@code innerFunction}
	 * labeled alternative in {@link ExpressionParser#functionParamExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInnerFunction(ExpressionParser.InnerFunctionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code functionMultiPath}
	 * labeled alternative in {@link ExpressionParser#functionParamExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionMultiPath(ExpressionParser.FunctionMultiPathContext ctx);
}