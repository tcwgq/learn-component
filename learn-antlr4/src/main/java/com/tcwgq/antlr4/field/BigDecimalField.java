package com.tcwgq.antlr4.field;

import com.tcwgq.antlr4.bean.FieldBean;
import org.apache.commons.lang.math.NumberUtils;

import java.math.BigDecimal;

/**
 * {@link BigDecimal}类型的字段
 */
class BigDecimalField extends SimpleField<BigDecimal> {
    public BigDecimalField(DataModel belongedDataModel, String name, FieldBean fieldBean) {
        super(belongedDataModel, name, fieldBean);
    }

    @Override
    public Class<?> rawType() {
        return BigDecimal.class;
    }

    @Override
    public BigDecimal cast(Object rawValue) {
        if (rawValue == null) {
            return defaultValue;
        }

        if (isAssignableTo(rawValue.getClass())) {
            return (BigDecimal) rawValue;
        }

        if (Number.class.isAssignableFrom(rawValue.getClass())) {
            return BigDecimal.valueOf(((Number) rawValue).doubleValue());
        }

        if (String.class.isAssignableFrom(rawValue.getClass())) {
            String rawValueString = (String) rawValue;
            if (NumberUtils.isNumber(rawValueString)) {
                return NumberUtils.createBigDecimal(rawValueString);
            }
        }
        return defaultValue;
    }
}
