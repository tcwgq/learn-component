package com.tcwgq.antlr4.field;

import com.tcwgq.antlr4.bean.FieldBean;
import org.apache.commons.lang.BooleanUtils;

/**
 * {@link String}类型的字段
 */
final class BooleanField extends SimpleField<Boolean> {
    public BooleanField(DataModel belongedDataModel, String name, FieldBean fieldBean) {
        super(belongedDataModel, name, fieldBean);
    }

    @Override
    public Class<?> rawType() {
        return Boolean.class;
    }

    @Override
    public Boolean cast(Object rawValue) {
        if (rawValue == null) {
            return defaultValue;
        }
        if (isAssignableTo(rawValue.getClass())) {
            return (Boolean) rawValue;
        }

        if (String.class.isAssignableFrom(rawValue.getClass())) {
            String rawValueString = (String) rawValue;
            return BooleanUtils.toBoolean(rawValueString);
        }
        return defaultValue;
    }
}
