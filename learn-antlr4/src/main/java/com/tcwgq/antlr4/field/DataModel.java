package com.tcwgq.antlr4.field;

import com.tcwgq.antlr4.exception.FusionException;
import com.tcwgq.antlr4.exception.FusionReturn;
import com.tcwgq.antlr4.orchestration.Context;
import com.tcwgq.antlr4.orchestration.Validatable;
import com.tcwgq.antlr4.orchestration.ValidationResult;
import com.tcwgq.antlr4.bean.FieldBean;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.PropertyUtils;

import java.lang.reflect.Constructor;
import java.util.Collections;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 数据模型
 */
@Slf4j
@Getter
public class DataModel implements Validatable<DataSet> {
    private final String name;
    private final Map<String, ModelField> fields;

    @Setter
    private Class<?> rawType;

    public DataModel(String name, Map<String, FieldBean> inputs) {
        this.name = name;
        this.fields = inputs.entrySet().stream()
                .map(entry -> createField(entry.getKey(), entry.getValue()))
                .collect(Collectors.toMap(ModelField::getName, Function.identity()));
    }

    private ModelField createField(String fieldName, FieldBean fieldBean) {
        FieldType fieldType = FieldType.of(fieldBean.getType());
        try {
            Constructor<? extends ModelField> constructor = fieldType.getType()
                    .getConstructor(DataModel.class, String.class, FieldBean.class);
            constructor.setAccessible(true);
            return constructor.newInstance(this, fieldName, fieldBean);
        } catch (Exception e) {
            log.warn("exception when creating field {}", fieldName, e);
            throw new IllegalArgumentException("wrong implementation: " + fieldName, e);
        }
    }

    public Map<String, ModelField> getFields() {
        return Collections.unmodifiableMap(fields);
    }

    @SuppressWarnings("unchecked")
    public DataSet cast(Object rawValue) {
        if (rawValue == null) {
            return new DataSet(this);
        } else if (DataSet.class.isAssignableFrom(rawValue.getClass())) {
            return (DataSet) rawValue;
        } else if (Map.class.isAssignableFrom(rawValue.getClass())) {
            return cast((Map<String, Object>) rawValue);
        } else {
            Map<String, Object> data;
            try {
                data = PropertyUtils.describe(rawValue);
            } catch (Exception e) {
                log.error("exception when getting properties from {}", rawValue.getClass(), e);
                throw new FusionException(FusionReturn.BEAN_CAST_FAILED, rawValue.getClass().getName());
            }
            return cast(data);
        }
    }

    private DataSet cast(Map<String, Object> rawValue) {
        DataSet dataSet = new DataSet(this);
        for (Map.Entry<String, ModelField> entry : fields.entrySet()) {
            String fieldName = entry.getKey();
            ModelField field = entry.getValue();
            Object rawFieldValue = rawValue.get(fieldName);
            Object fieldValue = field.cast(rawFieldValue);
            dataSet.setFieldValue(fieldName, fieldValue);
        }
        return dataSet;
    }

    public DataSet evaluate(Context context) {
        DataSet result = new DataSet(this);
        for (Map.Entry<String, ModelField> entry : fields.entrySet()) {
            String fieldName = entry.getKey();
            ModelField field = entry.getValue();
            Object fieldValue = field.evaluate(context);
            result.setFieldValue(fieldName, fieldValue);
        }
        return result;
    }

    public int getFieldSize() {
        return fields.size();
    }

    public boolean containsField(String fieldName) {
        return fields.containsKey(fieldName);
    }

    public ModelField getField(String fieldName) {
        return fields.get(fieldName);
    }

    @SuppressWarnings("unchecked")
    @Override
    public ValidationResult validate(DataSet data) {
        ValidationResult validationResult = new ValidationResult();
        for (Map.Entry<String, ModelField> entry : this.getFields().entrySet()) {
            ModelField modelField = entry.getValue();
            Object fieldValue = data.getFieldValue(modelField.getName());
            validationResult.add(modelField.validate(fieldValue));
        }
        return validationResult;
    }
}
