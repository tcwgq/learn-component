package com.tcwgq.antlr4.field;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tcwgq.antlr4.exception.FusionException;
import com.tcwgq.antlr4.exception.FusionReturn;
import com.tcwgq.antlr4.orchestration.ValidationResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.PropertyUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 通用数据集
 */
@Slf4j
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
public class DataSet {
    private final DataModel dataModel;
    private final Map<String, Object> data;

    public DataSet(DataModel dataModel) {
        this.dataModel = dataModel;
        this.data = new HashMap<>(dataModel.getFieldSize());
    }

    private DataSet(Map<String, Object> data) {
        this.dataModel = null;
        this.data = Collections.unmodifiableMap(data);
    }

    public static DataSet singleton(String key, Object value) {
        return new DataSet(Collections.singletonMap(key, value));
    }

    public void setFieldValue(String fieldName, Object fieldValue) {
        checkField(fieldName);
        data.put(fieldName, fieldValue);
    }

    public Object getFieldValue(String fieldName) {
        return data.get(fieldName);
    }

    private void checkField(String fieldName) {
        if (!dataModel.containsField(fieldName)) {
            log.error("unknown field {} in data model {}", fieldName, dataModel.getName());
            throw new FusionException(FusionReturn.UNKNOWN_FIELD,
                    "data model: " + dataModel.getName() + ", field: " + fieldName);
        }
    }

    public ValidationResult validate() {
        return this.dataModel.validate(this);
    }

    public Object toRawValue() {
        Class<?> rawType = this.dataModel.getRawType();
        if (Map.class.isAssignableFrom(rawType)) {
            return toMap();
        } else {
            try {
                Object bean = rawType.getConstructor().newInstance();
                this.data.forEach((fieldName, fieldValue) -> {
                    try {
                        if (ObjectField.class.isAssignableFrom(dataModel.getField(fieldName).getClass())) {
                            PropertyUtils.setProperty(bean, fieldName, ((DataSet) fieldValue).toRawValue());
                        } else if (MapField.class.isAssignableFrom(dataModel.getField(fieldName).getClass())) {
                            PropertyUtils.setProperty(bean, fieldName, ((DataSet) fieldValue).toMap());
                        } else if (ListField.class.isAssignableFrom(dataModel.getField(fieldName).getClass())) {
                            List<?> listFieldValue = (List<?>) fieldValue;
                            if (listFieldValue == null) {
                                return;
                            }
                            List<Object> result = new ArrayList<>(listFieldValue.size());
                            for (Object elementValue : listFieldValue) {
                                if (DataSet.class.isAssignableFrom(elementValue.getClass())) {
                                    result.add(((DataSet) elementValue).toRawValue());
                                } else {
                                    result.add(elementValue);
                                }
                            }
                            PropertyUtils.setProperty(bean, fieldName, result);
                        } else {
                            PropertyUtils.setProperty(bean, fieldName, fieldValue);
                        }
                    } catch (Exception e) {
                        log.error("exception when setting {} property {} = {}", rawType, fieldName, fieldValue, e);
                    }
                });
                return bean;
            } catch (Exception e) {
                throw new IllegalArgumentException(rawType.getName() + " can't be instanced.", e);
            }
        }
    }

    public Map<String, Object> toMap() {
        Map<String, Object> result = new HashMap<>(this.data.size());
        for (Map.Entry<String, Object> entry : this.data.entrySet()) {
            Object value = entry.getValue();
            Object resultValue = toRaw(value);
            result.put(entry.getKey(), resultValue);
        }
        return Collections.unmodifiableMap(result);
    }

    @SuppressWarnings("unchecked")
    private Object toRaw(Object value) {
        Object resultValue = value;
        if (value == null) {
            return null;
        } else if (DataSet.class.isAssignableFrom(value.getClass())) {
            resultValue = ((DataSet) value).toMap();
        } else if (List.class.isAssignableFrom(value.getClass())) {
            resultValue = ((List<Object>) value).stream()
                    .map(this::toRaw)
                    .collect(Collectors.toList());
        }
        return resultValue;
    }
}
