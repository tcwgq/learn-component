package com.tcwgq.antlr4.field;

import lombok.Getter;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 字段数据类型
 */
public enum FieldType {
    STRING(String.class, StringField.class),
    BOOLEAN(Boolean.class, BooleanField.class),
    LONG(Long.class, LongField.class),
    INTEGER(Integer.class, IntegerField.class),
    BIG_DECIMAL(BigDecimal.class, BigDecimalField.class),
    MAP(Map.class, MapField.class),
    LIST(List.class, ListField.class),
    OBJECT(Object.class, ObjectField.class);

    private final Class<?> rawType;
    @Getter
    private final Class<? extends ModelField> type;

    FieldType(Class<?> rawType, Class<? extends ModelField> type) {
        this.rawType = rawType;
        this.type = type;
    }

    public static FieldType of(String type) {
        return Arrays.stream(FieldType.values())
                .filter(fieldType -> fieldType.rawType.getName().equals(type))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("field type: " + type));
    }

}
