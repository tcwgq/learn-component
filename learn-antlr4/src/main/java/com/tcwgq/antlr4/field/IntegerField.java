package com.tcwgq.antlr4.field;

import com.tcwgq.antlr4.bean.FieldBean;
import org.apache.commons.lang.math.NumberUtils;

/**
 * {@link Integer}类型的字段
 */
class IntegerField extends SimpleField<Integer> {
    public IntegerField(DataModel belongedDataModel, String name, FieldBean fieldBean) {
        super(belongedDataModel, name, fieldBean);
    }

    @Override
    public Class<?> rawType() {
        return Integer.class;
    }

    @Override
    public Integer cast(Object rawValue) {
        if (rawValue == null) {
            return defaultValue;
        }

        if (isAssignableTo(rawValue.getClass())) {
            return (Integer) rawValue;
        }

        if (Number.class.isAssignableFrom(rawValue.getClass())) {
            return ((Number) rawValue).intValue();
        }

        if (String.class.isAssignableFrom(rawValue.getClass())) {
            String rawValueString = (String) rawValue;
            if (NumberUtils.isNumber(rawValueString)) {
                return NumberUtils.createInteger(rawValueString);
            }
        }
        return defaultValue;
    }
}
