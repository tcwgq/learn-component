package com.tcwgq.antlr4.field;

import com.tcwgq.antlr4.orchestration.Context;
import com.tcwgq.antlr4.orchestration.ValidationResult;
import com.tcwgq.antlr4.bean.FieldBean;
import com.tcwgq.antlr4.orchestration.expression.Expression;
import com.tcwgq.antlr4.orchestration.expression.ExpressionFactory;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

class ListField extends SimpleField<List<Object>> {
    private final DataModel element;
    private final List<Expression> valueExpressions;

    public ListField(DataModel belongedModel, String name, FieldBean fieldBean) {
        super(belongedModel, name, fieldBean);
        this.element = new DataModel(
                belongedModel.getName() + "-" + name + "-inner",
                Collections.singletonMap(getElementName(), fieldBean.getElement()));
        this.valueExpressions = fieldBean.getValues().stream()
                .map(ExpressionFactory::create)
                .collect(Collectors.toList());
    }

    private String getElementName() {
        return getName() + "-element";
    }

    @Override
    public List<Object> evaluate(Context context) {
        if (getLogic().isEmpty()) {
            ModelField<?> elementField = getElementField();
            return valueExpressions.stream()
                    .map(valueExpression -> valueExpression.evaluate(context))
                    .map(elementField::cast)
                    .collect(Collectors.toList());
        } else {
            return super.evaluate(context);
        }
    }

    @Override
    public Class<?> rawType() {
        return List.class;
    }

    @Override
    public List<Object> cast(Object rawValue) {
        if (rawValue == null) {
            return Collections.emptyList();
        }

        if (rawType().isAssignableFrom(rawValue.getClass())) {
            List<?> list = (List<?>) rawValue;
            ModelField<?> elementField = getElementField();
            List<Object> result = new ArrayList<>(list.size());
            for (Object value : list) {
                Object elementValue = elementField.cast(value);
                result.add(elementValue);
            }
            return result;

        }
        return Collections.emptyList();
    }

    private ModelField<?> getElementField() {
        return element.getField(getElementName());
    }

    @SuppressWarnings("unchecked")
    @Override
    public ValidationResult validate(List<Object> data) {
        if (isRequired() && CollectionUtils.isEmpty(data)) {
            return new ValidationResult(this.getName() + " is required but is missing");
        }

        ValidationResult validationResult = new ValidationResult();
        ModelField elementField = getElementField();
        for (Object elementValue : data) {
            validationResult.add(elementField.validate(elementValue));
        }
        return validationResult;
    }
}
