package com.tcwgq.antlr4.field;

import com.tcwgq.antlr4.bean.FieldBean;
import org.apache.commons.lang.math.NumberUtils;

/**
 * {@link Long}类型的字段
 */
class LongField extends SimpleField<Long> {
    public LongField(DataModel belongedDataModel, String name, FieldBean fieldBean) {
        super(belongedDataModel, name, fieldBean);
    }

    @Override
    public Class<?> rawType() {
        return Long.class;
    }

    @Override
    public Long cast(Object rawValue) {
        if (rawValue == null) {
            return defaultValue;
        }
        if (isAssignableTo(rawValue.getClass())) {
            return (Long) rawValue;
        }

        if (Number.class.isAssignableFrom(rawValue.getClass())) {
            return ((Number) rawValue).longValue();
        }

        if (String.class.isAssignableFrom(rawValue.getClass())) {
            String rawValueString = (String) rawValue;
            if (NumberUtils.isNumber(rawValueString)) {
                return NumberUtils.createLong(rawValueString);
            }
        }
        return defaultValue;
    }
}
