package com.tcwgq.antlr4.field;

import com.tcwgq.antlr4.orchestration.Context;
import com.tcwgq.antlr4.orchestration.ValidationResult;
import com.tcwgq.antlr4.bean.FieldBean;
import lombok.Getter;

import java.util.Map;

class MapField extends ModelField<DataSet> {
    @Getter
    private final DataModel dataModel;

    public MapField(DataModel belongedModel, String name, FieldBean fieldBean) {
        super(belongedModel, name, fieldBean);
        this.dataModel = new DataModel(belongedModel.getName() + "-inner-" + name,
                fieldBean.getFields());
        this.dataModel.setRawType(Map.class);
    }

    @Override
    public DataSet evaluate(Context context) {
        return dataModel.evaluate(context);
    }

    @Override
    public Class<?> rawType() {
        return Map.class;
    }

    @Override
    public DataSet cast(Object rawValue) {
        if (rawValue == null) {
            return null;
        }
        return dataModel.cast(rawValue);
    }

    @Override
    public ValidationResult validate(DataSet data) {
        if (isRequired() && data == null) {
            return new ValidationResult(this.getName() + " is required but is missing");
        }
        if (data != null) {
            return this.dataModel.validate(data);
        } else {
            return new ValidationResult();
        }
    }
}
