package com.tcwgq.antlr4.field;

import com.tcwgq.antlr4.orchestration.Castable;
import com.tcwgq.antlr4.orchestration.Context;
import com.tcwgq.antlr4.orchestration.Validatable;
import com.tcwgq.antlr4.bean.FieldBean;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

@Slf4j
@Getter
public abstract class ModelField<T> implements Castable<T>, Validatable<T> {
    private final String name;
    private final DataModel belongedModel;
    private final String description;
    private final boolean required;
    private final FieldType type;

    protected ModelField(DataModel belongedModel, String name, FieldBean fieldBean) {
        Objects.requireNonNull(belongedModel, "belonged model should be missing, name is" + name);
        this.name = name;
        this.belongedModel = belongedModel;
        this.description = fieldBean.getDescription();
        this.required = fieldBean.getRequired() != null && fieldBean.getRequired();
        this.type = FieldType.of(fieldBean.getType());
    }

    /**
     * 计算字段值
     *
     * @param context 上下文
     * @return 字段值
     */
    public abstract T evaluate(Context context);

    /**
     * 原始数据类型
     *
     * @return 原始数据类型
     */
    public abstract Class<?> rawType();

    /**
     * 给定类型是否是本数据类型或本数据类型的父类
     *
     * @param type 给定类型
     * @return 给定类型是否是本数据类型或本数据类型的父类
     */
    public boolean isAssignableTo(Class<?> type) {
        return type.isAssignableFrom(rawType());
    }

    /**
     * $开头是的是无名字段
     *
     * @return 字段名称
     */
    public String getName() {
        return name.startsWith("$") ? this.belongedModel.getName() + name : name;
    }

    @Override
    public String toString() {
        return belongedModel.getName() + "{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", required=" + required +
                ", type=" + type +
                '}';
    }
}
