package com.tcwgq.antlr4.field;

import com.tcwgq.antlr4.orchestration.Context;
import com.tcwgq.antlr4.orchestration.ValidationResult;
import com.tcwgq.antlr4.bean.FieldBean;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.PropertyUtils;

import java.beans.PropertyDescriptor;
import java.util.Arrays;

/**
 * 自定义对象类型的字段
 */
@Slf4j
class ObjectField extends ModelField<DataSet> {
    @Getter
    private final DataModel dataModel;
    private final Class<?> rawType;

    public ObjectField(DataModel belongedModel, String name, FieldBean fieldBean) {
        super(belongedModel, name, fieldBean);
        this.dataModel = new DataModel(belongedModel.getName() + "-inner-" + name,
                fieldBean.getFields());
        try {
            this.rawType = Class.forName(fieldBean.getRef());
        } catch (ClassNotFoundException e) {
            log.error("wrong config, ref in {}.{} is {} but can't be found",
                    belongedModel.getName(), name, fieldBean.getRef(), e);
            throw new IllegalArgumentException(
                    "wrong config, ref in " + belongedModel.getName() + "." + name +
                            " is " + fieldBean.getRef() + " but can't be found");
        }
        this.dataModel.setRawType(this.rawType);

        PropertyDescriptor[] propertyDescriptors = PropertyUtils.getPropertyDescriptors(this.rawType);

        this.dataModel.getFields().values().stream()
                .filter(field -> Arrays.stream(propertyDescriptors)
                        .noneMatch(propertyDescriptor -> propertyDescriptor.getName().equals(field.getName())))
                .findAny()
                .ifPresent(field -> {
                    throw new IllegalArgumentException(
                            "can't find property " + field.getName() + " in " + this.rawType.getName());
                });
    }

    @Override
    public DataSet evaluate(Context context) {
        return dataModel.evaluate(context);
    }

    @Override
    public Class<?> rawType() {
        return rawType;
    }

    @Override
    public DataSet cast(Object rawValue) {
        if (rawValue == null) {
            return null;
        }
        return dataModel.cast(rawValue);
    }

    @Override
    public ValidationResult validate(DataSet data) {
        if (isRequired() && data == null) {
            return new ValidationResult(this.getName() + " is required but is missing");
        }
        if (data != null) {
            return this.dataModel.validate(data);
        } else {
            return new ValidationResult();
        }
    }
}
