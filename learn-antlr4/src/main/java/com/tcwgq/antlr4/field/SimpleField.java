package com.tcwgq.antlr4.field;

import com.tcwgq.antlr4.orchestration.Context;
import com.tcwgq.antlr4.orchestration.ValidationResult;
import com.tcwgq.antlr4.bean.FieldBean;
import com.tcwgq.antlr4.orchestration.expression.Expression;
import com.tcwgq.antlr4.orchestration.expression.ExpressionFactory;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * 简单类型的字段
 */
@Slf4j
public abstract class SimpleField<T> extends ModelField<T> {
    @Getter
    private final Expression logic;
    protected final T defaultValue;

    public SimpleField(DataModel belongedDataModel, String name, FieldBean fieldBean) {
        super(belongedDataModel, name, fieldBean);
        this.logic = ExpressionFactory.create(fieldBean.getLogic());
        this.defaultValue = cast(fieldBean.getDefaultValue());
    }

    @Override
    public T evaluate(Context context) {
        log.debug("evaluate field {}", this);
        Object result;
        if (logic.isEmpty()) {
            result = context.getInput(getName());
        } else {
            result = logic.evaluate(context);
        }
        return result == null ? defaultValue : cast(result);
    }

    @Override
    public ValidationResult validate(T data) {
        if (isRequired() && data == null) {
            return new ValidationResult(this.getName() + " is required but is missing");
        }
        return new ValidationResult();
    }
}
