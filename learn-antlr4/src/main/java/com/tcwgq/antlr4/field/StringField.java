package com.tcwgq.antlr4.field;

import com.tcwgq.antlr4.bean.FieldBean;

/**
 * {@link String}类型的字段
 */
final class StringField extends SimpleField<String> {
    public StringField(DataModel belongedDataModel, String name, FieldBean fieldBean) {
        super(belongedDataModel, name, fieldBean);
    }

    @Override
    public Class<?> rawType() {
        return String.class;
    }

    @Override
    public String cast(Object rawValue) {
        if (rawValue == null) {
            return defaultValue;
        }
        return rawValue.toString();
    }
}
