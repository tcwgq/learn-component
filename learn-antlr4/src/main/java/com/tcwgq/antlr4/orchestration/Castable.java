package com.tcwgq.antlr4.orchestration;

@FunctionalInterface
public interface Castable<T> {
    /**
     * 转换数据
     *
     * @param rawValue 原始数据
     * @return 转换后的数据
     */
    T cast(Object rawValue);
}
