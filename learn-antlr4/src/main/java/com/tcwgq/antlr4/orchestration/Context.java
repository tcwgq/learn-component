package com.tcwgq.antlr4.orchestration;

import com.tcwgq.antlr4.field.DataSet;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

/**
 * 执行计划计算上下文
 */
public class Context {
    public static final String INPUTS = "inputs";

    private final Map<String, DataSet> dataMap;
    @Getter
    @Setter
    private Object by;

    public Context() {
        this.dataMap = new HashMap<>();
    }

    public Context(Context context) {
        this.dataMap = new HashMap<>(context.dataMap);
        this.by = null;
    }

    public static Context singleton(DataSet dataSet) {
        return new SingleContext(dataSet);
    }

    public void addInputData(DataSet dataSet) {
        this.dataMap.put(INPUTS, dataSet);
    }

    public void addData(String name, DataSet dataSet) {
        this.dataMap.put(name, dataSet);
    }

    public Object getInput(String fieldName) {
        if (this.dataMap.containsKey(INPUTS)) {
            return this.dataMap.get(INPUTS).getFieldValue(fieldName);
        } else {
            return null;
        }
    }

    public DataSet getData(String name) {
        return this.dataMap.get(name);
    }

    private static class SingleContext extends Context {
        public SingleContext(DataSet dataSet) {
            super();
            super.addInputData(dataSet);
        }

        @Override
        public void addInputData(DataSet dataSet) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void addData(String name, DataSet dataSet) {
            throw new UnsupportedOperationException();
        }

        @Override
        public Object getInput(String fieldName) {
            return super.getInput(fieldName);
        }

        @Override
        public DataSet getData(String name) {
            return super.getData(name);
        }
    }
}
