package com.tcwgq.antlr4.orchestration;

import com.tcwgq.antlr4.bean.NodeBean;
import com.tcwgq.antlr4.field.DataSet;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;

import java.util.*;

@Slf4j
@Getter
public class ExecutionPlan {
    private final String name;
    private final Node startNode;
    private final int size;

    public ExecutionPlan(String name, Map<String, NodeBean> plan) {
        this.name = name;
        this.startNode = buildPlan(plan);
        this.size = plan.size();

        if (!validate(this.startNode.getNext())) {
            throw new IllegalArgumentException("wrong execution plan config: " + this.name);
        }
    }

    private Node buildPlan(Map<String, NodeBean> plan) {
        Node beginNode = Node.createStartNode();
        Map<String, Node> nodeMap = new HashMap<>(plan.size() + 2);
        nodeMap.put("", beginNode);
        nodeMap.put(null, beginNode);

        for (Map.Entry<String, NodeBean> entry : plan.entrySet()) {
            String nodeName = entry.getKey();
            NodeBean nodeBean = entry.getValue();

            Node node = Node.create(nodeBean);

            if (nodeMap.containsKey(nodeName)) {
                nodeMap.get(nodeName).replaceBy(node);
            } else {
                nodeMap.put(nodeName, node);
            }

            List<String> previousNodeNames = nodeBean.getPrevious();

            if (CollectionUtils.isEmpty(previousNodeNames)) {
                beginNode.addNext(node);
            }

            previousNodeNames.forEach(previousNodeName -> {
                Node previousNode = nodeMap.getOrDefault(previousNodeName, Node.createDummyNode(previousNodeName));
                previousNode.addNext(node);
                nodeMap.put(previousNode.getName(), previousNode);
            });
        }
        return beginNode;
    }

    private boolean validate(List<Node> nodeList) {
        for (Node node : nodeList) {
            if (node.isDummy()) {
                return false;
            }
            validate(node.getNext());
        }
        return true;
    }

    public void execute(Context context) {
        Set<String> executedNode = new HashSet<>(size);
        executedNode.add(this.startNode.getName());
        executeNode(context, this.startNode, executedNode);
    }

    private void executeNode(Context context, Node node, Set<String> executedNode) {
        boolean parentHasExecuted = node.getPrevious().stream()
                .allMatch(n -> executedNode.contains(n.getName()));
        if (!parentHasExecuted) {
            return;
        }

        DataSet nodeResult = node.execute(context);
        context.addData(node.getName(), nodeResult);

        executedNode.add(node.getName());

        node.getNext().forEach(n -> executeNode(context, n, executedNode));
    }
}
