package com.tcwgq.antlr4.orchestration;

import com.tcwgq.antlr4.exception.FusionException;
import com.tcwgq.antlr4.exception.FusionReturn;
import com.tcwgq.antlr4.bean.NodeBean;
import com.tcwgq.antlr4.field.DataSet;
import com.tcwgq.antlr4.field.ModelField;
import com.tcwgq.antlr4.response.DataResponse;
import com.tcwgq.antlr4.response.ServiceResponse;
import com.tcwgq.antlr4.util.ContextAware;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.exception.ExceptionUtils;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

@Slf4j
public class FeignNode extends Node {
    private final String clientBeanName;
    private final String methodName;
    private final boolean fastFailure;
    private final Object client;
    private final Method method;

    public FeignNode(String name, NodeBean nodeBean) {
        super(name, nodeBean);
        this.clientBeanName = nodeBean.getClient();
        this.methodName = nodeBean.getMethod();

        Object bean = ContextAware.getApplicationContext().getBean(this.clientBeanName);
        Method[] methods = bean.getClass().getMethods();
        this.fastFailure = nodeBean.getFastFailure();
        this.client = bean;
        this.method = Arrays.stream(methods)
                .filter(m -> {
                    if (!m.getName().equals(this.methodName)) {
                        return false;
                    }
                    if (m.getParameterCount() != getParamModel().getFieldSize()) {
                        return false;
                    }
                    Parameter[] parameters = m.getParameters();
                    for (int i = 0; i < m.getParameterCount(); i++) {
                        ModelField<?> field = getParamModel().getField(getParamModel().getName() + "$" + i);
                        if (!field.isAssignableTo(parameters[i].getType())) {
                            return false;
                        }
                    }
                    return true;
                })
                .findFirst()
                .orElseThrow(() -> new UnsupportedOperationException(
                        "feign node operation: " + clientBeanName + "." + methodName));
        this.method.setAccessible(true);
    }

    @Override
    public DataSet execute(Context context) {
        ValidationResult validationResult = new ValidationResult();
        Object[] args = castParameters(context, validationResult);

        DataResponse<Object> result = null;
        if (validationResult.isNotEmpty()) {
            if (fastFailure) {
                throw new FusionException(FusionReturn.WRONG_BACKEND_PARAM, validationResult.getMessage());
            }
        } else {
            result = call(context, args);
        }

        if (result != null && ServiceResponse.SUCCESS_CODE.equals(result.getCode())) {
            Object data = result.getData();
            Map<String, Object> dataMap = Collections.singletonMap("result", data);
            return getResultModel().cast(dataMap);
        } else {
            log.error("exception when execute node {} with context {}", getName(), context);
            String message;
            if (result == null) {
                message = "response is missing";
            } else {
                message = "response code: " + result.getCode() + ", message: " + result.getResultMessage();
            }

            if (fastFailure) {
                throw new FusionException(FusionReturn.BACKEND_FAILED, message);
            }
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    private DataResponse<Object> call(Context context, Object[] args) {
        DataResponse<Object> result = null;
        try {
            Object rawResponse = method.invoke(client, args);
            if (rawResponse != null) {
                if (DataResponse.class.isAssignableFrom(rawResponse.getClass())) {
                    result = (DataResponse<Object>) rawResponse;
                } else {
                    result = new DataResponse<>(rawResponse);
                }
            }
        } catch (Exception e) {
            log.error("exception when execute node {} with context {}", getName(), context, e);
            if (fastFailure) {
                throw new FusionException(FusionReturn.BACKEND_FAILED,
                        "client: " + this.clientBeanName + ", method: " + this.methodName +
                                ", exception: " + ExceptionUtils.getRootCauseMessage(e),
                        e);
            }
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    private Object[] castParameters(Context context, ValidationResult validationResult) {
        Object[] args = new Object[method.getParameterCount()];
        for (int i = 0; i < method.getParameterCount(); i++) {
            ModelField field = getParamModel().getField(getParamModel().getName() + "$" + i);
            Object fieldValue = field.evaluate(context);

            validationResult.add(field.validate(fieldValue));

            if (fieldValue == null) {
                args[i] = null;
            } else if (DataSet.class.isAssignableFrom(fieldValue.getClass())) {
                args[i] = ((DataSet) fieldValue).toRawValue();
            } else {
                args[i] = fieldValue;
            }
        }
        return args;
    }
}
