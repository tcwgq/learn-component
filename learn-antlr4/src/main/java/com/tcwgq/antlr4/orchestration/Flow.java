package com.tcwgq.antlr4.orchestration;

import com.tcwgq.antlr4.exception.FusionException;
import com.tcwgq.antlr4.exception.FusionReturn;
import com.tcwgq.antlr4.bean.FlowBean;
import com.tcwgq.antlr4.field.DataModel;
import com.tcwgq.antlr4.field.DataSet;
import lombok.Getter;

import java.util.Map;
import java.util.Objects;

/**
 * 处理流程
 */
@Getter
public class Flow {
    private final String name;
    private final String path;
    private final DataModel input;
    private final ExecutionPlan plan;
    private final DataModel output;

    public Flow(String name, FlowBean flowBean) {
        this.name = name;
        Objects.requireNonNull(flowBean.getPath(), "FLOW绑定的URL路径不能为空");
        this.path = flowBean.getPath();
        this.input = new DataModel(name + "-inputs", flowBean.getInputs());
        this.plan = new ExecutionPlan(name + "-plan", flowBean.getPlan());
        this.output = new DataModel(name + "-outputs", flowBean.getOutputs());
        // TODO 校验input, plan, output之间的依赖关系，特别是参数依赖
    }

    public DataSet execute(Map<String, Object> request) {
        DataSet dataSet = input.cast(request);
        ValidationResult validationResult = dataSet.validate();
        if (validationResult.isNotEmpty()) {
            throw new FusionException(FusionReturn.WRONG_FIELD, validationResult.getMessage());
        }

        Context context = new Context();
        context.addInputData(dataSet);

        plan.execute(context);

        return output.evaluate(context);
    }
}
