package com.tcwgq.antlr4.orchestration;

import com.tcwgq.antlr4.bean.NodeBean;
import com.tcwgq.antlr4.orchestration.expression.Expression;
import com.tcwgq.antlr4.orchestration.expression.ExpressionFactory;
import com.tcwgq.antlr4.field.DataSet;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class LoopNode extends Node {
    private Expression by;

    private ExecutionPlan innerPlan;


    public LoopNode(String name, NodeBean nodeBean) {
        super(name, nodeBean);
        this.by = ExpressionFactory.create(nodeBean.getBy());
        Map<String, NodeBean> plan = nodeBean.getEachNodes().stream()
                .collect(Collectors.toMap(NodeBean::getName, Function.identity()));
        this.innerPlan = new ExecutionPlan(name + "-inner-plan", plan);
    }

    @Override
    public DataSet execute(Context context) {
        Object byValue = this.by.evaluate(context);
        if (byValue == null || !Collection.class.isAssignableFrom(byValue.getClass())) {
            return null;
        }

        Collection<?> byCollection = (Collection<?>) byValue;
        List<DataSet> result = byCollection.parallelStream()
                .map(eachKey -> {
                    Context innerContext = new Context(context);
                    innerContext.setBy(eachKey);
                    innerPlan.execute(innerContext);
                    return getResultModel().evaluate(innerContext);
                })
                .collect(Collectors.toList());
        return DataSet.singleton("result", result);
    }
}
