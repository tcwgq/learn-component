package com.tcwgq.antlr4.orchestration;

import com.tcwgq.antlr4.bean.NodeBean;
import com.tcwgq.antlr4.field.DataModel;
import com.tcwgq.antlr4.field.DataSet;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Constructor;
import java.util.*;

/**
 * 节点
 */
@Slf4j
@Getter
public abstract class Node {
    private final String name;
    private NodeType type;
    private DataModel paramModel;
    private DataModel resultModel;

    private final Map<String, Node> previous = new HashMap<>();
    private final Map<String, Node> next = new HashMap<>();

    private Node(String name) {
        this.name = name;
    }

    public Node(String name, NodeBean nodeBean) {
        this.name = name;
        this.type = NodeType.of(nodeBean.getType());
        this.paramModel = new DataModel("plan-" + name + "-params-model", nodeBean.getParams());
        this.resultModel = new DataModel("plan-" + name + "-results-model", Collections.singletonMap("result", nodeBean.getResults()));
    }

    public static Node createStartNode() {
        return new Node("START") {
            @Override
            public DataSet execute(Context context) {
                return null;
            }
        };
    }

    public static Node createDummyNode(String name) {
        return new Node(name) {
            @Override
            public DataSet execute(Context context) {
                throw new UnsupportedOperationException();
            }

            @Override
            public boolean isDummy() {
                return true;
            }
        };
    }

    public void addNext(Node node) {
        node.previous.put(this.getName(), this);
        this.next.put(node.getName(), node);
    }

    public boolean isFirst() {
        return previous.isEmpty();
    }

    public boolean isLast() {
        return next.isEmpty();
    }

    public List<Node> getPrevious() {
        return Collections.unmodifiableList(new ArrayList<>(previous.values()));
    }

    public List<Node> getNext() {
        return Collections.unmodifiableList(new ArrayList<>(next.values()));
    }

    public void replaceBy(Node newNode) {
        Node oldNode = this;

        for (Iterator<Map.Entry<String, Node>> iterator = oldNode.previous.entrySet().iterator(); iterator.hasNext(); ) {
            Node preNode = iterator.next().getValue();
            // 等同于 oldNode.previous.remove(preNode)
            iterator.remove();
            preNode.next.remove(oldNode.getName());

            preNode.addNext(newNode);
        }

        for (Iterator<Map.Entry<String, Node>> iterator = oldNode.next.entrySet().iterator(); iterator.hasNext(); ) {
            Node nextNode = iterator.next().getValue();
            nextNode.previous.remove(oldNode.getName());
            // 等同于 oldNode.next.remove(nextNode)
            iterator.remove();

            newNode.addNext(nextNode);
        }
    }

    /**
     * 执行节点计算
     *
     * @param context 计算上下文
     */
    public abstract DataSet execute(Context context);

    public boolean isDummy() {
        return false;
    }

    public static Node create(NodeBean nodeBean) {
        Node node;
        try {
            Class<? extends Node> nodeTypeClass = NodeType.of(nodeBean.getType()).getNodeClass();
            Constructor<? extends Node> constructor = nodeTypeClass.getConstructor(String.class, NodeBean.class);
            constructor.setAccessible(true);
            node = constructor.newInstance(nodeBean.getName(), nodeBean);
        } catch (Exception e) {
            log.warn("exception when creating node {}", nodeBean.getName(), e);
            throw new IllegalArgumentException("wrong implementation: " + nodeBean.getName(), e);
        }
        return node;
    }
}
