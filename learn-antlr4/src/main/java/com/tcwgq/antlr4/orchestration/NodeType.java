package com.tcwgq.antlr4.orchestration;

import lombok.Getter;

import java.util.Arrays;

/**
 * 执行计划节点类型
 */
@Getter
public enum NodeType {
    FEIGN(FeignNode.class),
    LOOP(LoopNode.class);

    private final Class<? extends Node> nodeClass;

    NodeType(Class<? extends Node> nodeClass) {
        this.nodeClass = nodeClass;
    }

    public static NodeType of(String type) {
        return Arrays.stream(values())
                .filter(nodeType -> nodeType.name().equalsIgnoreCase(type))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("unknown node type: " + type));
    }
}
