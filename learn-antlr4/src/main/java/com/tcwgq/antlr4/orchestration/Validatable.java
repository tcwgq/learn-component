package com.tcwgq.antlr4.orchestration;

@FunctionalInterface
public interface Validatable<T> {
    /**
     * 校验数据
     *
     * @param data 待校验的数据
     * @return 是否通过
     */
    ValidationResult validate(T data);
}
