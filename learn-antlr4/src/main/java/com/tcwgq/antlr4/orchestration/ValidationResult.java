package com.tcwgq.antlr4.orchestration;

import java.util.ArrayList;
import java.util.List;

public class ValidationResult {
    private final List<String> messages = new ArrayList<>();

    public ValidationResult() {
    }

    public ValidationResult(String message) {
        this.messages.add(message);
    }

    public void add(ValidationResult validationResult) {
        this.messages.addAll(validationResult.messages);
    }

    public boolean isNotEmpty() {
        return !this.messages.isEmpty();
    }

    public String getMessage() {
        return String.join("\n", messages);
    }
}
