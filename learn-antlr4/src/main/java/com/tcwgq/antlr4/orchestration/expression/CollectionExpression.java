package com.tcwgq.antlr4.orchestration.expression;

import com.tcwgq.antlr4.exception.FusionException;
import com.tcwgq.antlr4.exception.FusionReturn;
import com.tcwgq.antlr4.orchestration.Context;
import lombok.Getter;

import java.util.List;

/**
 * 属性表达式，如a.b
 */
@Getter
public class CollectionExpression implements Expression {
    private final Expression parent;
    private final int index;

    public CollectionExpression(Expression parent, String index) {
        this.parent = parent;
        this.index = Integer.parseInt(index);
    }

    @Override
    public Object evaluate(Context context) {
        Object parentValue = this.parent.evaluate(context);
        if (parentValue == null) {
            return null;
        }
        if (!List.class.isAssignableFrom(parentValue.getClass())) {
            throw new FusionException(FusionReturn.WRONG_CONFIG, "result of " + parentValue.toString() + " is not List");
        }
        List<?> parentList = (List<?>) parentValue;
        if (index >= parentList.size()) {
            return null;
        }
        return parentList.get(index);
    }

    @Override
    public String toString() {
        return parent.toString() + "._" + index;
    }
}
