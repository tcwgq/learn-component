package com.tcwgq.antlr4.orchestration.expression;

import com.tcwgq.antlr4.orchestration.Context;

public final class EmptyExpression implements Expression {
    public static final EmptyExpression EMPTY_EXPRESSION = new EmptyExpression();

    private EmptyExpression() {
    }

    public static EmptyExpression create() {
        return EMPTY_EXPRESSION;
    }

    @Override
    public Object evaluate(Context context) {
        return null;
    }

    @Override
    public boolean isEmpty() {
        return true;
    }
}
