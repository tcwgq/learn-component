package com.tcwgq.antlr4.orchestration.expression;

import com.tcwgq.antlr4.orchestration.Context;

public class EqualsExpression implements Expression {
    private final Expression left;
    private final Expression right;

    public EqualsExpression(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public Object evaluate(Context context) {
        Object leftValue = left.evaluate(context);
        Object rightValue = right.evaluate(context);
        if (leftValue == null) {
            return rightValue == null;
        } else {
            return leftValue.equals(rightValue);
        }
    }

    @Override
    public String toString() {
        return left + " == " + right;
    }
}
