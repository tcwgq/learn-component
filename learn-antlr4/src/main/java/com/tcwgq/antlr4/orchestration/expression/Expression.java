package com.tcwgq.antlr4.orchestration.expression;

import com.tcwgq.antlr4.orchestration.Context;

/**
 * 表达式基础类
 */
public interface Expression {
    /**
     * 计算表达式
     *
     * @param context 计算上下文
     * @return 结算结果
     */
    Object evaluate(Context context);

    /**
     * 是否是空表达式
     *
     * @return 是否是空表达式
     */
    default boolean isEmpty() {
        return false;
    }
}
