package com.tcwgq.antlr4.orchestration.expression;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.apache.commons.lang.StringUtils;

public class ExpressionFactory {
    private ExpressionFactory() {
    }

    public static Expression create(String logic) {
        if (StringUtils.isEmpty(logic)) {
            return EmptyExpression.create();
        }
        CharStream inputStream = CharStreams.fromString(logic);
        ExpressionLexer lexer = new ExpressionLexer(inputStream);
        CommonTokenStream tokenStream = new CommonTokenStream(lexer);
        ExpressionParser parser = new ExpressionParser(tokenStream);
        ParseTree parseTree = parser.propertyExpr();
        ExpressionVisitorImpl visitor = new ExpressionVisitorImpl();
        return visitor.visit(parseTree);
    }
}
