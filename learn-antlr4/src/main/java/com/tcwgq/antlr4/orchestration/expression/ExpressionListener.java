// Generated from /Users/yangshuan/projects/aihuishou/risk/riskfusion/riskfusion-service/src/main/resources/Expression.g4 by ANTLR 4.8
package com.tcwgq.antlr4.orchestration.expression;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ExpressionParser}.
 */
public interface ExpressionListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by the {@code variableProperty}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 */
	void enterVariableProperty(ExpressionParser.VariablePropertyContext ctx);
	/**
	 * Exit a parse tree produced by the {@code variableProperty}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 */
	void exitVariableProperty(ExpressionParser.VariablePropertyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code subProperty}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 */
	void enterSubProperty(ExpressionParser.SubPropertyContext ctx);
	/**
	 * Exit a parse tree produced by the {@code subProperty}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 */
	void exitSubProperty(ExpressionParser.SubPropertyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code equalsExpression}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 */
	void enterEqualsExpression(ExpressionParser.EqualsExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code equalsExpression}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 */
	void exitEqualsExpression(ExpressionParser.EqualsExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stringExpression}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 */
	void enterStringExpression(ExpressionParser.StringExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stringExpression}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 */
	void exitStringExpression(ExpressionParser.StringExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code mapValueExpression}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 */
	void enterMapValueExpression(ExpressionParser.MapValueExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code mapValueExpression}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 */
	void exitMapValueExpression(ExpressionParser.MapValueExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code numberExpression}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 */
	void enterNumberExpression(ExpressionParser.NumberExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code numberExpression}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 */
	void exitNumberExpression(ExpressionParser.NumberExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nullExpression}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 */
	void enterNullExpression(ExpressionParser.NullExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nullExpression}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 */
	void exitNullExpression(ExpressionParser.NullExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ternaryExpression}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 */
	void enterTernaryExpression(ExpressionParser.TernaryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ternaryExpression}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 */
	void exitTernaryExpression(ExpressionParser.TernaryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code functionTransform}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 */
	void enterFunctionTransform(ExpressionParser.FunctionTransformContext ctx);
	/**
	 * Exit a parse tree produced by the {@code functionTransform}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 */
	void exitFunctionTransform(ExpressionParser.FunctionTransformContext ctx);
	/**
	 * Enter a parse tree produced by the {@code collectionProperty}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 */
	void enterCollectionProperty(ExpressionParser.CollectionPropertyContext ctx);
	/**
	 * Exit a parse tree produced by the {@code collectionProperty}
	 * labeled alternative in {@link ExpressionParser#propertyExpr}.
	 * @param ctx the parse tree
	 */
	void exitCollectionProperty(ExpressionParser.CollectionPropertyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code variable}
	 * labeled alternative in {@link ExpressionParser#variableExpr}.
	 * @param ctx the parse tree
	 */
	void enterVariable(ExpressionParser.VariableContext ctx);
	/**
	 * Exit a parse tree produced by the {@code variable}
	 * labeled alternative in {@link ExpressionParser#variableExpr}.
	 * @param ctx the parse tree
	 */
	void exitVariable(ExpressionParser.VariableContext ctx);
	/**
	 * Enter a parse tree produced by the {@code functionWithParams}
	 * labeled alternative in {@link ExpressionParser#functionExpr}.
	 * @param ctx the parse tree
	 */
	void enterFunctionWithParams(ExpressionParser.FunctionWithParamsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code functionWithParams}
	 * labeled alternative in {@link ExpressionParser#functionExpr}.
	 * @param ctx the parse tree
	 */
	void exitFunctionWithParams(ExpressionParser.FunctionWithParamsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code functionWithoutParam}
	 * labeled alternative in {@link ExpressionParser#functionExpr}.
	 * @param ctx the parse tree
	 */
	void enterFunctionWithoutParam(ExpressionParser.FunctionWithoutParamContext ctx);
	/**
	 * Exit a parse tree produced by the {@code functionWithoutParam}
	 * labeled alternative in {@link ExpressionParser#functionExpr}.
	 * @param ctx the parse tree
	 */
	void exitFunctionWithoutParam(ExpressionParser.FunctionWithoutParamContext ctx);
	/**
	 * Enter a parse tree produced by the {@code propertyParam}
	 * labeled alternative in {@link ExpressionParser#paramExprs}.
	 * @param ctx the parse tree
	 */
	void enterPropertyParam(ExpressionParser.PropertyParamContext ctx);
	/**
	 * Exit a parse tree produced by the {@code propertyParam}
	 * labeled alternative in {@link ExpressionParser#paramExprs}.
	 * @param ctx the parse tree
	 */
	void exitPropertyParam(ExpressionParser.PropertyParamContext ctx);
	/**
	 * Enter a parse tree produced by the {@code functionParam}
	 * labeled alternative in {@link ExpressionParser#paramExprs}.
	 * @param ctx the parse tree
	 */
	void enterFunctionParam(ExpressionParser.FunctionParamContext ctx);
	/**
	 * Exit a parse tree produced by the {@code functionParam}
	 * labeled alternative in {@link ExpressionParser#paramExprs}.
	 * @param ctx the parse tree
	 */
	void exitFunctionParam(ExpressionParser.FunctionParamContext ctx);
	/**
	 * Enter a parse tree produced by the {@code multiParams}
	 * labeled alternative in {@link ExpressionParser#paramExprs}.
	 * @param ctx the parse tree
	 */
	void enterMultiParams(ExpressionParser.MultiParamsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code multiParams}
	 * labeled alternative in {@link ExpressionParser#paramExprs}.
	 * @param ctx the parse tree
	 */
	void exitMultiParams(ExpressionParser.MultiParamsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code functionPath}
	 * labeled alternative in {@link ExpressionParser#functionParamExpr}.
	 * @param ctx the parse tree
	 */
	void enterFunctionPath(ExpressionParser.FunctionPathContext ctx);
	/**
	 * Exit a parse tree produced by the {@code functionPath}
	 * labeled alternative in {@link ExpressionParser#functionParamExpr}.
	 * @param ctx the parse tree
	 */
	void exitFunctionPath(ExpressionParser.FunctionPathContext ctx);
	/**
	 * Enter a parse tree produced by the {@code innerFunction}
	 * labeled alternative in {@link ExpressionParser#functionParamExpr}.
	 * @param ctx the parse tree
	 */
	void enterInnerFunction(ExpressionParser.InnerFunctionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code innerFunction}
	 * labeled alternative in {@link ExpressionParser#functionParamExpr}.
	 * @param ctx the parse tree
	 */
	void exitInnerFunction(ExpressionParser.InnerFunctionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code functionMultiPath}
	 * labeled alternative in {@link ExpressionParser#functionParamExpr}.
	 * @param ctx the parse tree
	 */
	void enterFunctionMultiPath(ExpressionParser.FunctionMultiPathContext ctx);
	/**
	 * Exit a parse tree produced by the {@code functionMultiPath}
	 * labeled alternative in {@link ExpressionParser#functionParamExpr}.
	 * @param ctx the parse tree
	 */
	void exitFunctionMultiPath(ExpressionParser.FunctionMultiPathContext ctx);
}