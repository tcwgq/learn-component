package com.tcwgq.antlr4.orchestration.expression;

public class ExpressionVisitorImpl extends ExpressionBaseVisitor<Expression> {
    @Override
    public Expression visitSubProperty(ExpressionParser.SubPropertyContext ctx) {
        Expression parent = visit(ctx.propertyExpr());
        String path = ctx.ID().getText();
        return new PropertyExpression(parent, path);
    }

    @Override
    public Expression visitCollectionProperty(ExpressionParser.CollectionPropertyContext ctx) {
        Expression parent = visit(ctx.propertyExpr());
        String index = ctx.NUMBER().getText();
        return new CollectionExpression(parent, index);
    }

    @Override
    public Expression visitFunctionTransform(ExpressionParser.FunctionTransformContext ctx) {
        Expression parent = visit(ctx.propertyExpr());
        FunctionExpression function = (FunctionExpression) visit(ctx.functionExpr());
        function.setParent(parent);
        return function;
    }

    @Override
    public Expression visitVariableProperty(ExpressionParser.VariablePropertyContext ctx) {
        return visit(ctx.variableExpr());
    }

    @Override
    public Expression visitTernaryExpression(ExpressionParser.TernaryExpressionContext ctx) {
        return new TernaryExpression(
                visit(ctx.propertyExpr(0)),
                visit(ctx.propertyExpr(1)),
                visit(ctx.propertyExpr(2)));
    }

    @Override
    public Expression visitNumberExpression(ExpressionParser.NumberExpressionContext ctx) {
        return new NumberExpression(ctx.NUMBER().getText());
    }

    @Override
    public Expression visitVariable(ExpressionParser.VariableContext ctx) {
        return new VariableExpression(ctx.ID().getText());
    }

    @Override
    public Expression visitFunctionWithParams(ExpressionParser.FunctionWithParamsContext ctx) {
        String functionName = ctx.ID().getText();
        Expression paramExpression = visit(ctx.paramExprs());
        return new FunctionExpression(functionName, paramExpression);
    }

    @Override
    public Expression visitFunctionWithoutParam(ExpressionParser.FunctionWithoutParamContext ctx) {
        String functionName = ctx.ID().getText();
        return new FunctionExpression(functionName);
    }

    @Override
    public Expression visitMultiParams(ExpressionParser.MultiParamsContext ctx) {
        Expression paramsExpression1 = visit(ctx.paramExprs(0));
        Expression paramsExpression2 = visit(ctx.paramExprs(1));
        return new FunctionParamsExpression(paramsExpression1, paramsExpression2);
    }

    @Override
    public Expression visitPropertyParam(ExpressionParser.PropertyParamContext ctx) {
        Expression expression = visit(ctx.propertyExpr());
        return new FunctionParamExpression(expression);
    }

    @Override
    public Expression visitFunctionParam(ExpressionParser.FunctionParamContext ctx) {
        Expression expression = visit(ctx.functionParamExpr());
        return new FunctionParamExpression(expression);
    }

    @Override
    public Expression visitFunctionMultiPath(ExpressionParser.FunctionMultiPathContext ctx) {
        Expression expression = visit(ctx.functionParamExpr());
        String path = ctx.ID().getText();
        return new PropertyExpression(expression, path);
    }

    @Override
    public Expression visitInnerFunction(ExpressionParser.InnerFunctionContext ctx) {
        Expression param = visit(ctx.functionParamExpr());
        FunctionExpression function = (FunctionExpression) visit(ctx.functionExpr());
        function.setParent(param);
        return function;
    }

    @Override
    public Expression visitFunctionPath(ExpressionParser.FunctionPathContext ctx) {
        String path = ctx.ID().getText();
        return new PropertyExpression(new IdentityExpression(), path);
    }

    @Override
    public Expression visitEqualsExpression(ExpressionParser.EqualsExpressionContext ctx) {
        Expression left = visit(ctx.propertyExpr(0));
        Expression right = visit(ctx.propertyExpr(1));
        return new EqualsExpression(left, right);
    }

    @Override
    public Expression visitNullExpression(ExpressionParser.NullExpressionContext ctx) {
        return new NullExpression();
    }

    @Override
    public Expression visitMapValueExpression(ExpressionParser.MapValueExpressionContext ctx) {
        return new MapValueExpression(visit(ctx.propertyExpr(0)), visit(ctx.propertyExpr(1)));
    }

    @Override
    public Expression visitStringExpression(ExpressionParser.StringExpressionContext ctx) {
        String text = ctx.STRING().getText();
        text = text.substring(1, text.length() - 1);
        return new StringExpression(text);
    }
}
