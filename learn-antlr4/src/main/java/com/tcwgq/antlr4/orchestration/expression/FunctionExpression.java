package com.tcwgq.antlr4.orchestration.expression;

import com.tcwgq.antlr4.orchestration.Context;
import com.tcwgq.antlr4.orchestration.function.FunctionRepository;
import com.tcwgq.antlr4.orchestration.function.RiskFunction;
import lombok.Setter;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 函数表达式
 */
public class FunctionExpression implements Expression {
    private final RiskFunction function;
    private final List<FunctionParamExpression> paramExpressionList;

    @Setter
    private Expression parent;

    public FunctionExpression(String name) {
        this(name, Collections.emptyList());
    }

    public FunctionExpression(String name, Expression expression) {
        this.function = FunctionRepository.getByName(name);
        if (FunctionParamsExpression.class.isAssignableFrom(expression.getClass())) {
            this.paramExpressionList = Collections.unmodifiableList(((FunctionParamsExpression) expression).get());
        } else if (FunctionParamExpression.class.isAssignableFrom(expression.getClass())) {
            this.paramExpressionList = Collections.unmodifiableList(
                    Collections.singletonList((FunctionParamExpression) expression));
        } else {
            this.paramExpressionList = Collections.emptyList();
        }
    }

    public <T> FunctionExpression(String name, List<FunctionParamExpression> paramExpressionList) {
        this.function = FunctionRepository.getByName(name);
        this.paramExpressionList = Collections.unmodifiableList(paramExpressionList);
    }

    @Override
    public Object evaluate(Context context) {
        Object source = parent.evaluate(context);
        if (source == null) {
            return null;
        }
        return function.apply(context, source, paramExpressionList);
    }

    @Override
    public String toString() {
        return (parent != null ? parent.toString() + '.' : "") +
                function.getName() +
                '(' +
                paramExpressionList.stream().map(Expression::toString).collect(Collectors.joining(", ")) +
                ')';
    }
}
