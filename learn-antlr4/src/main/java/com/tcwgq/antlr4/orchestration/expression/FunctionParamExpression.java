package com.tcwgq.antlr4.orchestration.expression;

import com.tcwgq.antlr4.orchestration.Context;

/**
 * 函数参数表达式
 */
public class FunctionParamExpression implements Expression {
    private final Expression expression;

    public FunctionParamExpression(Expression expression) {
        this.expression = expression;
    }

    @Override
    public Object evaluate(Context context) {
        return expression.evaluate(context);
    }

    @Override
    public String toString() {
        return expression.toString();
    }
}
