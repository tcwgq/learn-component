package com.tcwgq.antlr4.orchestration.expression;

import com.tcwgq.antlr4.orchestration.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 函数参数列表表达式
 */
public class FunctionParamsExpression implements Expression {
    private final List<FunctionParamExpression> expressions = new ArrayList<>();

    public FunctionParamsExpression(Expression left, Expression right) {
        add(left);
        add(right);
    }

    private void add(Expression expression) {
        if (FunctionParamsExpression.class.isAssignableFrom(expression.getClass())) {
            expressions.addAll(((FunctionParamsExpression) expression).get());
        } else {
            expressions.add((FunctionParamExpression) expression);
        }
    }

    @Override
    public Object evaluate(Context context) {
        return expressions.stream()
                .map(expression -> expression.evaluate(context))
                .collect(Collectors.toList());
    }

    public List<FunctionParamExpression> get() {
        return expressions;
    }

    @Override
    public String toString() {
        return "(" +
                expressions.stream().map(Expression::toString).collect(Collectors.joining(", ")) +
                ')';
    }
}
