package com.tcwgq.antlr4.orchestration.expression;

import com.tcwgq.antlr4.orchestration.Context;

/**
 * 返回自己
 */
public class IdentityExpression implements Expression {
    @Override
    public Object evaluate(Context context) {
        return context.getData(Context.INPUTS);
    }

    @Override
    public String toString() {
        return "_";
    }
}
