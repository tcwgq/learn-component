package com.tcwgq.antlr4.orchestration.expression;

import com.tcwgq.antlr4.orchestration.Context;
import lombok.Getter;

import java.util.Collections;

@Getter
public class MapValueExpression implements Expression {
    private final Expression keyExpression;
    private final Expression valueExpression;

    public MapValueExpression(Expression keyExpression, Expression valueExpression) {
        this.keyExpression = keyExpression;
        this.valueExpression = valueExpression;
    }

    @Override
    public Object evaluate(Context context) {
        Object key = keyExpression.evaluate(context);
        Object value = valueExpression.evaluate(context);
        return Collections.singletonMap(key, value);
    }

    @Override
    public String toString() {
        return keyExpression + " -> " + valueExpression;
    }
}
