package com.tcwgq.antlr4.orchestration.expression;

import com.tcwgq.antlr4.orchestration.Context;

public class NullExpression implements Expression {
    @Override
    public Object evaluate(Context context) {
        return null;
    }

    @Override
    public String toString() {
        return "null";
    }
}
