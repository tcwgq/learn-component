package com.tcwgq.antlr4.orchestration.expression;

import com.tcwgq.antlr4.orchestration.Context;
import org.apache.commons.lang3.math.NumberUtils;

public class NumberExpression implements Expression {
    private final Double number;

    public NumberExpression(String number) {
        this.number = NumberUtils.createDouble(number);
    }

    @Override
    public Object evaluate(Context context) {
        return number;
    }

    @Override
    public String toString() {
        return "" + number;
    }
}
