package com.tcwgq.antlr4.orchestration.expression;

import com.tcwgq.antlr4.exception.FusionException;
import com.tcwgq.antlr4.exception.FusionReturn;
import com.tcwgq.antlr4.orchestration.Context;
import com.tcwgq.antlr4.field.DataSet;
import lombok.Getter;

/**
 * 属性表达式，如a.b
 */
@Getter
public class PropertyExpression implements Expression {
    private final Expression parent;
    private final String path;

    public PropertyExpression(Expression parent, String path) {
        this.parent = parent;
        this.path = path;
    }

    @Override
    public Object evaluate(Context context) {
        Object parentValue = this.parent.evaluate(context);
        if (parentValue == null) {
            return null;
        }
        if (!DataSet.class.isAssignableFrom(parentValue.getClass())) {
            throw new FusionException(FusionReturn.WRONG_CONFIG, "result of " + parentValue.toString() + " is not DataSet");
        }
        return ((DataSet) parentValue).getFieldValue(path);
    }

    @Override
    public String toString() {
        return parent.toString() + "." + path;
    }
}
