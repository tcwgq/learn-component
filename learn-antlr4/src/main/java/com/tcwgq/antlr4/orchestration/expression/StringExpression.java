package com.tcwgq.antlr4.orchestration.expression;

import com.tcwgq.antlr4.orchestration.Context;

public class StringExpression implements Expression {
    private final String text;

    public StringExpression(String text) {
        this.text = text;
    }

    @Override
    public Object evaluate(Context context) {
        return text;
    }

    @Override
    public String toString() {
        return '\"' + text + '\"';
    }
}
