package com.tcwgq.antlr4.orchestration.expression;

import com.tcwgq.antlr4.orchestration.Context;
import org.apache.commons.lang3.BooleanUtils;

public class TernaryExpression implements Expression {
    private final Expression condition;
    private final Expression positive;
    private final Expression negative;

    public TernaryExpression(Expression condition, Expression positive, Expression negative) {
        this.condition = condition;
        this.positive = positive;
        this.negative = negative;
    }

    @Override
    public Object evaluate(Context context) {
        Object conditionResult = condition.evaluate(context);
        if (conditionResult == null
                || !Boolean.class.isAssignableFrom(conditionResult.getClass())) {
            return null;
        }
        if (BooleanUtils.isTrue((Boolean) conditionResult)) {
            return positive.evaluate(context);
        }
        return negative.evaluate(context);
    }

    @Override
    public String toString() {
        return condition + " ? " + positive + " : " + negative;
    }
}
