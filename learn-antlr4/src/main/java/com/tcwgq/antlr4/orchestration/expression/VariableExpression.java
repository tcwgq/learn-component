package com.tcwgq.antlr4.orchestration.expression;

import com.tcwgq.antlr4.orchestration.Context;
import lombok.Getter;

/**
 * 变量表达式，如${a}
 */
@Getter
public class VariableExpression implements Expression {
    private static final String BY = "by";
    private static final String CONTEXT = "context";

    private final String name;

    public VariableExpression(String name) {
        this.name = name;
    }

    @Override
    public Object evaluate(Context context) {
        if (BY.equals(name)) {
            return context.getBy();
        } else if (CONTEXT.equals(name)) {
            return context;
        } else {
            return context.getData(name);
        }
    }

    @Override
    public String toString() {
        return "${" + name + '}';
    }
}
