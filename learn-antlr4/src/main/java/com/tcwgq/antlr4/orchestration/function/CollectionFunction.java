package com.tcwgq.antlr4.orchestration.function;

import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Slf4j
public abstract class CollectionFunction extends RiskFunction {
    public CollectionFunction(String name) {
        super(name);
    }

    @SuppressWarnings("unchecked")
    protected Collection<Object> toCollection(Object source) {
        if (source == null) {
            return Collections.emptyList();
        }

        if (Collection.class.isAssignableFrom(source.getClass())) {
            return (Collection<Object>) source;
        } else if (source.getClass().isArray()) {
            int length = Array.getLength(source);
            List<Object> list = new ArrayList<>(length);
            for (int i = 0; i < length; i++) {
                list.add(Array.get(source, i));
            }
            return list;
        } else {
            log.warn("source should be collection or array, but is {}", source);
            return Collections.emptyList();
        }
    }
}
