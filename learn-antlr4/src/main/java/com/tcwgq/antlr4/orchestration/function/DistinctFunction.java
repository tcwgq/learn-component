package com.tcwgq.antlr4.orchestration.function;

import com.tcwgq.antlr4.exception.FusionException;
import com.tcwgq.antlr4.exception.FusionReturn;
import com.tcwgq.antlr4.orchestration.Context;
import com.tcwgq.antlr4.orchestration.expression.FunctionParamExpression;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class DistinctFunction extends CollectionFunction {
    public DistinctFunction() {
        super("distinct");
    }

    @Override
    public Object apply(Context context, Object source, List<FunctionParamExpression> params) {
        if (params == null || !params.isEmpty()) {
            throw new FusionException(FusionReturn.WRONG_FUNCTION, "distinct函数没有参数");
        }

        Collection<Object> collection = toCollection(source);
        return collection.stream()
                .filter(Objects::nonNull)
                .distinct()
                .collect(Collectors.toList());
    }
}
