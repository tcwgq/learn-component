package com.tcwgq.antlr4.orchestration.function;

import com.tcwgq.antlr4.exception.FusionException;
import com.tcwgq.antlr4.exception.FusionReturn;
import com.tcwgq.antlr4.orchestration.Context;
import com.tcwgq.antlr4.orchestration.expression.FunctionParamExpression;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
public class FlatFunction extends CollectionFunction {
    public FlatFunction() {
        super("flat");
    }

    @Override
    public List<Object> apply(Context context, Object source, List<FunctionParamExpression> params) {
        if (params == null || !params.isEmpty()) {
            throw new FusionException(FusionReturn.WRONG_FUNCTION, "flat函数没有参数");
        }

        Collection<Object> collection = toCollection(source);
        return collection.stream()
                .filter(Objects::nonNull)
                .map(this::toCollection)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }
}
