package com.tcwgq.antlr4.orchestration.function;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AssignableTypeFilter;

import java.lang.reflect.Constructor;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 函数仓库
 */
@Slf4j
public class FunctionRepository {
    private Map<String, RiskFunction> functionMap;

    @SuppressWarnings("unchecked")
    private FunctionRepository() {
        this.functionMap = new ConcurrentHashMap<>();
        ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(false);
        provider.addIncludeFilter(new AssignableTypeFilter(RiskFunction.class));
        Set<BeanDefinition> beanDefinitions = provider.findCandidateComponents(this.getClass().getPackage().getName());
        this.functionMap = beanDefinitions.stream()
                .filter(beanDefinition -> !beanDefinition.isAbstract())
                .map(BeanDefinition::getBeanClassName)
                .map(className -> {
                    try {
                        return (Class<? extends RiskFunction>) Class.forName(className);
                    } catch (ClassNotFoundException e) {
                        log.warn("can't find class {}", className, e);
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .map(c -> {
                    try {
                        Constructor<? extends RiskFunction> constructor = c.getConstructor();
                        return constructor.newInstance();
                    } catch (Exception e) {
                        log.warn("can't init class {}", c.getName(), e);
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toConcurrentMap(RiskFunction::getName, Function.identity()));
    }

    public static RiskFunction getByName(String functionName) {
        return SingletonHolder.INSTANCE.functionMap.get(functionName);
    }

    private static class SingletonHolder {
        private static final FunctionRepository INSTANCE = new FunctionRepository();
    }
}
