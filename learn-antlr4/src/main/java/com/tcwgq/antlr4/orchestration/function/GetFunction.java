package com.tcwgq.antlr4.orchestration.function;

import com.tcwgq.antlr4.exception.FusionException;
import com.tcwgq.antlr4.exception.FusionReturn;
import com.tcwgq.antlr4.orchestration.Context;
import com.tcwgq.antlr4.orchestration.expression.FunctionParamExpression;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.List;
import java.util.Map;

public class GetFunction extends RiskFunction {
    public GetFunction() {
        super("get");
    }

    @Override
    public Object apply(Context context, Object source, List<FunctionParamExpression> params) {
        if (params == null || params.size() != 1) {
            throw new FusionException(FusionReturn.WRONG_FUNCTION, "get函数只能有一个参数");
        }

        if (source == null) {
            return null;
        }

        FunctionParamExpression paramExpression = params.get(0);
        Object indexObject = paramExpression.evaluate(context);
        if (List.class.isAssignableFrom(source.getClass())) {
            if (indexObject == null) {
                return null;
            }

            int index;
            if (Number.class.isAssignableFrom(indexObject.getClass())) {
                index = ((Number) indexObject).intValue();
            } else if (NumberUtils.isCreatable(indexObject.toString())) {
                index = NumberUtils.toInt(indexObject.toString());
            } else {
                return null;
            }
            if (index < 0 || index >= ((List) source).size()) {
                return null;
            }
            return ((List) source).get(index);
        } else if (Map.class.isAssignableFrom(source.getClass())) {
            if (Number.class.isAssignableFrom(indexObject.getClass())) {
                indexObject = ((Number) indexObject).doubleValue();
            }
            return ((Map) source).get(indexObject);
        }
        return null;
    }
}
