package com.tcwgq.antlr4.orchestration.function;

import com.tcwgq.antlr4.exception.FusionException;
import com.tcwgq.antlr4.exception.FusionReturn;
import com.tcwgq.antlr4.orchestration.Context;
import com.tcwgq.antlr4.orchestration.expression.FunctionParamExpression;
import com.tcwgq.antlr4.field.DataSet;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class MapFunction extends CollectionFunction {
    public MapFunction() {
        super("map");
    }

    @Override
    public List apply(Context context, Object source, List<FunctionParamExpression> params) {
        if (params == null || params.size() != 1) {
            throw new FusionException(FusionReturn.WRONG_FUNCTION, "map函数只能有一个参数");
        }

        Collection<Object> sourceCollection = toCollection(source);
        FunctionParamExpression param = params.get(0);
        return sourceCollection.stream()
                .map(o -> {
                    if (o == null) {
                        return null;
                    }
                    DataSet dataSet;
                    if (DataSet.class.isAssignableFrom(o.getClass())) {
                        dataSet = (DataSet) o;
                    } else {
                        dataSet = DataSet.singleton("_", o);
                    }
                    return param.evaluate(Context.singleton(dataSet));
                })
                .collect(Collectors.toList());
    }
}
