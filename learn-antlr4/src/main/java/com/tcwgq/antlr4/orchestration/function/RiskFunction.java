package com.tcwgq.antlr4.orchestration.function;

import com.tcwgq.antlr4.orchestration.Context;
import com.tcwgq.antlr4.orchestration.expression.FunctionParamExpression;
import lombok.Getter;

import java.util.List;

/**
 * 风控表达式函数
 */
public abstract class RiskFunction {
    @Getter
    private final String name;

    public RiskFunction(String name) {
        this.name = name;
    }

    /**
     * 计算函数
     *
     * @param context 计算上下文
     * @param source  函数调用者对象
     * @param params  函数参数表达式
     * @return 函数计算结果
     */
    public abstract Object apply(Context context, Object source, List<FunctionParamExpression> params);
}
