package com.tcwgq.antlr4.orchestration.function;

import com.tcwgq.antlr4.orchestration.Context;
import com.tcwgq.antlr4.orchestration.expression.FunctionParamExpression;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ToMapFunction extends RiskFunction {
    public ToMapFunction() {
        super("toMap");
    }

    @Override
    public Map<Object, Object> apply(Context context, Object source, List<FunctionParamExpression> params) {
        if (params == null) {
            return Collections.emptyMap();
        }
        Map<Object, Object> result = new HashMap<>(params.size());
        for (FunctionParamExpression paramExpression : params) {
            Object param = paramExpression.evaluate(context);
            if (param != null && Map.class.isAssignableFrom(param.getClass())) {
                result.putAll((Map<?, ?>) param);
            }
        }
        return Collections.unmodifiableMap(result);
    }
}
