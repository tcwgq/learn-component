grammar Expression;

@header{package com.tcwgq.antlr4.expression;}

propertyExpr:  propertyExpr '->' propertyExpr  # mapValueExpression
| '(' propertyExpr '==' propertyExpr ')'       # equalsExpression
| propertyExpr '.' ID                          # subProperty
|  propertyExpr '._' NUMBER                    # collectionProperty
|  propertyExpr '.' functionExpr               # functionTransform
|  variableExpr                                # variableProperty
| NUMBER                                       # numberExpression
| 'null'                                       # nullExpression
| STRING                                       # stringExpression
| propertyExpr '?' propertyExpr ':' propertyExpr # ternaryExpression
;

variableExpr : '${' ID '}'                     # variable
;

functionExpr : ID '(' paramExprs ')'           # functionWithParams
| ID '()'                                      # functionWithoutParam
;

paramExprs : paramExprs ', ' paramExprs        # multiParams
| propertyExpr                                 # propertyParam
| functionParamExpr                            # functionParam
;

functionParamExpr : functionParamExpr '.' ID   # functionMultiPath
| functionParamExpr '.' functionExpr           # innerFunction
| '_.' ID                                      # functionPath
;

NUMBER: [0-9]+ ;
ID : [a-zA-Z0-9\-]+ ;
STRING: '"' (.)*? '"';
WS : [ \t]+ -> skip ;
