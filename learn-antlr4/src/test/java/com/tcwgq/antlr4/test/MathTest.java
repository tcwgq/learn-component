package com.tcwgq.antlr4.test;

import com.tcwgq.antlr4.math.MathBaseVisitor;
import com.tcwgq.antlr4.math.MathLexer;
import com.tcwgq.antlr4.math.MathParser;
import com.tcwgq.antlr4.math.MathVisitor;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;

/**
 * @author tcwgq
 * @time 2020/9/26 23:29
 */
public class MathTest {
    @Test
    public void test1() {
        CharStream input = CharStreams.fromString("12*2+12\r\n");
        MathLexer lexer = new MathLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        MathParser parser = new MathParser(tokens);
        ParseTree tree = parser.prog(); // parse
        MathBaseVisitor<String> vt = new MathBaseVisitor<>();
        vt.visit(tree);
    }
}
