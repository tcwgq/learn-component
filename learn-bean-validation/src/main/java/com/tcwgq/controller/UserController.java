package com.tcwgq.controller;

import com.tcwgq.entity.User;
import com.tcwgq.response.ApiResponse;
import com.tcwgq.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.constraints.Min;

/**
 * @author tcwgq
 * @since 2018-10-14 10:40:52
 */
@Validated
@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @ResponseBody
    @RequestMapping(value = "/get", method = {RequestMethod.GET, RequestMethod.POST})
    public ApiResponse<User> get(@Min(value = 0, message = "id参数不合法") Long id) {
        return ApiResponse.success(userService.getUser(id));
    }

    @ResponseBody
    @RequestMapping(value = "/update", method = {RequestMethod.GET, RequestMethod.POST})
    public ApiResponse<Boolean> update(@Validated User user, BindingResult result) {
        if (result.hasErrors()) {
            System.out.println(result);
        }
        return ApiResponse.success(true);
    }
}
