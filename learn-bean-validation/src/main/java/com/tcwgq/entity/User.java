package com.tcwgq.entity;

import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {

    private Long id;

   @Pattern(regexp = "^[a-z]{3}$", message = "姓名不合法")
    private String name;


    private Integer age;


    private String email;


    private int orderStatus;


    private Integer version;


    private Byte deleted;


    private Date timeCreated;


    private Date timeModified;

    private static final long serialVersionUID = 1L;


    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }


    public Integer getAge() {
        return age;
    }


    public void setAge(Integer age) {
        this.age = age;
    }


    public String getEmail() {
        return email;
    }


    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }


    public int getOrderStatus() {
        return orderStatus;
    }


    public void setOrderStatus(int orderStatus) {
        this.orderStatus = orderStatus;
    }


    public Integer getVersion() {
        return version;
    }


    public void setVersion(Integer version) {
        this.version = version;
    }


    public Byte getDeleted() {
        return deleted;
    }


    public void setDeleted(Byte deleted) {
        this.deleted = deleted;
    }


    public Date getTimeCreated() {
        return timeCreated;
    }


    public void setTimeCreated(Date timeCreated) {
        this.timeCreated = timeCreated;
    }


    public Date getTimeModified() {
        return timeModified;
    }

    public void setTimeModified(Date timeModified) {
        this.timeModified = timeModified;
    }
}