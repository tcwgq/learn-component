package com.tcwgq.service;

import com.tcwgq.entity.User;

/**
 * @author tcwgq
 * @since 2018-10-14 10:40:52
 */
public interface UserService {
	public User getUser(Long id);
}
