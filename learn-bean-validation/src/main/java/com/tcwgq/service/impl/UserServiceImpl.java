package com.tcwgq.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tcwgq.entity.User;
import com.tcwgq.mapper.UserMapper;
import com.tcwgq.service.UserService;

/**
 * @author tcwgq
 * @since 2018-10-14 10:40:52
 */
@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserMapper userMapper;

	@Override
	public User getUser(Long id) {
		return userMapper.selectByPrimaryKey(id);
	}
}
