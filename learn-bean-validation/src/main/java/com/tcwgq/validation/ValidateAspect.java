package com.tcwgq.validation;

import com.tcwgq.validation.annotation.ValidateField;
import com.tcwgq.validation.annotation.ValidateGroup;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by wangguangqiang on 2019/11/11 17:46
 */
@Slf4j
@Aspect
@Component
public class ValidateAspect {
    private static final ConcurrentHashMap<String, List<Object>> CACHE = new ConcurrentHashMap<>();

    @Around("@annotation(com.tcwgq.validation.annotation.ValidateGroup)")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        log.info("ValidateAspect...");

        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        String[] parameterNames = methodSignature.getParameterNames();
        Class[] parameterTypes = methodSignature.getParameterTypes();
        // 获取方法参数
        Object[] args = joinPoint.getArgs();
        int length = args.length;

        Method method = methodSignature.getMethod();
        ValidateGroup validateGroup = method.getAnnotation(ValidateGroup.class);
        ValidateField[] validateFields = validateGroup.value();
        for (int i = 0; i < length; i++) {
            Object parameterValue = args[i];
            for (ValidateField validateField : validateFields) {
                if (parameterNames[i].equals(validateField.key())) {
                    // 对象类型
                    String fieldName = validateField.fieldName();
                    if (StringUtils.isNoneBlank(fieldName)) {
                        Class<?> fieldType = getFieldType(parameterTypes[i], fieldName);
                        if (fieldType != null) {
                            checkPrimitiveValue(invokeReadMethod(parameterTypes[i], fieldType, fieldName, parameterValue), validateField);
                        }
                    } else {
                        //基本类型
                        checkPrimitiveValue(parameterValue, validateField);
                    }
                }
            }
        }

        return joinPoint.proceed();
    }

    public void checkPrimitiveValue(Object value, ValidateField field) {
        if (field.notNull()) {
            if (value == null) {
                throw new RuntimeException(field.notNullNotice());
            }
        }

        if (field.noneBlank()) {
            if (value instanceof String) {
                String str = (String) value;
                if (StringUtils.isBlank(str)) {
                    throw new RuntimeException(field.noneBlankNotice());
                }
            } else if (value == null) {
                throw new RuntimeException(field.noneBlankNotice());
            }
        }

        if (field.mustBlank()) {
            if (value instanceof String) {
                String str = (String) value;
                if (StringUtils.isNoneBlank(str)) {
                    throw new RuntimeException(field.noneBlankNotice());
                }
            } else if (value != null) {
                throw new RuntimeException(field.noneBlankNotice());
            }
        }

        if (field.minValue() != 0) {
            if (value instanceof Long) {
                long l = (long) value;
                if (l < field.minValue()) {
                    throw new RuntimeException(field.minValueNotice());
                }
            }
        }

    }

    private Class<?> getFieldType(Class clazz, String property) {
        try {
            Field declaredField = clazz.getDeclaredField(property);
            return declaredField.getType();
        } catch (NoSuchFieldException e) {
            log.error("class {} has no property {}", clazz.getName(), property, e);

            return null;
        }
    }

    private Object invokeReadMethod(Class<?> clazz, Class<?> fieldType, String property, Object arg) {
        String readMethod = getReadMethod(property);
        MethodType methodType = MethodType.methodType(fieldType);
        try {
            MethodHandle methodhandle = MethodHandles.lookup().findVirtual(clazz, readMethod, methodType);
            return methodhandle.invoke(arg);
        } catch (Throwable e) {
            log.error("invoke class {} method {} error", clazz.getName(), readMethod, e);

            return null;
        }
    }

    private String getReadMethod(String name) {
        char[] chars = name.toCharArray();
        chars[0] -= 32;
        return "get" + String.valueOf(chars);
    }
}
