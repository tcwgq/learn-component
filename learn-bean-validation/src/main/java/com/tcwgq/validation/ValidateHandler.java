package com.tcwgq.validation;

import com.tcwgq.validation.annotation.ValidateField;
import com.tcwgq.validation.annotation.ValidateGroup;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * Created by wangguangqiang on 2019/11/11 17:46
 */
@Slf4j
@Aspect
//@Component
public class ValidateHandler {
    @Pointcut("@annotation(com.tcwgq.validation.annotation.ValidateGroup)")
    public void pointcut() {
    }

    @Around("pointcut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        log.info("ValidateAspect...");
        System.out.println(joinPoint.toLongString());// execution(public com.tcwgq.bean.validation.model.User com.tcwgq.bean.validation.service.impl.UserServiceImpl.get(java.lang.Long))
        System.out.println(joinPoint.toShortString());// execution(UserServiceImpl.get(..))
        System.out.println(joinPoint.toString());// execution(User com.tcwgq.bean.validation.service.impl.UserServiceImpl.get(Long))
        System.out.println(joinPoint.getSignature().toLongString());//public com.tcwgq.bean.validation.model.User com.tcwgq.bean.validation.service.impl.UserServiceImpl.get(java.lang.Long)
        System.out.println(joinPoint.getSignature().toShortString());//UserServiceImpl.get(..)
        System.out.println(joinPoint.getSignature().toString());//User com.tcwgq.bean.validation.service.impl.UserServiceImpl.get(Long)
        System.out.println(Arrays.asList(joinPoint.getArgs()));
        // 获取方法参数

        Signature signature = joinPoint.getSignature();
        Object[] args = joinPoint.getArgs();
        int length = args.length;
        MethodSignature methodSignature = (MethodSignature) signature;
        String[] parameterNames = methodSignature.getParameterNames();
        Class[] parameterTypes = methodSignature.getParameterTypes();

        System.out.println(Arrays.asList(parameterNames));
        System.out.println(Arrays.asList(parameterTypes));

        Method method = methodSignature.getMethod();
        ValidateGroup validateGroup = method.getAnnotation(ValidateGroup.class);
        ValidateField[] validateFields = validateGroup.value();
        for (int i = 0; i < length; i++) {
            String parameterName = parameterNames[i];
            Object parameterValue = args[i];
            for (int j = 0; j < validateFields.length; j++) {
                ValidateField validateField = validateFields[j];
                if (parameterName.equals(validateField.key())) {
                    checkValue(parameterValue, validateField);
                }
            }
        }

        return joinPoint.proceed();
    }

    public void checkValue(Object value, ValidateField field) {
        if (field.notNull()) {
            if (value == null) {
                throw new RuntimeException(field.notNullNotice());
            }
        }

        if (field.noneBlank()) {
            if (value instanceof String) {
                String str = (String) value;
                if (StringUtils.isBlank(str)) {
                    throw new RuntimeException(field.noneBlankNotice());
                }
            } else if (value == null) {
                throw new RuntimeException(field.noneBlankNotice());
            }
        }

        if (field.mustBlank()) {
            if (value instanceof String) {
                String str = (String) value;
                if (StringUtils.isNoneBlank(str)) {
                    throw new RuntimeException(field.noneBlankNotice());
                }
            } else if (value != null) {
                throw new RuntimeException(field.noneBlankNotice());
            }
        }

        if (field.minValue() != 0) {
            if (value instanceof Long) {
                long l = (long) value;
                if (l < field.minValue()) {
                    throw new RuntimeException(field.minValueNotice());
                }
            }
        }

    }

    private Object invokeReadMethod(Class<?> clazz, String property, Object arg) {
        String readMethod = getReadMethod(property);
        MethodType methodType = MethodType.methodType(Object.class);
        try {
            MethodHandle methodhandle = MethodHandles.lookup().findVirtual(clazz, readMethod, methodType);
            return methodhandle.invoke(arg);
        } catch (Throwable e) {
            e.printStackTrace();

            return null;
        }
    }

    private String getReadMethod(String name) {
        char[] chars = name.toCharArray();
        chars[0] -= 32;
        return "get" + String.valueOf(chars);
    }
}
