package com.tcwgq.validation.annotation;

import java.lang.annotation.*;

/**
 * Created by wangguangqiang on 2019/11/11 17:28
 */
@Target({ElementType.METHOD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ValidateField {
    String key();

    String fieldName() default "";

    boolean notNull() default false;

    String notNullNotice() default "不能为null";

    boolean noneBlank() default false;

    String noneBlankNotice() default "不能为空字符相关";

    boolean mustBlank() default false;

    String mustBlankNotice() default "必须为空字符串";

    long minValue() default 0;

    String minValueNotice() default "不能小于minValue设定的最小值";
}
