package com.tcwgq.validation.annotation;

import java.lang.annotation.*;

/**
 * Created by wangguangqiang on 2019/11/11 17:26
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ValidateGroup {
    ValidateField[] value();
}
