package com.tcwgq.cache.learncaffeine.caffeine;

import com.github.benmanes.caffeine.cache.CacheLoader;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

/**
 * @author tcwgq
 * @time 2021/4/10 15:42
 */
public class CaffeineTest {
    @Test
    public void test1() {
        LoadingCache<String, String> cache = Caffeine.newBuilder().
                initialCapacity(32).
                maximumSize(64).expireAfterAccess(30, TimeUnit.MINUTES).build(new CacheLoader<String, String>() {
                    @Override
                    public @Nullable String load(@NonNull String key) throws Exception {
                        return null;
                    }
                });
        cache.put("name", "zhangSan");
        System.out.println(cache.get("name"));
        System.out.println(cache.get("age"));
    }
}
