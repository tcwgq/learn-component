package com.tcwgq.learn_cglib;

import com.tcwgq.learn_cglib.interceptor.CglibProxyInterceptor;
import com.tcwgq.learn_cglib.service.PersonService;
import net.sf.cglib.core.DebuggingClassWriter;
import net.sf.cglib.proxy.Enhancer;

public class Test {
    public static void main(String[] args) {
        //代理类class文件存入本地磁盘
        System.setProperty(DebuggingClassWriter.DEBUG_LOCATION_PROPERTY, System.getProperty("user.dir") + "/learn-cglib/target/classes/cglib");
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(PersonService.class);
        enhancer.setCallback(new CglibProxyInterceptor());
        PersonService proxy = (PersonService) enhancer.create();
        // final方法未被代理
        proxy.getPerson("1");
        proxy.setPerson();
    }
}