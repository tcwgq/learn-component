package com.tcwgq.learn_cglib;

import com.tcwgq.learn_cglib.service.Sample;
import net.sf.cglib.core.DebuggingClassWriter;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author tcwgq
 * @time 2020/5/31 22:11
 */
public class Test1 {
    public static void main(String[] args) {
        System.setProperty(DebuggingClassWriter.DEBUG_LOCATION_PROPERTY, "D:\\code");
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(Sample.class);
        enhancer.setCallback(new MethodInterceptor() {
            @Override
            public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
                System.out.println("begin...");
                Object result = proxy.invokeSuper(obj, args);
                System.out.println("end...");
                return result;
            }
        });
        Sample sample = (Sample) enhancer.create();
        sample.sayHello();
    }
}
