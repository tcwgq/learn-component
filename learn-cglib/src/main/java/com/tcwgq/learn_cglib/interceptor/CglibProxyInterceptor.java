package com.tcwgq.learn_cglib.interceptor;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class CglibProxyInterceptor implements MethodInterceptor {
    /**
     * @param sub         代理对象
     * @param method      目标方法
     * @param objects     方法参数
     * @param methodProxy 代理方法
     * @return
     * @throws Throwable
     */
    @Override
    public Object intercept(Object sub, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println("执行前...");
        Object object = methodProxy.invokeSuper(sub, objects);
        // 被代理方法(methodProxy.invoke会调用，这就是为什么在拦截器中调用methodProxy.invoke会死循环，一直在调用拦截器)
        // 递归调用，导致StackOverflowError
        //Object object = methodProxy.invoke(sub, objects);
        System.out.println("执行后...");
        return object;
    }
}