package com.tcwgq.learn_cglib.service;

import com.tcwgq.learn_cglib.bean.Person;

/**
 * 类不能被final修饰
 * <p>
 * 需要增强的方法不能被final修饰，且不能是static方法
 */
public class PersonService {
    public PersonService() {
        System.out.println("PersonService构造");
    }

    // 该方法不能被子类覆盖
    final public Person getPerson(String name) {
        System.out.println("PersonService:final getPerson>>" + name);
        return new Person("张三", 23);
    }

    public void setPerson() {
        System.out.println("PersonService:setPerson");
    }

}