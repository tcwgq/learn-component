package com.tcwgq.dto;

import lombok.Data;

import java.util.Date;

/**
 * Created by wangguangqiang on 2019/8/7 17:28
 */
@Data
public class UserDTO {
    private String name;
    private Integer age;
    private String creator;
    private Date created;
    private String modifier;
    private Date modified;
}
