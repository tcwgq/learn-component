package com.tcwgq.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by wangguangqiang on 2019/8/7 17:20
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private String name;
    private Integer age;
    private String creator;
    private Date created;
    private String modifier;
    private Date modified;
}
