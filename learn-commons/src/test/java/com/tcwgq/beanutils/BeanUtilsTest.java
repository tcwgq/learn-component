package com.tcwgq.beanutils;

import com.tcwgq.dto.UserDTO;
import com.tcwgq.model.User;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.MethodUtils;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by wangguangqiang on 2019/8/7 17:30
 */
public class BeanUtilsTest {
    @Test
    public void test1() throws InvocationTargetException, IllegalAccessException {
        User user = new User();
        user.setName("zhangSan");
        user.setAge(23);
        user.setCreator("system");
        user.setModifier("system");
        Date date = new Date();
        user.setCreated(date);
        user.setModified(date);
        System.out.println(user);
        UserDTO userDTO = new UserDTO();
        // 注意参数顺序
        BeanUtils.copyProperties(userDTO, user);
        System.out.println(userDTO);
    }

    @Test
    public void test2() throws InvocationTargetException, IllegalAccessException {
        Map<String, String> map = new HashMap<>();
        map.put("name", "zhangSan");
        map.put("age", "23");
        User user = new User();
        BeanUtils.populate(user, map);
        System.out.println(user);
    }

    @Test
    public void test() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        User user = new User();
        Method method = MethodUtils.getAccessibleMethod(User.class, "setName", String.class);
        Object zhangSan = method.invoke(user, "zhangSan");
        MethodUtils.invokeMethod(user, "setAge", 23);
        System.out.println(user);
    }
}
