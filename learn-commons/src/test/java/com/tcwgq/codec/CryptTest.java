package com.tcwgq.codec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Test;

import java.util.UUID;

public class CryptTest {
    private String key = "2c5a3f0a2af3485ba95783ab1007d50c";

    @Test
    public void test1() {
        String s = DigestUtils.md5Hex("513235600李颖37078319900722110Xiyunbao");
        System.out.println(s);
    }

    @Test
    public void test2() {
        String replace = UUID.randomUUID().toString().replace("-", "");
        System.out.println(replace);
    }

    @Test
    public void test3() {
        String s = Base64.encodeBase64String("Hello, world".getBytes());
        System.out.println(s);
    }

    @Test
    public void test4() {
        String s = DigestUtils.md5Hex("123456Iyb");
        System.out.println(s);
    }

}