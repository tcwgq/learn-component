package com.tcwgq.io;

import org.apache.commons.io.filefilter.EmptyFileFilter;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class FileFilterTest {

    private String basePath = null;

    @Before
    public void setUp() throws Exception {
        basePath = System.getProperty("user.dir") + "\\file\\";
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * 空内容文件过滤器
     *
     * @throws IOException
     */
    @Test
    public void testEmptyFileFilter() throws IOException {
        File dir = new File(basePath);
        String[] files = dir.list(EmptyFileFilter.NOT_EMPTY);
        for (String file : files) {
            System.out.println(file);
        }
    }

    /**
     * 文件名称后缀过滤器
     *
     * @throws IOException
     */
    @Test
    public void testSuffixFileFilter() throws IOException {
        File dir = new File(basePath);
        String[] files = dir.list(new SuffixFileFilter("a.txt"));
        for (String file : files) {
            System.out.println(file);
        }
    }

}