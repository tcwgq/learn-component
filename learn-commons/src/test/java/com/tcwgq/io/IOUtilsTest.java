package com.tcwgq.io;

import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by wangguangqiang on 2019/8/7 17:54
 */
public class IOUtilsTest {
    @Test
    public void test1() throws IOException {
        InputStream in = new URL( "http://commons.apache.org"   ).openStream();
        try {
            InputStreamReader inR = new InputStreamReader( in );
            BufferedReader buf = new BufferedReader( inR );
            String line;
            while ( ( line = buf.readLine() ) != null ) {
                System.out.println( line );
            }
        } finally {
            in.close();
        }
    }

    @Test
    public void test2() throws IOException {
        InputStream in = new URL( "http://commons.apache.org" ).openStream();
        try {
            System.out.println(IOUtils.toString(in));
        } finally {
            IOUtils.closeQuietly(in);
        }
    }

    @Test
    public void test3() throws IOException {
        File file = new File("d:/a.txt");
        IOUtils.write("Hello world, java!", new FileOutputStream(file), Charsets.UTF_8);
    }

}
