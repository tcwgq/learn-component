package com.tcwgq.string;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

/**
 * Created by wangguangqiang on 2019/10/10 17:59
 */
public class StringUtilsTest {
    @Test
    public void test1() {
        boolean anyBlank = StringUtils.isAnyBlank("zhangSan", "lisi", "");
        System.out.println(anyBlank);
    }

    @Test
    public void test2() {
        boolean anyBlank = StringUtils.isAnyBlank("zhangSan", "lisi", null);
        System.out.println(anyBlank);
    }

    @Test
    public void test3() {
        boolean anyBlank = StringUtils.isAnyBlank("zhangSan", "lisi", "wangwu");
        System.out.println(anyBlank);
    }

}
