/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tcwgq.watch;

import org.apache.commons.lang.time.StopWatch;

/**
 * <p>
 * Frequence Utils
 * <p>
 *
 * <p>#ThreadSafe#</p>
 *
 * @author Opencfg Software Foundation
 * @version $Id: FrequenceUtils.java 2011-06-11 23:58:53 reymondtu $
 * @since 0.0.1-SNAPSHOT
 */
public final class FrequenceUtils {
    private static ThreadLocal<FrequenceUnit> threadLocal = new ThreadLocal<FrequenceUnit>() {
        protected synchronized FrequenceUnit initialValue() {
            FrequenceUnit funit = new FrequenceUnit();
            funit.watch.start();
            return funit;
        }
    };

    /**
     * FrequenceUnit
     */
    private static class FrequenceUnit {
        long limitSplitTime;
        int limitCount;
        StopWatch watch;
        int realCount = 0;

        FrequenceUnit() {
            this.watch = new StopWatch();
        }
    }

    /**
     * <p>
     * Limit call count in split time
     * </p>
     *
     * @param limitSplitTime
     * @param limitCount
     * @throws InterruptedException
     */
    public static void limit(final long limitSplitTime, final int limitCount) throws InterruptedException {
        FrequenceUnit funit = threadLocal.get();
        funit.limitSplitTime = limitSplitTime;
        funit.limitCount = limitCount;
        funit.watch.split();
        long diffTime = funit.limitSplitTime - funit.watch.getSplitTime();
        if (diffTime >= 0) {
            if (funit.realCount >= funit.limitCount) {
            	// 暂停
                funit.watch.suspend();
                Thread.sleep(diffTime);
                // （中断后）重新开始，继续；重新回到，恢复（席位，地位或职位）
                funit.watch.resume();
                funit.realCount = 0;
            }
        }
        funit.realCount++;
    }

}