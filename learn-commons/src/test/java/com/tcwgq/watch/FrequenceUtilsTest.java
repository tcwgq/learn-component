/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tcwgq.watch;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * <p>
 * Frequence Utils Test
 * <p>
 *
 * <p>JUnit4 Test</p>
 *
 * @author Opencfg Software Foundation
 * @version $Id: FrequenceUtilsTest.java 2011-06-12 01:36:53 reymondtu $
 * @since 0.0.1-SNAPSHOT
 */
public class FrequenceUtilsTest {

    @Test
    public void testLimit() throws InterruptedException {
        FrequenceTestClass ftc = new FrequenceTestClass(1000, 10);
        for (int i = 0; i < 100; i++) {
            ftc.method(i);
        }
        assertTrue(true);
    }

    @Test
    public void testLimitMutiThreads() throws InterruptedException {
        Thread t1 = new Thread(new FrequenceTestClass(1000, 10));
        t1.start();

        Thread t2 = new Thread(new FrequenceTestClass(1000, 20));
        t2.start();

        Thread.sleep(10000);
    }

    class FrequenceTestClass implements Runnable {
        final long limitTime;
        final int limitCount;

        FrequenceTestClass(final long limitTime, final int limitCount) {
            this.limitTime = limitTime;
            this.limitCount = limitCount;
        }

        public void method(int i) throws InterruptedException {
            FrequenceUtils.limit(limitTime, limitCount);
            System.out.println("tid:" + Thread.currentThread().getId() + ", i=" + i);
        }

        @Override
        public void run() {
            for (int i = 0; i < 100; i++) {
                try {
                    method(i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}