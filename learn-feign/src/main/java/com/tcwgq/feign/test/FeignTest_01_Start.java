package com.tcwgq.feign.test;

import java.util.List;

import com.tcwgq.interceptor.ForwardedForInterceptor;
import com.tcwgq.model.Contributor;
import com.tcwgq.service.GitHub;

import feign.Feign;
import feign.Logger;
import feign.Retryer;
import feign.auth.BasicAuthRequestInterceptor;
import feign.gson.GsonDecoder;
import feign.okhttp.OkHttpClient;

/**
 * @author tcwgq
 * @time 2017年12月5日下午2:59:55
 * @email qiang.wang@guanaitong.com
 */
public class FeignTest_01_Start {
    public static void main(String... args) {
        GitHub github = Feign.builder().//
                decoder(new GsonDecoder()).// 使用GsxonDecoder
                // 通过配置Logger，我们可以记录request的相关信息。在Feign中，我们可以设置Logger和LogLevel两个值
                logger(new Logger.JavaLogger().appendToFile("logs/http.log")).// Feign内置三个Logger，分别是JavaLogger、ErrorLogger和NoOpLogger，默认为NoOpLogger.JavaLogger内部使用jul打印request日志；ErrorLogger则简单地通过“System.err.printf”打印信息；NoOpLogger不做任何操作。
                logLevel(Logger.Level.FULL).// Feign为我们提供四种Log
                                            // Level：NONE、BASIC、HEADERS和FULL，NONE为不打印，FULL为打印headers、body和metadata，BASIC打印request
                                            // method和url或response状态码和headers，HEADERS打印request或response的基本信息。
                // Feign还实现了Slf4jLogger，我们需要引入feign-slf4j和slf4j等相关依赖包，即可使用slf4j打印request和response日志。
                requestInterceptor(new ForwardedForInterceptor()).// 使用自定义拦截器
                requestInterceptor(new BasicAuthRequestInterceptor("zhangSan", "112113")).// 使用内置的BasicAuthRequestInterceptor
                client(new OkHttpClient()).// Client组件应该是最重要的组件之一了，因为Feign最终发送request请求以及接收response响应，都是由Client组件完成的，它们都需要实现Client接口。
                retryer(new Retryer.Default(0, 0, -1)).// -1表示不重试。Feign通过Retryer组件向我们提供重试机制。Feign提供默认的Retryer实现，其构造函数使得我们可以传入period、maxPeriod和maxAttempts三个参数进行配置，默认的period为100ms，maxPeriod为1000ms，maxAttempts为5次。
                target(GitHub.class, "https://api.github.com/");
        // 获取贡献者列表，并打印其登录名以及贡献次数
        List<Contributor> contributors = github.contributors("netflix", "feign");
        for (Contributor contributor : contributors) {
            System.out.println(contributor.getLogin() + " (" + contributor.getContributions() + ")");
        }
    }
}