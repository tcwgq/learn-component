package com.tcwgq.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;

/**
 * Feign组件，自定义请求拦截器
 * 
 * @author tcwgq
 * @time 2017年12月6日上午10:04:47
 * @email qiang.wang@guanaitong.com
 */
public class ForwardedForInterceptor implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate template) {
        template.header("X-Forwarded-For", "origin.host.com");
    }

}
