package com.tcwgq.model;

/**
 * @author tcwgq
 * @time 2017年12月6日下午2:07:43
 * @email qiang.wang@guanaitong.com
 */
public class Bar {
    private String key;
    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Bar [key=" + key + ", value=" + value + "]";
    }

}
