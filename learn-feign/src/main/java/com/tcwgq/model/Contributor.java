package com.tcwgq.model;

/**
 * @author tcwgq
 * @time 2017年12月5日下午3:10:22
 * @email qiang.wang@guanaitong.com
 */
public class Contributor {
    private String login;
    private int contributions;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getContributions() {
        return contributions;
    }

    public void setContributions(int contributions) {
        this.contributions = contributions;
    }

    @Override
    public String toString() {
        return "Contributor [login=" + login + ", contributions=" + contributions + "]";
    }

}
