package com.tcwgq.service;

import com.tcwgq.model.Bar;

/**
 * @author tcwgq
 * @time 2017年12月6日下午2:08:29
 * @email qiang.wang@guanaitong.com
 */
public interface BarApi extends BaseApi<Bar> {

}
