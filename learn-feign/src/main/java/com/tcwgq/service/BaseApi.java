package com.tcwgq.service;

import java.util.List;

import feign.Headers;
import feign.Param;
import feign.RequestLine;

/**
 * @author tcwgq
 * @time 2017年12月6日下午2:04:30
 * @email qiang.wang@guanaitong.com
 */
@Headers("Accept: application/json")
public interface BaseApi<V> {
    @RequestLine("GET /api/{key}")
    V get(@Param("key") String key);

    @RequestLine("GET /api")
    List<V> list();

    @Headers("Content-Type: application/json")
    @RequestLine("PUT /api/{key}")
    void put(@Param("key") String key, V value);
}
