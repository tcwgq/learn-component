package com.tcwgq.service;

import com.tcwgq.model.Foo;

/**
 * @author tcwgq
 * @time 2017年12月6日下午2:06:59
 * @email qiang.wang@guanaitong.com
 */
public interface FooApi extends BaseApi<Foo> {

}
