package com.tcwgq.service;

import java.util.List;

import com.tcwgq.model.Contributor;

import feign.Param;
import feign.RequestLine;

/**
 * @author tcwgq
 * @time 2017年12月5日下午6:25:48
 * @email qiang.wang@guanaitong.com
 */
public interface GitHub {
    @RequestLine("GET /repos/{owner}/{repo}/contributors")
    List<Contributor> contributors(@Param("owner") String owner, @Param("repo") String repo);
}
