package com.tcwgq.learn_guava.base;

import com.google.common.base.CharMatcher;
import org.junit.Test;

/**
 * Created by tcwgq on 2019/8/3 18:09.
 */
public class CharMatcherTest {
    @Test
    public void test1() {
        System.out.println(CharMatcher.DIGIT.retainFrom("12323dsafdasfd"));// 保留数字
        System.out.println(CharMatcher.DIGIT.removeFrom("12323dsafdasfd"));// 移除数字
        System.out.println(CharMatcher.JAVA_DIGIT.removeFrom("12323dsafdasfd"));// 移除数字
        System.out.println(CharMatcher.JAVA_DIGIT.or(CharMatcher.WHITESPACE).retainFrom("hello 1234 jajaj"));// 保留空格及数字
        System.out.println(CharMatcher.DIGIT.collapseFrom("Hello1234", 'a'));// 连续出现的数字，替换为a
    }

}