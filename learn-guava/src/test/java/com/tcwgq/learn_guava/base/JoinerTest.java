package com.tcwgq.learn_guava.base;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.junit.Test;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

/**
 * @author iyb-wangguangqiang 2019/8/5 10:45
 */
public class JoinerTest {
    private static final Joiner joiner = Joiner.on(",").skipNulls();

    @Test
    public void test1() {
        String s = joiner.join(Lists.newArrayList(1, null, 3));
        System.out.println(s);
    }

    @Test
    public void test2() {
        StringBuilder sb = new StringBuilder();
        joiner.appendTo(sb, "a", "b");
        System.out.println(sb.toString());
    }

    @Test
    public void test3() throws IOException {
        FileWriter fw = new FileWriter("d:/a.txt");
        joiner.appendTo(fw, "a", "b");
        fw.close();
    }

    @Test
    public void test4() {
        Map<String, Integer> map = Maps.newTreeMap();
        map.put("zhangSan", 23);
        map.put("liSi", 24);
        String join = Joiner.on(",").withKeyValueSeparator("=").join(map);
        System.out.println(join);
    }

}
