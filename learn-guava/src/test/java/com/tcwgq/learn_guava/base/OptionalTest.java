package com.tcwgq.learn_guava.base;

import com.google.common.base.Optional;
import com.tcwgq.learn_guava.model.User;
import org.junit.Test;

/**
 * @author iyb-wangguangqiang 2019/8/5 15:40
 */
public class OptionalTest {
    @Test
    public void test1() {
        User user = new User("lisi", 24);
        User zhangSan = Optional.fromNullable(user).or(new User("zhangSan", 27));
    }
}
