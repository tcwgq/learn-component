package com.tcwgq.learn_guava.base;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import org.junit.Test;

/**
 * @author iyb-wangguangqiang 2019/8/5 15:29
 */
public class PreconditionsTest {
    @Test
    public void test1() {
        // 默认值
        Object or = Optional.fromNullable(null).or(new Object());
        System.out.println(or);
    }

    private void add(String name, int age) {
        Preconditions.checkNotNull(name, "name can not be null!");
        Preconditions.checkArgument(age > 18, "age must > 18!");
    }
}
