package com.tcwgq.learn_guava.base;

import com.google.common.base.Splitter;
import org.junit.Test;

import java.util.List;
import java.util.Map;

/**
 * @author iyb-wangguangqiang 2019/8/5 10:45
 */
public class SplitterTest {
    private static final Splitter splitter = Splitter.on(",").omitEmptyStrings().trimResults();

    @Test
    public void test1() {
        String s = splitter.split("1,,2,3,4").toString();
        System.out.println(s);
    }

    @Test
    public void test2() {
        String queryString = "name=zhangSan&age=23&addr=";
        Map<String, String> split = Splitter.on("&").omitEmptyStrings().withKeyValueSeparator("=").split(queryString);
        System.out.println(split);
    }

    @Test
    public void test3() {
        String queryString = "a b c d";
        List<String> stringList = Splitter.on(" ").limit(1).splitToList(queryString);
        System.out.println(stringList);
    }

    @Test
    public void test4() {
        String queryString = "a b c d";
        System.out.println(queryString.split(" ", 1));
    }

}
