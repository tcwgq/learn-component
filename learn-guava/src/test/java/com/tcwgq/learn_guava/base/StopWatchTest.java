package com.tcwgq.learn_guava.base;

import com.google.common.base.Stopwatch;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

/**
 * @author iyb-wangguangqiang 2019/8/5 10:38
 */
public class StopWatchTest {
    @Test
    public void test1() {
        Stopwatch stopwatch = Stopwatch.createUnstarted();
        stopwatch.start();
        for (int i = 0; i < 10000; i++) {
            System.out.println(i);
        }
        stopwatch.stop();
        System.out.println(stopwatch.elapsed(TimeUnit.MILLISECONDS));
    }

}
