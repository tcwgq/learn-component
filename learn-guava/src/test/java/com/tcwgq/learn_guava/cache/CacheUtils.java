package com.tcwgq.learn_guava.cache;

import com.google.common.cache.*;
import org.junit.Test;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * @author admin
 */
public class CacheUtils {
    // 基于泛型的CacheLoader实现
    public <K, V> LoadingCache<K, V> cached(CacheLoader<K, V> cacheLoader) {
        LoadingCache<K, V> cache = CacheBuilder.newBuilder().expireAfterAccess(5, TimeUnit.MINUTES).expireAfterWrite(5, TimeUnit.MINUTES).maximumSize(1024).weakKeys().softValues().removalListener(new RemovalListener<K, V>() {
            @Override
            public void onRemoval(RemovalNotification<K, V> notification) {
                System.out.println(notification.getKey() + " is removed");
            }
        }).build(cacheLoader);
        return cache;
    }

    public LoadingCache<String, String> commonCache(String key) {
        LoadingCache<String, String> cache = cached(new CacheLoader<String, String>() {
            // 注意，CacheLoader不能返回null
            @Override
            public String load(String key) throws Exception {
                return "Helloworld";
            }
        });
        return cache;
    }

    @Test
    public void testCacheLoader() throws ExecutionException {
        LoadingCache<String, String> cache = commonCache("name");
        System.out.println(cache.get("name"));
        cache.apply("age");
        System.out.println(cache.get("age"));
        cache.apply("sex");
        System.out.println(cache.get("sex"));
        cache.invalidate("name");
    }

}
