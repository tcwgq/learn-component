package com.tcwgq.learn_guava.cache;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.junit.Test;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class CacheUtils1 {

	private static Cache<String, String> staticCache = null;

	/**
	 * 基于泛型的Callable实现
	 * 
	 * @throws ExecutionException
	 */

	public static <K, V> Cache<K, V> callableCached() {
		Cache<K, V> cache = CacheBuilder.newBuilder().expireAfterWrite(5, TimeUnit.MINUTES).maximumSize(1024).build();
		return cache;
	}

	public String getCallableCache(final String name) {
		// Callable只有在缓存值不存在时，才会调用
		// 匿名内部类访问局部变量，局部变量必须为final
		try {
			return staticCache.get(name, new Callable<String>() {
				@Override
				public String call() throws Exception {
					System.out.println("select " + name + " from db");
					return "Hello " + name;
				}
			});
		} catch (ExecutionException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Test
	public void testCallable() throws ExecutionException {
		// 确保是同一个Cache实例
		staticCache = callableCached();
		System.out.println(getCallableCache("zhangSan"));
		System.out.println(getCallableCache("liSi"));
		System.out.println(getCallableCache("zhangSan"));
		System.out.println(getCallableCache("zhangSan"));
		System.out.println(getCallableCache("zhangSan"));
		System.out.println(getCallableCache("zhangSan"));
	}

}
