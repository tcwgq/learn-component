package com.tcwgq.learn_guava.cache;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

/**
 * 线程安全的缓存实现机制
 * 
 * @author admin
 *
 */
public class GuavaTest {
	@Test
	public void testCache() {
		// 简单使用
		Cache<String, String> cache = CacheBuilder.newBuilder().concurrencyLevel(3).expireAfterAccess(3600, TimeUnit.DAYS).initialCapacity(1024).maximumSize(2048).expireAfterWrite(3600, TimeUnit.HOURS).build();
		cache.put("name", "zhangSan");
		cache.put("age", "23");
		// 有则返回key对应的value
		System.out.println(cache.getIfPresent("name") + "--" + cache.getIfPresent("age"));
		// 没有则返回null
		System.out.println(cache.getIfPresent("sex"));
	}

	@Test
	public void testLoadingCache() throws ExecutionException {
		// CacheLoader统一指定key的value值
		LoadingCache<String, String> loadingCache = CacheBuilder.newBuilder().maximumSize(1024).build(new CacheLoader<String, String>() {
			@Override
			public String load(String key) throws Exception {
				return "helloworld";
			}
		});
		System.out.println(loadingCache.apply("name"));
		System.out.println(loadingCache.get("name"));
		System.out.println(loadingCache.get("age"));
		System.out.println(loadingCache.apply("sex"));
		System.out.println(loadingCache.get("sex"));
		loadingCache.put("name", "zhangSan");
		System.out.println(loadingCache.get("name"));
	}

	@Test
	public void testCallbackCache() throws ExecutionException {
		// 条目达到1024是开始清理不使用的键值对
		Cache<String, String> cache = CacheBuilder.newBuilder().maximumSize(1024).expireAfterWrite(3600, TimeUnit.SECONDS).build();
		String value = cache.get("name", new Callable<String>() {
			@Override
			public String call() throws Exception {
				return "This is callbackCache";
			}
		});
		System.out.println(value);

		value = cache.get("age", new Callable<String>() {
			@Override
			public String call() throws Exception {
				return "23";
			}
		});

		System.out.println(value);
	}

	@Test
	public void testInvolidate() {
		// 内存回收分为主动移除和被动移除
		// 被动回收
		// 使用expireAfterAccess， 根据某个键值对最后一次访问之后多少时间后移除
		// expireAfterWrite，根据某个键值对被创建或值被替换后多少时间移除
		// maximumsize，根据容量回收内存，容量指缓存中的条目数量，不是内存大小或其他
		Cache<String, String> cache = CacheBuilder.newBuilder().expireAfterAccess(2, TimeUnit.MINUTES).expireAfterWrite(5, TimeUnit.MINUTES).maximumSize(1024).build();
		cache.put("name", "zhangSan");
		cache.put("age", "23");
		cache.put("sex", "male");
		System.out.println(cache.getIfPresent("name"));// zhangSan
		// 主动移除的三个方法
		// invalidate(key)，单独移除
		// invalidateAll()，批量移除
		// invalidateAll()，移除所有
		cache.invalidate("name");
		System.out.println(cache.getIfPresent("name"));// null
		List<String> keys = new ArrayList<String>();
		keys.add("name");
		keys.add("age");
		keys.add("sex");
		Iterator<String> it = keys.iterator();
		cache.invalidateAll(keys);
		System.out.println(cache.getIfPresent("age"));// null
	}
}
