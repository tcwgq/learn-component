package com.tcwgq.learn_guava.collection;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import org.junit.Test;

/**
 * value重复会报错
 *
 * @author iyb-wangguangqiang 2019/8/5 14:16
 */
public class BiMapTest {
    @Test
    public void test1() {
        BiMap<String, String> bimap = HashBiMap.create();
        bimap.put("zhangSan", "23");
        bimap.put("liSi", "24");
        bimap.put("liSi", "25");
        bimap.put("wangwu", "23");// java.lang.IllegalArgumentException: value already present: 23
        System.out.println(bimap.get("zhangSan"));
        System.out.println(bimap.inverse().get("23"));
    }

}
