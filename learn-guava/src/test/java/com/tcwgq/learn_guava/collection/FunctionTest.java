package com.tcwgq.learn_guava.collection;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 函数式编程
 *
 * @author iyb-wangguangqiang 2019/8/5 15:02
 */
public class FunctionTest {
    @Test
    public void test1() {
        ArrayList<String> list = Lists.newArrayList("zhangSan", "liSi", "wangWu");
        Function<String, String> f1 = new Function<String, String>() {
            @Override
            public String apply(String input) {
                // 先截取
                return input.substring(4);
            }
        };

        Function<String, String> f2 = new Function<String, String>() {
            @Override
            public String apply(String input) {
                // 变大写
                return input.toUpperCase();
            }
        };

        Function<String, String> compose = Functions.compose(f1, f2);
        Collection<String> transform = Collections2.transform(list, compose);
        List<String> transform1 = Lists.transform(list, compose);
        Iterable<String> transform2 = Iterables.transform(list, compose);
        System.out.println(transform);
    }

}
