package com.tcwgq.learn_guava.collection;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author iyb-wangguangqiang 2019/8/5 14:25
 */
public class ImmutableTest {
    @Test
    public void test1() {
        ArrayList<Integer> list = Lists.newArrayList(1, 2, 3);
        List<Integer> unmodifiableList = Collections.unmodifiableList(list);
        //unmodifiableList.add(4);
        list.add(4);
        System.out.println(unmodifiableList.size());
    }

    @Test
    public void test2() {
        ArrayList<Integer> list = Lists.newArrayList(1, 2, 3);
        // Defensive Copies，保护性拷贝，避免上述问题
        List<Integer> unmodifiableList = Collections.unmodifiableList(new ArrayList<>(list));
        //unmodifiableList.add(4);
        list.add(4);
        System.out.println(unmodifiableList.size());
    }

    @Test
    public void test3() {
        ImmutableList<String> immutableList = ImmutableList.of("a", "b", "c");
        immutableList.add("d");
        System.out.println(immutableList.size());
    }

    @Test
    public void test4() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        ImmutableList<Integer> immutableList = ImmutableList.copyOf(list);
        list.add(4);
        System.out.println(immutableList.size());
    }

    @Test
    public void test5() {
        ImmutableMap<String, String> map = ImmutableMap.of("firstname", "zhao", "age", "27");
        map.put("addr", "shandong");
        System.out.println(map.size());
    }

}
