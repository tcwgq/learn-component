package com.tcwgq.learn_guava.collection;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import org.junit.Test;

/**
 * @author iyb-wangguangqiang 2019/8/5 14:19
 */
public class MuitiSetTest {
    @Test
    public void test1() {
        Multiset<String> multiset = HashMultiset.create();
        multiset.add("a");
        multiset.add("a");
        multiset.add("b");
        multiset.add("c");
        System.out.println(multiset.count("a"));
        System.out.println(multiset.contains("c"));
    }
}
