package com.tcwgq.learn_guava.collection;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import org.junit.Test;

/**
 * @author iyb-wangguangqiang 2019/8/5 11:11
 */
public class MultimapTest {
    @Test
    public void test1() {
        Multimap<String, String> multimap = HashMultimap.create();
        multimap.put("a", "a");
        multimap.put("a", "b");
        System.out.println(multimap);
        System.out.println(multimap.get("a"));
    }

}
