package com.tcwgq.learn_guava.collection;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import org.junit.Test;

import java.util.List;

/**
 * @author iyb-wangguangqiang 2019/8/5 16:07
 */
public class OreringTest {
    @Test
    public void test1(){
        List<String> list = Lists.newArrayList("zhang", "ai", "li");
        Ordering<String> comparableOrdering = Ordering.natural().nullsFirst();
        List<String> strings = comparableOrdering.sortedCopy(list);
        System.out.println(strings);
    }

}
