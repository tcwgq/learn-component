package com.tcwgq.learn_guava.collection;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author iyb-wangguangqiang 2019/8/5 15:22
 */
public class PredicateTest {
    @Test
    public void test1() {
        ArrayList<String> list = Lists.newArrayList("zhangSan", "liSi", "wangWu");
        Collection<String> filter = Collections2.filter(list, new Predicate<String>() {
            @Override
            public boolean apply(String input) {
                return input.length() > 4;
            }
        });
        Iterable<String> filter1 = Iterables.filter(list, new Predicate<String>() {
            @Override
            public boolean apply(String input) {
                return false;
            }
        });
        System.out.println(filter);
    }

}
