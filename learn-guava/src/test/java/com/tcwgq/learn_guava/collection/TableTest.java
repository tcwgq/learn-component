package com.tcwgq.learn_guava.collection;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import org.junit.Test;

import java.util.Set;

/**
 * @author iyb-wangguangqiang 2019/8/5 14:48
 */
public class TableTest {
    @Test
    public void test1() {
        Table<String, String, Integer> table = HashBasedTable.create();
        table.put("zhang", "语文", 89);
        table.put("zhang", "数学", 98);
        table.put("zhang", "英语", 99);
        table.put("wang", "语文", 99);
        table.put("wang", "数学", 98);
        table.put("wang", "英语", 65);
        Set<Table.Cell<String, String, Integer>> cells = table.cellSet();
        for (Table.Cell cell : cells) {
            System.out.println(cell.getRowKey() + "-" + cell.getColumnKey() + "-" + cell.getValue());
        }
        System.out.println(table.rowKeySet());
        System.out.println(table.columnKeySet());
        System.out.println(table.row("wang"));// {英语=65, 数学=98, 语文=99}
        System.out.println(table.column("语文"));// {wang=99, zhang=89}
    }

}
