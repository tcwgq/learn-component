package com.tcwgq.learn_guava.io;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;


/**
 * @author iyb-wangguangqiang 2019/8/5 16:55
 */
public class FilesTest {
    @Test
    public void test1() {
        System.out.println(System.getProperty("java.io.tmpdir"));
        System.out.println(System.getProperty("user.dir"));
        File tempDir = Files.createTempDir();
        System.out.println(tempDir.getPath());
        File file = new File(tempDir.getPath() + "/a.txt");
        System.out.println(file.getPath());
    }

    @Test
    public void test2() throws IOException {
        File root = new File(System.getProperty("java.io.tmpdir") + "/baotong/");
        if (!root.exists()) {
            root.mkdirs();
        }
        File file = new File(root.getPath() + "/a.txt");
        Files.append("hello", file, Charsets.UTF_8);
        List<String> strings = Files.readLines(file, Charsets.UTF_8);
        System.out.println(strings);
    }

    @Test
    public void test3() throws FileNotFoundException {
        System.out.println(System.getProperty("java.io.tmpdir"));
        File tempDir = Files.createTempDir();
        File file = new File(tempDir.getPath() + "/a.txt");
        System.out.println(file.getPath());
        //IOUtils.copy(null, new FileOutputStream(file));
    }
}
