package com.tcwgq.learn_guava.primitives;

import com.google.common.primitives.Ints;
import org.junit.Test;

import java.util.Arrays;

/**
 * @author iyb-wangguangqiang 2019/8/5 11:04
 */
public class IntsTest {
    @Test
    public void test1() {
        System.out.println(Ints.asList(1, 2, 3, 5));
        System.out.println(Ints.join(",", 1, 2, 3, 5));
        int[] concat = Ints.concat(new int[]{1, 2}, new int[]{3, 4});
        System.out.println(Arrays.toString(concat));
        System.out.println(Ints.max(1, 2, 3, 4));
        System.out.println(Ints.contains(new int[]{1, 2, 3, 4}, 4));
    }
}
