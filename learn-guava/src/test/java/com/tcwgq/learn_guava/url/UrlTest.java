package com.tcwgq.learn_guava.url;

import com.google.common.net.UrlEscapers;
import org.junit.Test;

/**
 * @author tcwgq
 * @date 2020/4/16 11:18
 */
public class UrlTest {
    @Test
    public void test1() {
        // String escape = UrlEscapers.urlFragmentEscaper().escape("https://api.iyunbao.com/tower/open/v1/productinfo/productlink?productId=1042309\\t&accountId=$accountId$");
        // System.out.println(escape);

        String url = "https://api.iyunbao.com/tower/open/v1/productinfo/productlink?productId=1042309\\t&accountId=$accountId$";
        System.out.println(url.replaceAll("^\\s*|\t|\r|\n|\\|$", ""));
    }
}
