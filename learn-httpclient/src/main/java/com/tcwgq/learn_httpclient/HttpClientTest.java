package com.tcwgq.learn_httpclient;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

/**
 * get与post请求
 * 
 * @author admin
 *
 */
public class HttpClientTest {
	private CloseableHttpClient httpclient = HttpClients.createDefault();

	@Test
	public void testGet() {
		try {
			BasicCookieStore cookieStore = new BasicCookieStore();
			httpclient = HttpClients.custom().setDefaultCookieStore(cookieStore).build();
			HttpGet httpGet = new HttpGet("http://localhost:8080");
			// 设置请求头信息
			// httpGet.addHeader("", "");
			CloseableHttpResponse response = httpclient.execute(httpGet);
			try {
				// 终止请求
				// httpGet.abort();
				System.out.println(response.getStatusLine());
				Header[] headers = response.getAllHeaders();
				Header serverHeader = response.getFirstHeader("server");
				System.out.println(serverHeader.getName() + "--" + serverHeader.getValue());
				for (Header header : headers) {
					System.out.println(header.getName() + "--" + header.getValue());
				}
				if (response.getStatusLine().getStatusCode() == 200) {
					HttpEntity entity1 = response.getEntity();
					// 使用EntityUtils工具类解析Entity
					// 这里已经把流关闭了
					String content1 = EntityUtils.toString(entity1, "UTF-8");
					System.out.println(content1);
					System.out.println(entity1.getContentType());
					System.out.println(entity1.getContentLength());
					System.out.println(entity1.getContentEncoding());
					// Attempted read from closed stream
					// InputStream is = entity1.getContent();
					// String content = IOUtils.toString(is, "UTF-8");
					// System.out.println(content);
					// is.close();
					EntityUtils.consume(entity1);
					List<Cookie> cookies = cookieStore.getCookies();
					for (Cookie cookie : cookies) {
						System.out.println(cookie.getName() + "=" + cookie.getValue());
					}
				}
			} finally {
				response.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}

	}

	@Test
	public void testPost() {
		try {
			// 保留cookie
			BasicCookieStore cookieStore = new BasicCookieStore();
			httpclient = HttpClients.custom().setDefaultCookieStore(cookieStore).build();
			HttpPost httpPost = new HttpPost("http://localhost:8080/login/developer?returnUrl=%2F");
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("mobile", "11111111111"));
			nvps.add(new BasicNameValuePair("password", "111111"));
			RequestConfig config = RequestConfig.custom().setConnectTimeout(5000).setConnectionRequestTimeout(5000).setSocketTimeout(5000).build();
			// 设置post的请求体
			httpPost.setEntity(new UrlEncodedFormEntity(nvps));
			httpPost.setConfig(config);
			CloseableHttpResponse response = httpclient.execute(httpPost);
			try {
				System.out.println(response.getStatusLine());
				if (response.getStatusLine().getStatusCode() == 200) {
					HttpEntity entity2 = response.getEntity();
					InputStream is = entity2.getContent();
					String content = IOUtils.toString(is, "UTF-8");
					System.out.println(content);
					is.close();
					EntityUtils.consume(entity2);
					List<Cookie> cookies = cookieStore.getCookies();
					for (Cookie cookie : cookies) {
						System.out.println(cookie.getName() + "=" + cookie.getValue());
					}
				}
			} finally {
				response.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}

	}
}
