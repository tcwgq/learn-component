package com.tcwgq.learn_httpclient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

/**
 * 使用responseHandler处理响应结果
 * 
 * @author admin
 *
 */
public class HttpClientTest1 {
	public static void main(String[] args) throws ClientProtocolException, IOException {
		BasicCookieStore cookieStore = new BasicCookieStore();
		CloseableHttpClient httpclient = HttpClients.custom().setDefaultCookieStore(cookieStore).build();
		HttpPost httpPost = new HttpPost("http://localhost:8080/login/developer?returnUrl=%2F");
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair("mobile", "11111111111"));
		nvps.add(new BasicNameValuePair("password", "111111"));
		RequestConfig config = RequestConfig.custom().setConnectTimeout(5000).setConnectionRequestTimeout(5000).setSocketTimeout(5000).build();
		// 设置post的请求体
		httpPost.setEntity(new UrlEncodedFormEntity(nvps));
		httpPost.setConfig(config);
		// 前面带泛型，后面不带泛型
		ResponseHandler<String> handler = new BasicResponseHandler();
		String response = httpclient.execute(httpPost, handler);
		System.out.println(response);
	}

}
