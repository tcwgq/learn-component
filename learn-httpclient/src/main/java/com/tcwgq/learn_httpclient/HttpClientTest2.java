package com.tcwgq.learn_httpclient;

import java.io.File;
import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

/**
 * post文件上传
 * 
 * @author admin
 *
 */
public class HttpClientTest2 {
	public static void main(String[] args) throws ClientProtocolException, IOException {
		System.out.println("begin......");
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost post = new HttpPost("http://localhost:8080/AServlet");
		// 文件
		FileBody file = new FileBody(new File("d:/a.jpg"));
		// 普通文本字符串
		StringBody string = new StringBody("zhangSan", ContentType.TEXT_PLAIN);
		// 多部件实体
		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		// 添加文件与参数信息
		HttpEntity entity = builder.addPart("file", file).addPart("string", string).build();
		// 设置
		post.setEntity(entity);
		CloseableHttpResponse response = httpclient.execute(post);
		System.out.println(response.getStatusLine().getStatusCode());
		if (response.getStatusLine().getStatusCode() >= 200) {
			HttpEntity httpEntity = response.getEntity();
			String content = EntityUtils.toString(httpEntity);
			System.out.println("content=" + content);
		}
		System.out.println("end......");
	}
}
