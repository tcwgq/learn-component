package com.tcwgq.learn_httpclient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

/**
 * post方法传递参数的两种方式
 * 方式很灵活，开发中按需要使用
 * @author admin
 *
 */
public class HttpClientTest3 {
	public static void main(String[] args) {

	}

	/**
	 * 第一种 直接是key1=value1&key2=value2...这种形式的URL queryString
	 * 
	 * @param url
	 * @param json
	 * @param charSet
	 * @param contentType
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public String getResult(String url, String json, String charSet, String contentType) throws ClientProtocolException, IOException {
		CloseableHttpClient httpclient = HttpClients.custom().setMaxConnPerRoute(100).build();
		HttpPost post = new HttpPost(url);
		StringEntity entity = new StringEntity(json, contentType);
		entity.setContentType("application/x-www-form-urlencoded");
		entity.setContentEncoding(new BasicHeader("Content-Encoding", charSet));
		post.setEntity(entity);
		CloseableHttpResponse response = httpclient.execute(post);
		HttpEntity httpEntity = response.getEntity();
		String content = EntityUtils.toString(httpEntity);
		return content;
	}

	/**
	 * 使用NameValuePair的形式
	 * 
	 * @throws IOException
	 * @throws ClientProtocolException
	 */
	public String getPostContent() throws ClientProtocolException, IOException {
		CloseableHttpClient httpclient = HttpClients.custom().setMaxConnPerRoute(100).setMaxConnPerRoute(200).setMaxConnTotal(1000).build();
		HttpPost httpPost = new HttpPost("http://localhost:8080/login/developer?returnUrl=%2F");
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair("mobile", "11111111111"));
		nvps.add(new BasicNameValuePair("password", "111111"));
		// 设置post的请求体
		httpPost.setEntity(new UrlEncodedFormEntity(nvps));
		CloseableHttpResponse response = httpclient.execute(httpPost);
		HttpEntity entity = response.getEntity();
		String content = EntityUtils.toString(entity);
		return content;
	}
}
