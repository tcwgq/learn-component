package com.tcwgq.learn_httpclient;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

public class HttpClientUtilsTest {
	@Test
	public void testGet() {
		CloseableHttpClient httpclient = HttpclientUtils.getConnection();
		Map<String, String> params = new HashMap<String, String>();
		params.put("mobile", "11111111111");
		params.put("password", "111111");
		HttpUriRequest request = HttpclientUtils.getRequestMethod("http://localhost:8080", params, "get");
		try {
			CloseableHttpResponse response = httpclient.execute(request);
			if (response.getStatusLine().getStatusCode() == 200) {
				HttpEntity entity = response.getEntity();
				String content = EntityUtils.toString(entity);
				System.out.println(content);
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testPost() {
		CloseableHttpClient httpclient = HttpclientUtils.getConnection();
		Map<String, String> params = new HashMap<String, String>();
		params.put("mobile", "11111111111");
		params.put("password", "111111");
		HttpUriRequest request = HttpclientUtils.getRequestMethod("http://localhost:8080/login/developer?returnUrl=%2F", params, "post");
		try {
			CloseableHttpResponse response = httpclient.execute(request);
			System.out.println(response.getStatusLine());
			if (response.getStatusLine().getStatusCode() == 200) {
				Header[] headers = response.getAllHeaders();
				for (Header header : headers) {
					System.out.println(header.getName() + "=" + header.getValue());
				}
				HttpEntity entity = response.getEntity();
				String content = EntityUtils.toString(entity);
				System.out.println(content);
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
