package com.tcwgq.learn_httpclient;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;

public class HttpclientUtils {
	// 管理http连接
	private static PoolingHttpClientConnectionManager manager = null;
	// 创建http连接
	private static HttpClientBuilder builder = null;
	// 设置请求参数
	private static RequestConfig config = null;
	static {
		// 主机及端口地址
		HttpHost host = new HttpHost("localhost", 8080);
		manager = new PoolingHttpClientConnectionManager();
		// 最大连接数
		manager.setMaxTotal(1000);
		// 路由的默认最大连接数
		manager.setDefaultMaxPerRoute(200);
		// 路由的最大连接数，即本机的最大连接数
		manager.setMaxPerRoute(new HttpRoute(host), 20);
		builder = HttpClientBuilder.create().setConnectionManager(manager);
		// builder = HttpClients.custom().setConnectionManager(manager);
		config = RequestConfig.custom().setConnectTimeout(5000).setConnectionRequestTimeout(5000).setSocketTimeout(5000).build();
		// 此处也可以设置请求的配置
		builder.setDefaultRequestConfig(config);
	}

	public static CloseableHttpClient getConnection() {
		return builder.build();
	}

	public static HttpUriRequest getRequestMethod(String url, Map<String, String> map, String method) {
		List<NameValuePair> list = new ArrayList<NameValuePair>();
		for (Map.Entry<String, String> entry : map.entrySet()) {
			String name = entry.getKey();
			String value = entry.getValue();
			list.add(new BasicNameValuePair(name, value));
		}
		if ("get".equals(method.toLowerCase())) {
			// 此处还可以设置请求头信息，编码等
			return RequestBuilder.get().setUri(url).setHeader("hello", "world").setCharset(Charset.forName("UTF-8")).addParameters(list.toArray(new BasicNameValuePair[list.size()])).setConfig(config).build();
		} else if ("post".equals(method.toLowerCase())) {
			return RequestBuilder.post().setUri(url).addParameters(list.toArray(new BasicNameValuePair[list.size()])).setConfig(config).build();
		}
		return null;
	}
}
