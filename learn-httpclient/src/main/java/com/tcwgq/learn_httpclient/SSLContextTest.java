package com.tcwgq.learn_httpclient;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

/**
 * 获取httpclient的多种灵活方式，支持HTTPS
 * 
 * @author admin
 *
 */
public class SSLContextTest {
	@Test
	public void testContext() {
		try {
			// 使用连接池管理http连接
			PoolingHttpClientConnectionManager manager = new PoolingHttpClientConnectionManager();
			manager.setDefaultMaxPerRoute(200);
			manager.setDefaultMaxPerRoute(100);
			manager.setMaxTotal(400);
			// ssl上下文
			SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, new TrustStrategy() {
				@Override
				public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
					return true;
				}
			}).build();
			// 创建ssl socket连接工厂
			// ssl上下文，支持的协议，支持的加密方式，主机验证
			SSLConnectionSocketFactory factory = new SSLConnectionSocketFactory(sslContext, new String[] { "TLSv1", "SSL" }, null, SSLConnectionSocketFactory.getDefaultHostnameVerifier());
			// 创建http连接
			RequestConfig config = RequestConfig.custom().setConnectionRequestTimeout(5000).setConnectTimeout(5000).setSocketTimeout(5000).build();
			// builder方式获取http连接对象
			HttpClientBuilder builder = HttpClientBuilder.create().setConnectionManager(manager).setDefaultRequestConfig(config).setSSLSocketFactory(factory);
			// 自定义方式获得http连接对象
			// CloseableHttpClient httpclient =
			// HttpClients.custom().setConnectionManager(manager).setDefaultRequestConfig(config).setSSLSocketFactory(factory).build();
			// 上面两种方式的底层实现是相同的，都是通过HttpClientBuilder方式创建对象的
			CloseableHttpClient httpclient = builder.build();
			HttpGet httpGet = new HttpGet("https://www.baidu.com");
			try {
				CloseableHttpResponse response = httpclient.execute(httpGet);
				if (response.getStatusLine().getStatusCode() == 200) {
					HttpEntity entity = response.getEntity();
					String content = EntityUtils.toString(entity, "UTF-8");
					System.out.print(content);
				}
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException e) {
			e.printStackTrace();
		}
	}
}
