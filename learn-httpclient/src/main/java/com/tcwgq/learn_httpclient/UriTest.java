package com.tcwgq.learn_httpclient;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

public class UriTest {
	public static void main(String[] args) throws URISyntaxException {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("name", "zhangsan"));
		params.add(new BasicNameValuePair("age", "23"));
		// get请求
		// 方式一
		String queryString = URLEncodedUtils.format(params, "UTF-8");
		System.out.println(queryString);//name=zhangsan&age=23
		System.out.println("--------------------");
		// 方式二
		// 已过时
		// URI uri = URIUtils.createURI("http", "localhost", 8080, "/AServlet","name=zhangsan&age=23", null);
		// 推荐使用URIBuilder
		URIBuilder builder = new URIBuilder();
		builder.addParameter("name", "zhangsan");
		builder.addParameter("age", "23");
		builder.addParameter("sex", "male");
		URI uri = builder.build();
		System.out.println(uri.toString());
		System.out.println(builder.toString());//?name=zhangsan&age=23&sex=male
		// post请求
		try {
			UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params, "UTF-8");
			System.out.println(entity.toString());// [Content-Type: application/x-www-form-urlencoded; charset=UTF-8,Content-Length: 20,Chunked: false]
			System.out.println(entity.getContentLength());//20
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
}
