package com.tcwgq.jackson.test;

import java.io.IOException;
import java.util.Date;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.junit.Test;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.tcwgq.model.User;

/**
 * @author tcwgq
 * @time 2017年12月5日下午5:44:17
 * @email qiang.wang@guanaitong.com
 */
public class JacksonTest {
    @Test
    public void test1() {
        User user = new User();
        user.setName("zhangSan");
        user.setAge(23);
        user.setEmail("zs@163.com");
        user.setBirthday(new Date());
        // 个性化设置
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addSerializer(Date.class, new JsonSerializer<Date>() {
            @Override
            public void serialize(Date value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
                gen.writeString(DateFormatUtils.format(value, "yyyy-MM-dd hh:mm:ss"));
            }
        });
        ObjectMapper mapper = new ObjectMapper().//
                setSerializationInclusion(JsonInclude.Include.NON_NULL).// 不包含null的属性
                configure(SerializationFeature.INDENT_OUTPUT, true).// 缩进处理
                registerModule(simpleModule);// 个性化定制
        try {
            String asString = mapper.writeValueAsString(user);
            System.out.println(asString);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
