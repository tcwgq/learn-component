package com.tcwgq.jackson.test;

import java.io.IOException;
import java.util.Date;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.junit.Test;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.tcwgq.model.User;

/**
 * @author tcwgq
 * @time 2017年12月6日下午2:49:21
 * @email qiang.wang@guanaitong.com
 */
public class JacksonTest1 {
    @Test
    public void test1() {
        User user = new User();
        user.setName("zhangSan");
        user.setAge(23);
        user.setEmail("zs@163.com");
        user.setBirthday(new Date());
        // 个性化设置
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addSerializer(Date.class, new JsonSerializer<Date>() {
            @Override
            public void serialize(Date value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
                gen.writeString(DateFormatUtils.format(value, "yyyy-MM-dd hh:mm:ss"));
            }
        });
        ObjectMapper mapper = new ObjectMapper().//
                setSerializationInclusion(JsonInclude.Include.NON_NULL).// 不包含null的属性
                configure(SerializationFeature.INDENT_OUTPUT, true).// 缩进处理
                registerModule(simpleModule);// 个性化定制
        JavaType javaType = mapper.getTypeFactory().constructType(User.class.getGenericSuperclass());
        try {
            String asString = mapper.writerFor(javaType).writeValueAsString(user);
            System.out.println(asString);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test2() throws IOException {

        String jsonstr = "{\"msg\":{\"head\":{\"version\":\"1.0\",\"bizcode\":\"1006\",\"senddate\":\"20140827\",\"sendtime\":\"110325\",\"seqid\":\"1\"},\"body\":{\"datalist\":\"wahaha\",\"rstcode\":\"000000\",\"rstmsg\":\"成功\"}}}";
        ObjectMapper mapper = new ObjectMapper();
        // 允许出现特殊字符和转义符
        mapper.configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
        JsonNode root = mapper.readTree(jsonstr);
        // path与get作用相同,但是当找不到该节点的时候,返回missing node而不是Null.
        JsonNode msg = root.path("msg");
        JsonNode head = msg.path("head");
        JsonNode body = msg.path("body");

        String bizcode = head.path("bizcode").asText();
        String datalist = body.path("datalist").asText();

        System.err.println(bizcode);
        System.err.println(datalist);

        System.err.println(root.path("msg").path("body").path("datalist").asText());
    }

    @Test
    public void test3() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(SerializationFeature.INDENT_OUTPUT, true);

            // 允许出现特殊字符和转义符
            mapper.configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);

            ObjectNode root = mapper.createObjectNode();
            ObjectNode msg = mapper.createObjectNode();

            ObjectNode head = mapper.createObjectNode();
            head.put("version", "1.0");
            head.put("bizcode", "1006");
            head.put("senddate", "20140827");
            head.put("sendtime", "110325");
            head.put("seqid", "1");

            ObjectNode body = mapper.createObjectNode();
            body.put("datalist", "wahaha");
            body.put("rstcode", "000000");
            body.put("rstmsg", "成功");

            msg.put("head", head);
            msg.put("body", body);
            root.put("msg", msg);

            System.out.println(mapper.writeValueAsString(root));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
