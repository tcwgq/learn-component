package com.tcwgq.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author tcwgq
 * @time 2017年12月5日下午5:43:22
 * @email qiang.wang@guanaitong.com
 */
public class User {
    private String name;
    // @JsonIgnore
    private Integer age;
    @JsonProperty
    private String email;
    // @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date birthday;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "User [name=" + name + ", age=" + age + ", email=" + email + ", birthday=" + birthday + "]";
    }

}
