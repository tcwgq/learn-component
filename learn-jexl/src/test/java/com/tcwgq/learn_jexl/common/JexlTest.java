package com.tcwgq.learn_jexl.common;

import com.tcwgq.learn_jexl.bean.Context;
import com.tcwgq.learn_jexl.bean.MathUtil;
import com.tcwgq.learn_jexl.bean.Person;
import com.tcwgq.learn_jexl.bean.Udf;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.jexl3.*;
import org.apache.commons.jexl3.internal.Engine;
import org.junit.Test;

import java.util.*;

@Slf4j
public class JexlTest {
    JexlEngine jexlEngine = new JexlBuilder()
            .debug(log.isDebugEnabled())
            .silent(true)
            .strict(true)
            .namespaces(Collections.singletonMap("udf", new Udf()))
            .create();

    @Test
    public void test1() {
        Map<String, Object> map = new HashMap<>();
        map.put("name", "zhangSan");
        map.put("age", 23);
        Context context = new Context(map);
        JexlExpression expression = jexlEngine.createExpression("request.age");
        ObjectContext<Context> objectContext = new ObjectContext<>(jexlEngine, context);
        Object evaluate = expression.evaluate(objectContext);
        System.out.println(evaluate);
    }

    /**
     * 对象
     */
    @Test
    public void test2() {
        JexlEngine engine = new Engine();//创建表达式引擎对象
        JexlContext context = new MapContext();//创建Context设值对象
        String expressionStr = "array.size()";//表达式，表达式可以是数组的属性，元素等
        List<String> array = new ArrayList<>();//创建一个列表
        array.add("this is an array");
        array.add("hello");
        context.set("array", array);//使用context对象将表达式中用到的值设置进去，必须是所有用到的值
        JexlExpression expression = engine.createExpression(expressionStr);//使用引擎创建表达式对象
        Object o = expression.evaluate(context);//使用表达式对象开始计算
        System.out.println(o);//输出：2
    }

    /**
     * 语句块 if
     */
    @Test
    public void test3() {
        JexlEngine engine = new Engine();// 创建表达式引擎对象
        JexlContext context = new MapContext();// 创建Context设值对象
        String expressionStr = "if(a>b){c=a;}else{c=b;}";// 表达式，表达式为逻辑语句
        context.set("a", 1);
        context.set("b", 2);
        JexlExpression expression = engine.createExpression(expressionStr);//使用引擎创建表达式对象
        Object evaluate = expression.evaluate(context);//使用表达式对象开始计算
        System.out.println(evaluate);// 输出：2
    }

    /**
     * 语句块 while
     */
    @Test
    public void test31() {
        JexlEngine engine = new Engine();// 创建表达式引擎对象
        JexlContext context = new MapContext();// 创建Context设值对象
        context.set("a", 1);
        context.set("b", 2);
        String expressionStr = "while(a<b){a=a+b;}";
        JexlExpression expression = engine.createExpression(expressionStr);//使用引擎创建表达式对象
        Object o = expression.evaluate(context);//使用表达式对象开始计算
        System.out.println(o);// 输出：3
    }

    @Test
    public void test4() {
        JexlEngine engine = new Engine();//创建表达式引擎对象
        JexlContext context = new MapContext();//创建Context设值对象
        String expressionStr = "person.getAge()";//表达式，表达式为对象调用方法，当然也可以是类调用方法
        Person person = new Person("zhangSan", 23);
        context.set("person", person);
        JexlExpression expression = engine.createExpression(expressionStr);//使用引擎创建表达式对象
        Object evaluate = expression.evaluate(context);//使用表达式对象开始计算
        //输出：23
        System.out.println(evaluate);

        expressionStr = "System.out.println(\"123123\")";//表达式为类调用方法
        expression = engine.createExpression(expressionStr);//使用引擎创建表达式对象
        evaluate = expression.evaluate(context);//使用表达式对象开始计算
        //输出：123123
        System.out.println(evaluate);
    }

    @Test
    public void test5() {
        JexlEngine jexlEngine = new JexlBuilder()
                .namespaces(Collections.singletonMap("math", new MathUtil()))
                .create();
        JexlContext context = new MapContext();
        context.set("pi", Math.PI);
        JexlExpression expression = jexlEngine.createExpression("math:cos(pi)");
        Object evaluate = expression.evaluate(context);
        System.out.println(evaluate);
    }
}
