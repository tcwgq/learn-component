package com.tcwgq.learn_jexl.common;

import com.tcwgq.learn_jexl.bean.Context;
import com.tcwgq.learn_jexl.bean.Udf;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.jexl3.JexlBuilder;
import org.apache.commons.jexl3.JexlEngine;
import org.apache.commons.jexl3.JexlExpression;
import org.apache.commons.jexl3.ObjectContext;
import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class JexlTest1 {
    JexlEngine jexlEngine = new JexlBuilder()
            .debug(log.isDebugEnabled())
            .silent(true)
            .strict(true)
            .namespaces(Collections.singletonMap("udf", new Udf()))
            .create();

    @Test
    public void test1() {
        Map<String, Object> map = new HashMap<>();
        double a = 0;
        double b = 0.125;
        double c = 2.5;
        map.put("amount", a);
        map.put("amount1", b);
        map.put("amount2", c);
        Context context = new Context(map);
        ObjectContext<Context> objectContext = new ObjectContext<>(jexlEngine, context);
        JexlExpression expression = jexlEngine.createExpression("request.amount  >= 0");
        Object evaluate = expression.evaluate(objectContext);
        System.out.println(evaluate);

        JexlExpression expression1 = jexlEngine.createExpression("request.amount1 > 0");
        Object evaluate1 = expression1.evaluate(objectContext);
        System.out.println(evaluate1);

        JexlExpression expression2 = jexlEngine.createExpression("request.amount2 > 0");
        Object evaluate2 = expression2.evaluate(objectContext);
        System.out.println(evaluate2);
    }

}
