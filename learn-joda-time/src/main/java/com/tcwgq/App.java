package com.tcwgq;

import org.joda.time.DateTime;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        DateTime dateTime = new DateTime();
        DateTime.Property property = dateTime.millisOfSecond();
        System.out.println(property);
    }
}
