package com.tcwgq;

import org.joda.time.*;
import org.joda.time.format.DateTimeFormat;
import org.junit.Test;

/**
 * Created by wangguangqiang on 2019/11/8 16:31
 */
public class DateTimeCalulateTest {
    @Test
    public void test1() {
        DateTime d1 = DateTime.parse("2019-08-23 12:20:38", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        DateTime d2 = DateTime.parse("2019-09-28 08:18:20", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        //DateTime d1 = DateTime.now();
        //DateTime d2 = DateTime.now();
        System.out.println(Seconds.secondsBetween(d1, d2).getSeconds());
        System.out.println(Minutes.minutesBetween(d1, d2).getMinutes());
        System.out.println(Hours.hoursBetween(d1, d2).getHours());
        System.out.println(Days.daysBetween(d1, d2).getDays());
        System.out.println(Months.monthsBetween(d1, d2).getMonths());
        System.out.println(Years.yearsBetween(d1, d2).getYears());
    }

    @Test
    public void test2() {
        DateTime d1 = DateTime.parse("2019-08-23 12:20:38", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        DateTime d2 = DateTime.parse("2019-09-28 08:18:20", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        //DateTime d1 = DateTime.now();
        //DateTime d2 = DateTime.now();
        Period period = new Interval(d1, d2).toPeriod();
        System.out.println(period.getSeconds());
        System.out.println(period.getMinutes());
        System.out.println(period.getHours());
        System.out.println(period.getDays());
        System.out.println(period.getMonths());
        System.out.println(period.getYears());
    }
}
