package com.tcwgq;

import org.joda.time.*;
import org.joda.time.chrono.BuddhistChronology;
import org.joda.time.chrono.GJChronology;
import org.joda.time.format.DateTimeFormat;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Set;

/**
 * Created by wangguangqiang on 2019/11/7 11:46
 */
public class DateTimeTest {
    /**
     * create
     */
    @Test
    public void test1() {
        // 构造当前时间
        DateTime dateTime = new DateTime();
        System.out.println(dateTime);
        // 等同于上面
        DateTime dateTime1 = DateTime.now();
        System.out.println(dateTime1);
        // 世界调整时间，比东八区少8小时
        DateTime dateTime2 = new DateTime(DateTimeZone.UTC);
        System.out.println(dateTime2);

        Set<String> availableIDs = DateTimeZone.getAvailableIDs();
        System.out.println(availableIDs);

        DateTimeZone aDefault = DateTimeZone.getDefault();
        System.out.println(aDefault);// Asia/Shanghai
        DateTime dateTime5 = new DateTime(aDefault);
        System.out.println(dateTime5);

        DateTime dateTime3 = new DateTime(DateTimeZone.forID("Africa/Abidjan"));
        System.out.println(dateTime3);

        //DateTime dateTime4 = new DateTime(DateTimeZone.forID("+8"));
        //System.out.println(dateTime4);

        DateTimeZone dateTimeZone = DateTimeZone.forID("Asia/Tokyo");
        GJChronology gjChronology = GJChronology.getInstance(dateTimeZone);
        DateTime dateTime6 = new DateTime(gjChronology);
        System.out.println(dateTime6);

        DateTimeZone dateTimeZone1 = DateTimeZone.forOffsetHours(8);
        DateTime dateTime7 = new DateTime(dateTimeZone1);
        System.out.println(dateTime7);
        // 改变时区
        DateTime dateTime4 = new DateTime();
        DateTime dateTime8 = dateTime4.withZone(DateTimeZone.forID("Asia/Tokyo"));
        System.out.println(dateTime8);
        // 改变Chronology
        DateTime dateTime9 = new DateTime();
        DateTime dateTime10 = dateTime9.withChronology(BuddhistChronology.getInstance());
        System.out.println(dateTime10);
        // 使用Date
        DateTime dateTime11 = new DateTime(new Date());
        System.out.println(dateTime11);
    }

    /**
     * 使用jdk时间类初始化
     */
    @Test
    public void test2() {
        DateTime dateTime = new DateTime(2019, 8, 1, 15, 20, 11);
        System.out.println(dateTime);
        System.out.println(new DateTime(System.currentTimeMillis()));
        System.out.println(new DateTime(new Date()));

        Calendar instance = Calendar.getInstance();
        DateTime dateTime1 = new DateTime(instance);
        System.out.println(dateTime1);

        DateTime dateTime2 = new DateTime("2006-01-26T13:30:00-06:00");
        System.out.println(dateTime2);

        DateTime dateTime3 = DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime("2019-09-21 18:21:39");
        System.out.println(dateTime3);

        System.out.println(new DateTime(new Date()));
        System.out.println(new DateTime(Calendar.getInstance()));
        // UTC时间=GMT时间，只是叫法不同而已
        System.out.println(new DateTime(DateTimeZone.UTC));
        // 东八区时间
        System.out.println(new DateTime(DateTimeZone.forOffsetHours(8)));
    }

    /**
     * 获取
     */
    @Test
    public void test3() {
        DateTime dateTime = DateTime.now();
        System.out.println(dateTime.getDayOfMonth());
        System.out.println(dateTime.getDayOfWeek());
        System.out.println(dateTime.getDayOfYear());
        // 获取今天所在星期的最大值得日期
        System.out.println(dateTime.dayOfWeek().withMaximumValue().getDayOfMonth());
        System.out.println(dateTime.dayOfMonth().withMaximumValue().getDayOfMonth());
        System.out.println(dateTime.dayOfYear().withMaximumValue().getDayOfYear());
        System.out.println(dateTime.getMillis());
        System.out.println(dateTime.getMillisOfDay());
        System.out.println(dateTime.getMillisOfSecond());
        System.out.println(dateTime.getSecondOfDay());
        System.out.println(dateTime.getSecondOfMinute());
        System.out.println(dateTime.get(DateTimeFieldType.dayOfMonth()));
    }

    /**
     * 转化为jdk相关时间类
     */
    @Test
    public void test4() {
        DateTime dateTime = new DateTime();
        System.out.println(dateTime.toDate());
        System.out.println(dateTime.toDateTime());
        System.out.println(dateTime.toLocalDate());
        System.out.println(dateTime.toLocalTime());
        System.out.println(dateTime.toLocalDateTime());
        Calendar calendar = dateTime.toCalendar(Locale.SIMPLIFIED_CHINESE);
        System.out.println(calendar);
    }

    @Test
    public void test5() {
        DateTime dateTime = new DateTime();
        System.out.println(dateTime.dayOfWeek().getAsShortText());
        System.out.println(dateTime.dayOfWeek().getAsShortText(Locale.SIMPLIFIED_CHINESE));
        System.out.println(dateTime.dayOfMonth().getAsShortText());

        System.out.println(dateTime.toString());
        System.out.println(dateTime.toString("yyyy-MM-dd"));
        // 使用DateTimeFormat创建DateTimeFormatter对象
        System.out.println(dateTime.toString(DateTimeFormat.forPattern("yyyy-MM-dd")));
        System.out.println(dateTime.toString("yyyy-MM-dd HH:mm:ss", Locale.SIMPLIFIED_CHINESE));

        System.out.println(DateTimeFormat.forPattern("yyyy-MM-dd").print(dateTime));

        Instant instant = DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime("2018-12-24").toInstant();
        System.out.println(instant);
        System.out.println(instant.getMillis());

        // 格式化
        System.out.println(DateTimeFormat.forStyle("LL").print(new DateTime()));
        System.out.println(DateTimeFormat.forStyle("FF").print(new DateTime()));
        System.out.println(DateTimeFormat.shortDate().print(new DateTime()));
        System.out.println(DateTimeFormat.shortDateTime().print(new DateTime()));
        System.out.println(DateTimeFormat.shortTime().print(new DateTime()));
        System.out.println(DateTimeFormat.forPattern(DateTimeFormat.patternForStyle("FF", Locale.ENGLISH)).print(new DateTime()));
    }

    @Test
    public void test6() {
        System.out.println(DateTime.now().toInstant().getMillis());
        System.out.println(DateTime.now().dayOfMonth().toInterval());
        System.out.println(DateTime.now().dayOfWeek().toInterval());
        System.out.println(DateTime.now().dayOfYear().toInterval());
        System.out.println(DateTime.now().hourOfDay().toInterval());
        Duration duration = Duration.standardDays(1);
        System.out.println(duration);
    }
}
