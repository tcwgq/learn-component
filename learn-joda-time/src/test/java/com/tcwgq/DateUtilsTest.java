package com.tcwgq;

import org.junit.Test;

import java.util.Date;

/**
 * Created by wangguangqiang on 2019/11/8 17:09
 */
public class DateUtilsTest {
    @Test
    public void test1() {
        System.out.println(DateUtils.startOfDay());
        System.out.println(DateUtils.endOfDay());
        System.out.println(DateUtils.startOfDay(DateUtils.SEC_FULL_PATTERN));
    }

    @Test
    public void test2() {
        Date start = DateUtils.parse("2019-10-21", "yyyy-MM-dd");
        Date end = DateUtils.parse("2019-12-01", "yyyy-MM-dd");
        int i = DateUtils.betweenDays(start, end);
        System.out.println(i);
    }

    @Test
    public void currentMills() {
    }

    @Test
    public void currentSeconds() {
    }

    @Test
    public void currentMinutes() {
    }

    @Test
    public void startOfDay() {
    }

    @Test
    public void endOfDay() {
    }

    @Test
    public void testStartOfDay() {
    }

    @Test
    public void testEndOfDay() {
    }

    @Test
    public void startOfMonth() {
    }

    @Test
    public void endOfMonth() {
    }

    @Test
    public void testStartOfDay1() {
    }

    @Test
    public void testEndOfDay1() {
    }

    @Test
    public void daysAfter() {
    }

    @Test
    public void betweenMills() {
        Date current = new Date();
        Date start = DateUtils.parse("2019-10-21 12:00:00", DateUtils.SEC_FULL_PATTERN);
        System.out.println(start);
        long mills = DateUtils.betweenMills(start, current);
        System.out.println(mills);
    }

    @Test
    public void betweenSeconds() {
        Date current = new Date();
        Date start = DateUtils.parse("2019-10-21 12:00:00", DateUtils.SEC_FULL_PATTERN);
        System.out.println(start);
        int seconds = DateUtils.betweenSeconds(start, current);
        System.out.println(seconds);
    }

    @Test
    public void betweenMinutes() {
        Date current = new Date();
        Date start = DateUtils.parse("2019-10-21 12:00:00", DateUtils.SEC_FULL_PATTERN);
        System.out.println(start);
        int minutes = DateUtils.betweenMinutes(start, current);
        System.out.println(minutes);
    }

    @Test
    public void betweenHours() {
        Date current = new Date();
        Date start = DateUtils.parse("2019-10-21 12:00:00", DateUtils.SEC_FULL_PATTERN);
        System.out.println(start);
        int hours = DateUtils.betweenHours(start, current);
        System.out.println(hours);
    }

    @Test
    public void betweenDays() {
        Date current = new Date();
        Date start = DateUtils.parse("2019-10-21 12:00:00", DateUtils.SEC_FULL_PATTERN);
        System.out.println(start);
        int days = DateUtils.betweenDays(start, current);
        System.out.println(days);
    }

    @Test
    public void betweenMonths() {
        Date current = new Date();
        Date start = DateUtils.parse("2016-09-01 12:00:00", DateUtils.SEC_FULL_PATTERN);
        System.out.println(start);
        int months = DateUtils.betweenMonths(start, current);
        System.out.println(months);
    }

    @Test
    public void betweenYears() {
        Date current = new Date();
        Date start = DateUtils.parse("2011-10-21 12:00:00", DateUtils.SEC_FULL_PATTERN);
        System.out.println(start);
        int years = DateUtils.betweenYears(start, current);
        System.out.println(years);
    }

    @Test
    public void startOfDaysAfter() {
    }

    @Test
    public void endOfDaysAfter() {
    }

    @Test
    public void format() {
    }

    @Test
    public void testFormat() {
    }

    @Test
    public void parse() {
    }

}
