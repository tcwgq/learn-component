package com.tcwgq;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.format.DateTimeFormat;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import java.util.TimeZone;

/**
 * Created by wangguangqiang on 2019/11/8 10:52
 */
public class LocalDateTest {
    @Test
    public void test1() {
        System.out.println(LocalDate.now());
        System.out.println(new LocalDate());
        /**
         * ID - the ID for a TimeZone, either an abbreviation such as "PST",
         * a full name such as "America/Los_Angeles", or a custom ID such as "GMT-8:00".
         * Note that the support of abbreviations is for JDK 1.1.x compatibility only and full names should be used.
         */
        System.out.println(new LocalDate(DateTimeZone.forTimeZone(TimeZone.getTimeZone("GMT+8:00"))));
        System.out.println(new LocalDate(DateTimeZone.UTC));
        // 东八区
        System.out.println(new LocalDate(DateTimeZone.forOffsetHours(8)));
        Set<String> availableIDs = DateTimeZone.getAvailableIDs();
        availableIDs.stream().filter(s -> s.startsWith("Asia")).limit(5).forEach(System.out::println);
        // 上海
        System.out.println(new LocalDate(DateTimeZone.forID("Asia/Shanghai")));
        // 东八区
        System.out.println(new LocalDate(DateTimeZone.forID("GMT+0")));

        System.out.println(new LocalDate(2019, 8, 23));
        // 根据不同年表初始化
        System.out.println(new LocalDate(2018, 3, 28, ISOChronology.getInstance()));
        System.out.println(new LocalDate("2019-12-24"));
        System.out.println(new LocalDate(System.currentTimeMillis()));
        System.out.println(new LocalDate(new Date()));

        System.out.println(LocalDate.fromCalendarFields(Calendar.getInstance()));
        System.out.println(LocalDate.fromDateFields(new Date()));

        System.out.println(LocalDate.parse("2019-11-08"));
        // 少个0也没错
        System.out.println(LocalDate.parse("2019-11-8"));
        System.out.println(LocalDate.parse("2019-11-12", DateTimeFormat.forStyle("M-")));
    }

    @Test
    public void test2() {
        System.out.println(LocalDate.now().getDayOfYear());
        System.out.println(LocalDate.now().getDayOfWeek());
        System.out.println(LocalDate.now().getCenturyOfEra());
        System.out.println(LocalDate.now().getDayOfYear());
        System.out.println(LocalDate.now().dayOfMonth().withMaximumValue());
    }

    @Test
    public void test3() {
        System.out.println(LocalDate.now().toDateTimeAtCurrentTime());
        // 一天的起止时刻
        System.out.println(LocalDate.now().toDateTimeAtStartOfDay().toString());
        // 把日期转换为毫秒
        System.out.println(LocalDate.now().toDateTimeAtStartOfDay().getMillis());

        System.out.println(LocalDate.now().toInterval());

        System.out.println(LocalDate.now().toDateTime(LocalTime.now()));
    }
}
