package com.tcwgq.log4j.test;

import java.text.MessageFormat;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.tcwgq.po.User;

/**
 * @author tcwgq
 * @time 2017年10月9日下午1:47:44
 * @email qiang.wang@guanaitong.com
 */
public class Log4jTest {
    private Logger logger = LoggerFactory.getLogger(Log4jTest.class);

    @Test
    public void test1() {
        User user = new User();
        user.setName("zhangSan");
        user.setAge(23);
        logger.info("user {}", user);
    }

    @Test
    public void test2() {
        User user = new User();
        user.setName("zhangSan");
        user.setAge(23);
        logger.info("user {}", JSONObject.toJSONString(user));
    }

    @Test
    public void test3() {
        User user = new User();
        user.setName("zhangSan");
        user.setAge(23);
        logger.info("user {}", "Hello");
    }

    @Test
    public void test4() {
        // 此种情况打印不出来数据
        // 底层使用了MessageFormat
        User user = new User();
        user.setName("zhangSan");
        user.setAge(23);
        logger.info("user: ", user);// user:
        logger.info("user: ", JSONObject.toJSONString(user)); // user:
    }

    @Test
    public void test5() {
        // MessageFormat的使用
        String pattern = "{0} is a man, he is {1, number,Integer}.";
        String s = MessageFormat.format(pattern, new Object[] { "zhangSan", 23 });
        System.out.println(s);
    }

}
