package com.tcwgq.log4j.test;

import java.math.BigDecimal;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tcwgq.po.User;

/**
 * @author tcwgq
 * @time 2017年10月11日下午3:30:09
 * @email qiang.wang@guanaitong.com
 */
public class Log4jTest1 {
    private Logger logger = LoggerFactory.getLogger(Log4jTest1.class);

    @Test
    public void test() {
        User user = new User();
        user.setName("zhangSan");
        user.setAge(23);
        logger.debug("user: {}", user);
    }

    @Test
    public void test1() {
        System.out.println(new BigDecimal("1.12"));
        System.out.println(BigDecimal.valueOf(1.12));
        System.out.println(new BigDecimal(1.12));
    }
}
