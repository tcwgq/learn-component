package com.tcwgq.po;

/**
 * @author tcwgq
 * @time 2017年10月9日下午1:46:27
 * @email qiang.wang@guanaitong.com
 */
public class User {
    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "User [name=" + name + ", age=" + age + "]";
    }

}
