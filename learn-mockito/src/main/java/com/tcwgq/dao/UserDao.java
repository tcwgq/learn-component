package com.tcwgq.dao;

import com.tcwgq.domain.User;

/**
 * @author wgq
 * @email qiang.wang@guanaitong.com
 * @time 2017年3月10日 上午11:48:26
 */
public interface UserDao {
	public User insertUser(User user);

	public void deleteUser(User user);
}
