package com.tcwgq.service;

import com.tcwgq.domain.User;

/**
 * @author wgq
 * @email qiang.wang@guanaitong.com
 * @time 2017年3月10日 上午11:49:36
 */
public interface UserService {
	public User insertUser(User user);

	public void deleteUser(User user);
}
