package com.tcwgq.service.impl;

import com.tcwgq.dao.UserDao;
import com.tcwgq.domain.User;
import com.tcwgq.service.UserService;

/**
 * @author wgq
 * @email qiang.wang@guanaitong.com
 * @time 2017年3月10日 上午11:51:56
 */
public class UserServiceImpl implements UserService {
	private UserDao userDao;

	public UserServiceImpl() {

	}

	public UserServiceImpl(UserDao userDao) {
		this.userDao = userDao;
	}

	@Override
	public User insertUser(User user) {
		return userDao.insertUser(user);
	}

	@Override
	public void deleteUser(User user) {
		userDao.deleteUser(user);
	}

}
