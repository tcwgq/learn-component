package com.tcwgq.mockito.test;

import junit.framework.Assert;

import org.junit.Test;
import org.mockito.Mockito;

import com.tcwgq.dao.UserDao;
import com.tcwgq.domain.User;
import com.tcwgq.service.UserService;
import com.tcwgq.service.impl.UserServiceImpl;

public class MockitoTest {
	@Test
	public void testMockito() {
		User user = new User();
		user.setId(1);
		user.setName("zhangsan");
		user.setAge(23);
		// 使用Mockito mock了一个UserDao，相当于实现了UserDao
		UserDao userDao = Mockito.mock(UserDao.class);
		Mockito.when(userDao.insertUser(user)).thenReturn(user);
		User u = userDao.insertUser(user);
		UserService service = new UserServiceImpl(userDao);
		User us = service.insertUser(user);
		Assert.assertEquals(user, us);
		
		String json = "{}";
	}
}
