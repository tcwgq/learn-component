package com.tcwgq.netty.test;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by tcwgq on 2018/10/23 22:50.
 */
@Data
public class UserData implements Serializable {
    private Integer id;
    private String name;
    private String responseMessage;
}
