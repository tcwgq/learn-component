package com.tcwgq.netty.test;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by tcwgq on 2018/10/23 22:49.
 */
@Data
public class UserParam implements Serializable {
    private Integer id;
    private String name;
    private String requestMessage;
}
