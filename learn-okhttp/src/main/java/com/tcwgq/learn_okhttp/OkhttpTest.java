package com.tcwgq.learn_okhttp;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import org.junit.Test;

/**
 * @author tcwgq
 * @time 2017年12月12日下午2:16:56
 * @email qiang.wang@guanaitong.com
 */
public class OkhttpTest {
    @Test
    public void test1() throws IOException {
        // 普通的请求
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url("http://www.baidu.com").build();
        Response response = client.newCall(request).execute();
        if (response.isSuccessful()) {
            System.out.println(response.body().string());
        } else {
            System.out.println(response.code());
        }
    }

    @Test
    public void test2() throws IOException {
        // 提交json格式的数据
        MediaType type = MediaType.parse("application/json;charset=utf-8");
        OkHttpClient client = new OkHttpClient();
        RequestBody body = RequestBody.create(type, "");
        Request request = new Request.Builder().url("http://www.baidu.com").post(body).build();
        Response response = client.newCall(request).execute();
        if (response.isSuccessful()) {
            System.out.println(response.body().string());
        } else {
            System.out.println(response.code());
        }
    }

    @Test
    public void test3() throws IOException {
        // 提交form表单数据
        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder().add("platform", "android").add("name", "bug").add("subject", "XXXXXXXXXXXXXXX").build();
        Request request = new Request.Builder().url("http://www.baidu.com").post(formBody).build();
        Response response = client.newCall(request).execute();
        if (response.isSuccessful()) {
            System.out.println(response.body().string());
        } else {
            System.out.println(response.code());
        }
    }

    @Test
    public void test4() throws IOException {
        // 同步get
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url("http://publicobject.com/helloworld.txt").build();
        Response response = client.newCall(request).execute();
        if (!response.isSuccessful())
            throw new IOException("Unexpected code " + response);
        Headers responseHeaders = response.headers();
        for (int i = 0; i < responseHeaders.size(); i++) {
            System.out.println(responseHeaders.name(i) + ": " + responseHeaders.value(i));
        }
        System.out.println(response.body().string());
    }

    @Test
    public void test5() {
        // 异步get
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url("http://publicobject.com/helloworld.txt").build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful())
                    throw new IOException("Unexpected code " + response);

                Headers responseHeaders = response.headers();
                for (int i = 0; i < responseHeaders.size(); i++) {
                    System.out.println(responseHeaders.name(i) + ": " + responseHeaders.value(i));
                }

                System.out.println(response.body().string());
            }
        });
    }

    @Test
    public void test6() throws IOException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url("https://api.github.com/repos/square/okhttp/issues").header("User-Agent", "OkHttp Headers.java")
                .addHeader("Accept", "application/json; q=0.5").addHeader("Accept", "application/vnd.github.v3+json").build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful())
            throw new IOException("Unexpected code " + response);
        // 读取单值头
        System.out.println("Server: " + response.header("Server"));
        System.out.println("Date: " + response.header("Date"));
        // 读取多值头
        System.out.println("Vary: " + response.headers("Vary"));
    }
}
