package com.tcwgq.model.dto;

import lombok.Data;

import java.util.Date;

/**
 * @author iyb-wangguangqiang 2019/7/18 17:51
 */
@Data
public class PersonDto {
    private String name;
    private Integer age;
    private Date birthday;
    private String address;
}
