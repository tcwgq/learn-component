package com.tcwgq.model.entity;

import lombok.Data;

import java.util.Date;

/**
 * @author iyb-wangguangqiang 2019/7/18 17:49
 */
@Data
public class Person {
    private String name;
    private Integer age;
    private Date birthday;
    private String addr;
}
