package com.tcwgq.learn_poi;

/**
 * Hello world!
 *
 */
public class App {
    private static final User user = new User();

    public static void main(String[] args) {
        user.setName("zhangSan");
        System.out.println("Hello World!");
        System.out.println("Helloworld, java");
        System.out.println(user);
        // 引用不能修改
        // user = new User();
        User user1 = new User();
        System.out.println(user1);
    }
}
