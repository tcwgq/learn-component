package com.tcwgq.learn_poi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * @author tcwgq
 * @time 2018年1月2日下午2:42:29
 * @email qiang.wang@guanaitong.com
 */
public class AppendTest {
    public static void main(String[] args) throws IOException {
        FileInputStream fromFis = new FileInputStream(new File("d:/account.xlsx"));
        FileInputStream fs = new FileInputStream("d:/final.xlsx");
        // POIFSFileSystem ps = new POIFSFileSystem(fs);
        XSSFWorkbook fromWb = new XSSFWorkbook(fromFis);
        XSSFWorkbook toWb = new XSSFWorkbook(fs);
        int numberOfSheets = fromWb.getNumberOfSheets();
        for (int i = 0; i < numberOfSheets; i++) {
            XSSFSheet fromSheet = fromWb.getSheetAt(i);
            int fromLastRowNum = fromSheet.getLastRowNum();
            // 获取第一行作判断
            XSSFRow fromFirstRow = fromSheet.getRow(1);
            String type = fromFirstRow.getCell(2).toString();
            XSSFSheet sheet = null;
            if (type.equals("查询类")) {
                sheet = toWb.getSheet("r");
            } else {
                sheet = toWb.getSheet("w");
            }
            int lastRowNum = sheet.getLastRowNum();
            for (int j = 1; j <= fromLastRowNum; j++) {
                XSSFRow fromRow = fromSheet.getRow(j);
                int fromLastCellNum = fromRow.getLastCellNum();
                XSSFRow createRow = sheet.createRow(lastRowNum + j);
                StringBuilder sb = new StringBuilder();
                for (int k = 0; k < fromLastCellNum; k++) {
                    String value = fromRow.getCell(k).toString();
                    sb.append(value).append(" ");
                    createRow.createCell(k).setCellValue(value);
                }
                System.out.println(sb.deleteCharAt(sb.length() - 1).toString());
            }
        }
        // 写入文件
        FileOutputStream out = new FileOutputStream("d:/final.xlsx");
        toWb.write(out);
        fromWb.close();
        toWb.close();
        out.close();
    }

}
