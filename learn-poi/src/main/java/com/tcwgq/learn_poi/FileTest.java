package com.tcwgq.learn_poi;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

/**
 * @author tcwgq
 * @time 2017年12月14日下午6:21:43
 * @email qiang.wang@guanaitong.com
 */
public class FileTest {
    @Test
    public void test1() throws IOException {
        BufferedReader br = new BufferedReader((new InputStreamReader(new FileInputStream(new File("D:/assets-joined.log")), "UTF-8")));
        List<String> list = new ArrayList<>();
        String nextLine = null;
        while ((nextLine = br.readLine()) != null) {
            list.add(nextLine);
            int index = nextLine.indexOf("{");
            nextLine = nextLine.substring(index);
            System.out.println(nextLine);
            String[] splits = nextLine.split("\t");
            System.out.println("||||||||start|||||||||");
            System.out.println(splits[0]);
            System.out.println(splits[1]);
            System.out.println("||||||||end|||||||||");
        }
        br.close();
        // System.out.println(list);
    }
}
