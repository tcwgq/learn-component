package com.tcwgq.learn_poi;

import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.junit.Test;

/**
 * @author tcwgq
 * @time 2017年12月14日下午3:01:14
 * @email qiang.wang@guanaitong.com
 */
public class PIOTest1 {
    @Test
    public void CreateExcel() throws ParseException {
        List<Student> list = new ArrayList<>();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-mm-dd");
        Student user1 = new Student(1, "张三", 16, true, df.parse("1997-03-12"));
        Student user2 = new Student(2, "李四", 17, true, df.parse("1996-08-12"));
        Student user3 = new Student(3, "王五", 26, false, df.parse("1985-11-12"));
        list.add(user1);
        list.add(user2);
        list.add(user3);
        // 第一步，创建一个webbook，对应一个Excel文件
        HSSFWorkbook wb = new HSSFWorkbook();
        // 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
        HSSFSheet sheet = wb.createSheet("学生表一");
        sheet.setDefaultColumnWidth(30);
        // 第三步，在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制short
        HSSFRow row = sheet.createRow((int) 0);
        // 第四步，创建单元格，并设置值表头 设置表头居中
        HSSFCellStyle style = wb.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER); // 创建一个居中格式
        style.setVerticalAlignment(VerticalAlignment.CENTER);// 创建一个居中格式
        HSSFCell header = row.createCell((short) 0);
        header.setCellValue("学号");
        header.setCellStyle(style);
        header = row.createCell((short) 1);
        header.setCellValue("姓名");
        header.setCellStyle(style);
        header = row.createCell((short) 2);
        header.setCellValue("年龄");
        header.setCellStyle(style);
        header = row.createCell((short) 3);
        header.setCellValue("性别");
        header.setCellStyle(style);
        header = row.createCell((short) 4);
        header.setCellValue("生日");
        header.setCellStyle(style);
        // 第五步，写入实体数据 实际应用中这些数据从数据库得到，
        for (int i = 0; i < list.size(); i++) {
            row = sheet.createRow((int) i + 1);
            Student stu = (Student) list.get(i);
            // 第四步，创建单元格，并设置值
            HSSFCell id = row.createCell((short) 0);
            id.setCellStyle(style);
            id.setCellValue((double) stu.getId());
            HSSFCell name = row.createCell((short) 1);
            name.setCellStyle(style);
            name.setCellValue(stu.getName());
            HSSFCell age = row.createCell((short) 2);
            age.setCellStyle(style);
            age.setCellValue((double) stu.getAge());
            HSSFCell sex = row.createCell((short) 3);
            sex.setCellStyle(style);
            sex.setCellValue(stu.getSex() == true ? "男" : "女");
            HSSFCell birthday = row.createCell((short) 4);
            birthday.setCellStyle(style);
            birthday.setCellValue(new SimpleDateFormat("yyyy-mm-dd").format(stu.getBirthday()));
        }
        // 第六步，将文件存到指定位置
        try {
            FileOutputStream fout = new FileOutputStream("D:/students.xls");
            wb.write(fout);
            fout.close();
            wb.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}