package com.tcwgq.learn_poi;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tcwgq
 * @time 2018年1月3日下午1:38:53
 * @email qiang.wang@guanaitong.com
 */
public class Pair {
    private List<String> readList = new ArrayList<>();
    private List<String> writeList = new ArrayList<>();

    public List<String> getReadList() {
        return readList;
    }

    public void setReadList(List<String> readList) {
        this.readList = readList;
    }

    public List<String> getWriteList() {
        return writeList;
    }

    public void setWriteList(List<String> writeList) {
        this.writeList = writeList;
    }

    @Override
    public String toString() {
        return "Pair [readList=" + readList + ", writeList=" + writeList + "]";
    }

}
