package com.tcwgq.learn_poi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import com.alibaba.fastjson.JSONObject;

/**
 * @author tcwgq
 * @time 2018年1月3日上午10:54:27
 * @email qiang.wang@guanaitong.com
 */
public class ReadMultipleTest {
    @Test
    public void read() throws IOException {
        FileInputStream fromFis = new FileInputStream(new File("D:/point.xlsx"));
        XSSFWorkbook fromWb = new XSSFWorkbook(fromFis);
        int numberOfSheets = fromWb.getNumberOfSheets();
        Map<String, Pair> map = new LinkedHashMap<>();
        for (int i = 0; i < numberOfSheets; i++) {
            XSSFSheet fromSheet = fromWb.getSheetAt(i);
            int fromLastRowNum = fromSheet.getLastRowNum();
            String key = "";
            for (int j = 1; j <= fromLastRowNum; j++) {
                XSSFRow fromRow = fromSheet.getRow(j);
                XSSFCell cell1 = fromRow.getCell(0);
                if (cell1 == null) {
                    Pair pair = map.get(key);
                    for (int k = 0; k < 4; k++) {
                        XSSFCell cell = fromRow.getCell(k);
                        if (cell != null) {
                            String value = cell.toString();
                            if (k == 2) {
                                pair.getReadList().add(value);
                            } else if (k == 3) {
                                pair.getWriteList().add(value);
                            }
                        }
                    }
                } else {
                    key = cell1.toString();
                    Pair pair = new Pair();
                    for (int k = 0; k < 4; k++) {
                        XSSFCell cell = fromRow.getCell(k);
                        if (cell != null) {
                            String value = cell.toString();
                            if (k == 2) {
                                pair.getReadList().add(value);
                            } else if (k == 3) {
                                pair.getWriteList().add(value);
                            }
                        }
                    }
                    map.put(key, pair);
                }
            }
        }
        System.out.println(JSONObject.toJSONString(map));
        // 写入文件
        FileOutputStream out = new FileOutputStream("D:/final.xlsx");
        fromWb.close();
        out.close();
    }
}
