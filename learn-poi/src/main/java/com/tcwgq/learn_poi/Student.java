package com.tcwgq.learn_poi;

import java.util.Date;

/**
 * @author tcwgq
 * @time 2017年12月14日下午3:05:00
 * @email qiang.wang@guanaitong.com
 */
public class Student {
    private Integer id;
    private String name;
    private Integer age;
    private Boolean sex;
    private Date birthday;

    public Student() {

    }

    public Student(Integer id, String name, Integer age, Boolean sex, Date birthday) {
        super();
        this.id = id;
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.birthday = birthday;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Boolean getSex() {
        return sex;
    }

    public void setSex(Boolean sex) {
        this.sex = sex;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "Student [id=" + id + ", name=" + name + ", age=" + age + ", sex=" + sex + ", birthday=" + birthday + "]";
    }

}
