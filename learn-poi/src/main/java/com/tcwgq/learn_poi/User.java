package com.tcwgq.learn_poi;

/**
 * @author tcwgq
 * @time 2017年12月26日下午3:55:54
 * @email qiang.wang@guanaitong.com
 */
public class User {
    private String name;
    private Integer age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "User [name=" + name + ", age=" + age + "]";
    }

}
