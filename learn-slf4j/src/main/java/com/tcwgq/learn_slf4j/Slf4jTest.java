package com.tcwgq.learn_slf4j;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * debug < info < warn < error < fatal
 * 
 * @author admin
 *
 */
public class Slf4jTest {
	private Logger logger = LoggerFactory.getLogger(Slf4jTest.class);

	@Test
	public void fun() {
		logger.info("这是一个测试程序");
	}

	@Test
	public void debug() {
		logger.debug("debug级别的日志");

	}

	@Test
	public void info() {
		logger.info("info级别的日志");
	}

	@Test
	public void warn() {
		logger.warn("warn级别的日志");
	}

	@Test
	public void error() {
		logger.error("error级别的日志");
	}

}
